/****************************************************************************
 * drivers/sensors/ad5662.c
 * Character driver for the AD5662
 *
 * based on driver MAX31855 from Alan Carvalho de Assis
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/


/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdlib.h>
#include <fixedmath.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/kmalloc.h>
#include <nuttx/fs/fs.h>
#include <nuttx/spi/spi.h>
#include <nuttx/analog/ad5662.h>

#if CONFIG_SPI && CONFIG_DAC_AD5662

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private
 ****************************************************************************/
#define DEVNAME_FMT    "/dev/ad5662"
#define DEVNAME_FMTLEN (8 + 3 + 1)
#define AD5662_Hz 10


struct ad5662_dev_s
{
  FAR struct spi_dev_s *spi;
  sem_t exclsem;              /*  exclusion *//* Saved SPI driver instance */
};


/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Character driver methods */

static int     ad5662_open(FAR struct file *filep);
static int     ad5662_close(FAR struct file *filep);
static ssize_t ad5662_read(FAR struct file *filep, FAR char *buffer,
                             size_t buflen);
static ssize_t ad5662_write(FAR struct file *filep, FAR const char *buffer, size_t buflen);

static int     ad5662_ioctl(FAR struct file *filep, int cmd, unsigned long arg);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations g_ad5662fops =
{
  ad5662_open,
  ad5662_close,
  ad5662_read,
  ad5662_write,
  NULL,
  ad5662_ioctl,    /* ioctl */
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: ad5662_open
 *
 * Description:
 *   This function is called whenever the AD5662 device is opened.
 *
 ****************************************************************************/

static int ad5662_open(FAR struct file *filep)
{
	return OK;
}

/****************************************************************************
 * Name: ad5662_close
 *
 * Description:
 *   This routine is called when the AD5662 device is closed.
 *
 ****************************************************************************/

static int ad5662_close(FAR struct file *filep)
{
  return OK;
}

/****************************************************************************
 * Name: ad5662_read
 ****************************************************************************/

static ssize_t ad5662_read(FAR struct file *filep, FAR char *buffer, size_t buflen)
{
  return OK;
}

/****************************************************************************
 * Name: ad5662_write
 *
 * based on spi_transfer
 * Input Parameters:
 *   filep - file descriptor
 *   buffer -pointer to Byte Array, which contains all data to send
 *   size - size form 2 to 3 Byte
 *
 *
 ****************************************************************************/

static ssize_t ad5662_write(FAR struct file *filep, FAR const char *buffer, size_t buflen)
{
	FAR struct inode *inode = filep->f_inode;
	FAR struct ad5662_dev_s *priv;
	int ret, delay;

	/* Get exclusive access to the SPI driver state structure */

	DEBUGASSERT(filep != NULL && filep->f_inode != NULL);
	inode = filep->f_inode;

	priv = (FAR struct ad5662_dev_s*)inode->i_private;
	DEBUGASSERT(priv);


	ret = sem_wait(&priv->exclsem);

	if (ret < 0)
	{
		int errcode = errno;
		DEBUGASSERT(errcode < 0);
		return -errcode;
    }

	SPI_LOCK(priv->spi, true);

	SPI_SELECT(priv->spi, SPIDEV_AD5662, true);

	switch (buflen)
	 {
	 	 case 1:{
	 		SPI_SELECT(priv->spi, SPIDEV_AD5662, false);
	 		SPI_LOCK(priv->spi, false);
	 		return -errno;
	 	 }break;

	 	 case 2:
	 	 {
	 		unsigned char *empty = 0;
	 		/*first Byte empty*/
	 		SPI_SNDBLOCK(priv->spi, empty,1);
	 		++ buffer;
	 		/*TX Byte 1*/
	 		SPI_SNDBLOCK(priv->spi, buffer,1);
	 		++ buffer;
	 		/*TX Byte 2*/
	 		SPI_SNDBLOCK(priv->spi, buffer,1);

	 	 }break;
	 	 case 3:
	 	 {
	 		/*first Byte empty*/
	 		 SPI_SNDBLOCK(priv->spi, buffer,1);
	 		 ++ buffer;
	 		/*TX Byte 1*/
	 		 SPI_SNDBLOCK(priv->spi, buffer,1);
	 		 ++ buffer;
	 		 /*TX Byte 2*/
	 		 SPI_SNDBLOCK(priv->spi, buffer,1);
	 	 } 	break;
	 }

	/*if size of buffer to high*/

	if(buflen>3){
		SPI_SELECT(priv->spi, SPIDEV_AD5662, false);
		SPI_LOCK(priv->spi, false);
		return -errno;
	}


	SPI_SELECT(priv->spi, SPIDEV_AD5662, false);
	-- buffer;
	-- buffer;

	SPI_LOCK(priv->spi, false);

	sem_post(&priv->exclsem);

	return 0;
}

/****************************************************************************
 * Name: ad5662_ioctl
 *
 * Description:
 *
 *
 * Input Parameters:
 *   filep -
 *   cmd - IOCTL command
 *   arg - must be empty
 *
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/


static int ad5662_ioctl (FAR struct file *filep, int cmd, unsigned long arg)
{
	FAR struct inode *inode = filep->f_inode;
	FAR struct ad5662_dev_s *priv;
	int ret;

	/* Get exclusive access to the SPI driver state structure */

	DEBUGASSERT(filep != NULL && filep->f_inode != NULL);
	inode = filep->f_inode;

	priv = (FAR struct ad5662_dev_s*)inode->i_private;
	DEBUGASSERT(priv);

	ret = sem_wait(&priv->exclsem);

	if (ret < 0)
	{
		int errcode = errno;
		DEBUGASSERT(errcode < 0);
		return -errcode;
    }

	/*ioctl command*/

	 switch (cmd)
	    {
	      /* Command:    	1
	       * Description:	initialized the SPI - BUS
	       * Argument:		none
	       * Dependencies:
	       */
	      case 1:
	        {
	        	sem_post(&priv->exclsem);

	        	SPI_LOCK(priv->spi, true);

	        	SPI_SETFREQUENCY(priv->spi,AD5662_Hz);

	        	SPI_SETMODE(priv->spi,SPIDEV_MODE2);

	        	SPI_SETBITS(priv->spi,8);

	        	SPI_LOCK(priv->spi, false);
	        }
	        break;
	    }

	return 0;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: ad5662_register
 *
 * Description:
 *   Register the ad5662 character device as 'devpath'
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register. E.g., "/dev/temp0"
 *   spi - An instance of the SPI interface to use to communicate wit
 *   bus - the bus adress of the connected SPI Device
 *
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/

int ad5662_register(FAR const char *devpath,FAR struct spi_dev_s *spi, int bus)
{
	FAR struct ad5662_dev_s *priv;
	int ret;
	char devname[DEVNAME_FMTLEN];

	priv = (FAR struct ad5662_dev_s *)kmm_zalloc(sizeof(struct ad5662_dev_s));

	/*check*/
	priv->spi = spi;
	priv->exclsem.semcount = 1;
	/**/

	snprintf(devname, DEVNAME_FMTLEN, DEVNAME_FMT, bus);
	ret = register_driver(devname, &g_ad5662fops, 0666, priv);

	return ret;
}
#endif /* CONFIG_SPI && CONFIG_AD5662 */
