/****************************************************************************
 * nuttx/drivers/messBUS/messBUSmaster.c
 *
 *   Author: Peer Ulbig <p.ulbig@tu-braunschweig.de>
 *
 ****************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <poll.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/sched.h>
#include <nuttx/fs/fs.h>
#include <nuttx/messBUS/messBUSMaster.h>
#include <nuttx/fs/ioctl.h>

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/************************************************************************************
 * Private Types
 ************************************************************************************/

/************************************************************************************
 * Private Function Prototypes
 ************************************************************************************/

/* Character driver methods */
typedef FAR struct file		file_t;
static int     messBUSMaster_open(file_t *filep);
static int     messBUSMaster_close(file_t *filep);
static ssize_t messBUSMaster_read(file_t *filep, FAR char *buffer, size_t buflen);
static ssize_t messBUSMaster_write(file_t *filep, FAR const char *buf, size_t buflen);
static int     messBUSMaster_ioctl(FAR struct file *filep, int cmd, unsigned long arg);

/************************************************************************************
 * Private Data
 ************************************************************************************/

static const struct file_operations g_messBUSMaster_ops = {
	messBUSMaster_open,		/* open */
	messBUSMaster_close,	/* close */
	messBUSMaster_read,		/* read */
	messBUSMaster_write,	/* write */
	0,						/* seek */
	messBUSMaster_ioctl		/* ioctl */
#ifndef CONFIG_DISABLE_POLL
	, 0 					/* poll */
#endif
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
	, NULL      			/* unlink */
#endif
};

/************************************************************************************
 * Private Functions
 ************************************************************************************/

static int messBUSMaster_open(file_t *filep)
{
	FAR struct inode *inode = filep->f_inode;
	FAR messBUS_dev_t *dev = inode->i_private;
	int ret;
	int error = 0;	// OK

	if(dev->open)
	{
		/* Device has already been opened, so deny the access.
		 * We don't wont to support multiple opens yet to keep it
		 * simple. */
		error = -EACCES;
	}
	else
	{
		/* Open the device and perform initial setup. */
		dev->open = 1;
		ret = dev->ops->setup();
		if (ret < 0)
			{
				// Error handling ???
			}
	}

	return error; // error probably = 0 = OK
}

static int messBUSMaster_close(file_t *filep)
{
	/* Nothing to do here */

	return 0;
}

static ssize_t messBUSMaster_read(file_t *filep, FAR char *buf, size_t buflen)
{
	/* Nothing to do here */

	return 0;
}

static ssize_t messBUSMaster_write(file_t *filep, FAR const char *buf, size_t buflen)
{
	/* Nothing to do here */

	return 0;
}

static int messBUSMaster_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
	FAR struct inode *inode = filep->f_inode;
	messBUS_dev_t *dev = inode->i_private;
	int ret;

	switch(cmd)
	{
	case MESSBUSIOC_START:

		/* Make sure that there are already tables attached to the device.
		 * Reset the flag so that the interrupt handler does not switch
		 * to the empty tables immediately.
		 */
		if (dev->new_table_avail)
		{
			dev->new_table_avail = 0;
			ret = dev->ops->start();
			if (ret < 0)
				{
					// Error handling ???
				}
			dev->running = 1;
		}
		break;

	case MESSBUSIOC_ATTACH_SLOTLISTS:

		/* Cast the unsigned long arg to the address of our slotlist container.
		 * This container only contains one slotlist (slotlist1) for the client.
		 * The container is used to provide compatibility of the ioctl interfaces
		 * with respect to the master, which can operate up to four slotlists.
		 * The master's four slotlists have to be packed into one slotlist
		 * container as we can only parse one pointer to the ioctl function.
		 */
		; // Perform a dummy statement to satisfy the compiler
		struct slotlists_container_s *slotlists_container_ptr = (struct slotlists_container_s *) ((uintptr_t)arg);

		/* Things are more complicated for the master because we have to be able
		 * to convert multiple slotlists into both a single action table and
		 * multiple info tables. That's why we parse the whole container to the
		 * lower half driver and not a single slotlist.
		 * If the driver is already running convert the container to the currently
		 * inactive isr tables. If the driver is not running, always use tables
		 * with index 0 which the driver will start with by default.
		 */
		if (dev->running)
		{
			if (dev->active_table)
			{
				dev->ops->convert_slotlist(slotlists_container_ptr, dev->isr_actiontable0, dev->isr_infocontainer0);
			}
			else
			{
				dev->ops->convert_slotlist(slotlists_container_ptr, dev->isr_actiontable1, dev->isr_infocontainer1);
			}
		}
		else
		{
			dev->ops->convert_slotlist(slotlists_container_ptr, dev->isr_actiontable0, dev->isr_infocontainer0);
		}

		/* Signal that there are new tables available */
		dev->new_table_avail = 1;

		break;

	case MESSBUSIOC_SYNC_BUSYWAIT:

		/* Wait for the sync_counter to become non-zero.
		 *
		 * Hmmm... Maybe arg should be a pointer to uint8 in order to
		 * provide feedback about missed syncs in case sync_counter > 1.
		 *
		 * Be aware of the fact that usleep waits at least one microsecond.
		 * Depending on CONFIG_USEC_PER_TICK the sleep can take much longer
		 * than the specified microsecond. Furthermore this is not a busy
		 * wait but a suspension of the calling thread until the scheduler
		 * reawakens it.
		 */
		while (dev->sync_count < 1)
		{
			//usleep(CONFIG_MESSBUS_SYNCWAIT_SLEEP);
			/* usleep causes failure of the driver, probably because the scheduler
			 * disables interrupts for some time. The use of usleep should be preferred
			 * on the long run, but this requires improvements of the high priority
			 * interrupts, so that they can not be disabled at any times and also interrupt
			 * the scheduler.
			 */
		}

		/* Reset the sync_counter before returning. */
		dev->sync_count = 0;

		break;

	case MESSBUSIOC_STOP:

		/* Not implemented yet */
		break;


	case MESSBUSIOC_UPDATE_RXBUFFERS:

		/* make the lower half update the buffers */
		dev->ops->update_buffers();

		break;

#if CONFIG_MESSBUS_USE_CH1
	case MESSBUSIOC_CH1_GET_LAST_PROCESSED_SLOT_NUMBER:

			; // Perform a dummy statement to satisfy the compiler
			uint8_t *ch1_slot_number_ptr = (uint8_t *) ((uintptr_t)arg);
			if (ch1_slot_number_ptr != 0)
			{
				*ch1_slot_number_ptr = dev->ch1_last_processed_slot_number;
			}
			break;

	case MESSBUSIOC_CH1_ATTACH_NEXT_SLOT_CALLBACK:

	#if CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			; // Perform a dummy statement to satisfy the compiler
			void *ch1_slot_callback = (void *) ((uintptr_t)arg);
			if (ch1_slot_callback != 0)
			{
				if(dev->callbacks_info->ch1_single_callback_info->pendsv_pending == 0)
				{
					dev->callbacks_info->ch1_single_callback_info->request = MESSBUS_NEXT_SLOT_CALLBACK_REQUEST;
					dev->callbacks_info->ch1_single_callback_info->callback_function = ch1_slot_callback;
					dev->callbacks_info->ch1_single_callback_info->pendsv_pending = 1;
					dev->callbacks_info->ch1_single_callback_info->trigger_pending = 1;
				}
				else
				{
					return -EBUSY;
				}
			}
			else
			{
				return -EINVAL;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			break;

		case MESSBUSIOC_CH1_ATTACH_SLOT_NUMBER_CALLBACK:

	#if CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			; // Perform a dummy statement to satisfy the compiler
			struct single_callback_spec_s *ch1_callback_s = (struct single_callback_spec_s *) ((uintptr_t)arg);
			if (ch1_callback_s != 0)
			{
				if(dev->callbacks_info->ch1_single_callback_info->pendsv_pending == 0)
				{
					dev->callbacks_info->ch1_single_callback_info->request = ch1_callback_s->slot_number;
					dev->callbacks_info->ch1_single_callback_info->callback_function = ch1_callback_s->callback_function;
					dev->callbacks_info->ch1_single_callback_info->pendsv_pending = 1;
					dev->callbacks_info->ch1_single_callback_info->trigger_pending = 1;
				}
				else
				{
					return -EBUSY;
				}
			}
			else
			{
				return -EINVAL;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			break;

		case MESSBUSIOC_CH1_ATTACH_NEXT_SYNC_CALLBACK:

	#if CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			; // Perform a dummy statement to satisfy the compiler
			void *ch1_sync_callback = (void *) ((uintptr_t)arg);
			if (ch1_sync_callback != 0)
			{
				if(dev->callbacks_info->ch1_single_callback_info->pendsv_pending == 0)
				{
					dev->callbacks_info->ch1_single_callback_info->request = MESSBUS_SYNC_CALLBACK_REQUEST;
					dev->callbacks_info->ch1_single_callback_info->callback_function = ch1_sync_callback;
					dev->callbacks_info->ch1_single_callback_info->pendsv_pending = 1;
					dev->callbacks_info->ch1_single_callback_info->trigger_pending = 1;
				}
				else
				{
					return -EBUSY;
				}
			}
			else
			{
				return -EINVAL;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			break;

		case MESSBUSIOC_CH1_CONFIGURE_WAKE_UP_CALLBACK:

	#if CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
			; // Perform a dummy statement to satisfy the compiler
			struct wake_up_callback_spec_s *ch1_wake_up_callback_s = (struct wake_up_callback_spec_s *) ((uintptr_t)arg);
			if (ch1_wake_up_callback_s != 0)
			{
				dev->callbacks_info->ch1_wake_up_callback_info->t_wake_up = ch1_wake_up_callback_s->t_wake_up;
				dev->callbacks_info->ch1_wake_up_callback_info->callback_function = ch1_wake_up_callback_s->callback_function;
				dev->callbacks_info->ch1_wake_up_callback_info->configured = 1;
			}
			else
			{
				return -EINVAL;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
			break;

		case MESSBUSIOC_CH1_ENABLE_WAKE_UP_CALLBACK:

	#if CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
			if (dev->callbacks_info->ch1_wake_up_callback_info->configured == 1)
			{
				dev->callbacks_info->ch1_wake_up_callback_info->enabled = 1;
			}
			else
			{
				return -EPERM;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
			break;

		case MESSBUSIOC_CH1_CH3_ENABLE_BUS_POWER:

			dev->ops->enableBusPower(channel1, true);
			break;

		case MESSBUSIOC_CH1_CH3_DISABLE_BUS_POWER:

			dev->ops->enableBusPower(channel1, false);
			break;

#endif //CONFIG_MESSBUS_USE_CH1

#if CONFIG_MESSBUS_USE_CH2
	case MESSBUSIOC_CH2_GET_LAST_PROCESSED_SLOT_NUMBER:

			; // Perform a dummy statement to satisfy the compiler
			uint8_t *ch2_slot_number_ptr = (uint8_t *) ((uintptr_t)arg);
			if (ch2_slot_number_ptr != 0)
			{
				*ch2_slot_number_ptr = dev->ch2_last_processed_slot_number;
			}
			break;

	case MESSBUSIOC_CH2_ATTACH_NEXT_SLOT_CALLBACK:

	#if CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			; // Perform a dummy statement to satisfy the compiler
			void *ch2_slot_callback = (void *) ((uintptr_t)arg);
			if (ch2_slot_callback != 0)
			{
				if(dev->callbacks_info->ch2_single_callback_info->pendsv_pending == 0)
				{
					dev->callbacks_info->ch2_single_callback_info->request = MESSBUS_NEXT_SLOT_CALLBACK_REQUEST;
					dev->callbacks_info->ch2_single_callback_info->callback_function = ch2_slot_callback;
					dev->callbacks_info->ch2_single_callback_info->pendsv_pending = 1;
					dev->callbacks_info->ch2_single_callback_info->trigger_pending = 1;
				}
				else
				{
					return -EBUSY;
				}
			}
			else
			{
				return -EINVAL;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			break;

		case MESSBUSIOC_CH2_ATTACH_SLOT_NUMBER_CALLBACK:

	#if CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			; // Perform a dummy statement to satisfy the compiler
			struct single_callback_spec_s *ch2_callback_s = (struct single_callback_spec_s *) ((uintptr_t)arg);
			if (ch2_callback_s != 0)
			{
				if(dev->callbacks_info->ch2_single_callback_info->pendsv_pending == 0)
				{
					dev->callbacks_info->ch2_single_callback_info->request = ch2_callback_s->slot_number;
					dev->callbacks_info->ch2_single_callback_info->callback_function = ch2_callback_s->callback_function;
					dev->callbacks_info->ch2_single_callback_info->pendsv_pending = 1;
					dev->callbacks_info->ch2_single_callback_info->trigger_pending = 1;
				}
				else
				{
					return -EBUSY;
				}
			}
			else
			{
				return -EINVAL;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			break;

		case MESSBUSIOC_CH2_ATTACH_NEXT_SYNC_CALLBACK:

	#if CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			; // Perform a dummy statement to satisfy the compiler
			void *ch2_sync_callback = (void *) ((uintptr_t)arg);
			if (ch2_sync_callback != 0)
			{
				if(dev->callbacks_info->ch2_single_callback_info->pendsv_pending == 0)
				{
					dev->callbacks_info->ch2_single_callback_info->request = MESSBUS_SYNC_CALLBACK_REQUEST;
					dev->callbacks_info->ch2_single_callback_info->callback_function = ch2_sync_callback;
					dev->callbacks_info->ch2_single_callback_info->pendsv_pending = 1;
					dev->callbacks_info->ch2_single_callback_info->trigger_pending = 1;
				}
				else
				{
					return -EBUSY;
				}
			}
			else
			{
				return -EINVAL;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			break;

		case MESSBUSIOC_CH2_CONFIGURE_WAKE_UP_CALLBACK:

	#if CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
			; // Perform a dummy statement to satisfy the compiler
			struct wake_up_callback_spec_s *ch2_wake_up_callback_s = (struct wake_up_callback_spec_s *) ((uintptr_t)arg);
			if (ch2_wake_up_callback_s != 0)
			{
				dev->callbacks_info->ch2_wake_up_callback_info->t_wake_up = ch2_wake_up_callback_s->t_wake_up;
				dev->callbacks_info->ch2_wake_up_callback_info->callback_function = ch2_wake_up_callback_s->callback_function;
				dev->callbacks_info->ch2_wake_up_callback_info->configured = 1;
			}
			else
			{
				return -EINVAL;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
			break;

		case MESSBUSIOC_CH2_ENABLE_WAKE_UP_CALLBACK:

	#if CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
			if (dev->callbacks_info->ch2_wake_up_callback_info->configured == 1)
			{
				dev->callbacks_info->ch2_wake_up_callback_info->enabled = 1;
			}
			else
			{
				return -EPERM;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
			break;

		case MESSBUSIOC_CH2_CH4_ENABLE_BUS_POWER:

			dev->ops->enableBusPower(channel2, true);
			break;

		case MESSBUSIOC_CH2_CH4_DISABLE_BUS_POWER:

			dev->ops->enableBusPower(channel2, false);
			break;

#endif //CONFIG_MESSBUS_USE_CH2

#if CONFIG_MESSBUS_USE_CH3
	case MESSBUSIOC_CH3_GET_LAST_PROCESSED_SLOT_NUMBER:

			; // Perform a dummy statement to satisfy the compiler
			uint8_t *ch3_slot_number_ptr = (uint8_t *) ((uintptr_t)arg);
			if (ch3_slot_number_ptr != 0)
			{
				*ch3_slot_number_ptr = dev->ch3_last_processed_slot_number;
			}
			break;

	case MESSBUSIOC_CH3_ATTACH_NEXT_SLOT_CALLBACK:

	#if CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			; // Perform a dummy statement to satisfy the compiler
			void *ch3_slot_callback = (void *) ((uintptr_t)arg);
			if (ch3_slot_callback != 0)
			{
				if(dev->callbacks_info->ch3_single_callback_info->pendsv_pending == 0)
				{
					dev->callbacks_info->ch3_single_callback_info->request = MESSBUS_NEXT_SLOT_CALLBACK_REQUEST;
					dev->callbacks_info->ch3_single_callback_info->callback_function = ch3_slot_callback;
					dev->callbacks_info->ch3_single_callback_info->pendsv_pending = 1;
					dev->callbacks_info->ch3_single_callback_info->trigger_pending = 1;
				}
				else
				{
					return -EBUSY;
				}
			}
			else
			{
				return -EINVAL;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			break;

		case MESSBUSIOC_CH3_ATTACH_SLOT_NUMBER_CALLBACK:

	#if CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			; // Perform a dummy statement to satisfy the compiler
			struct single_callback_spec_s *ch3_callback_s = (struct single_callback_spec_s *) ((uintptr_t)arg);
			if (ch3_callback_s != 0)
			{
				if(dev->callbacks_info->ch3_single_callback_info->pendsv_pending == 0)
				{
					dev->callbacks_info->ch3_single_callback_info->request = ch3_callback_s->slot_number;
					dev->callbacks_info->ch3_single_callback_info->callback_function = ch3_callback_s->callback_function;
					dev->callbacks_info->ch3_single_callback_info->pendsv_pending = 1;
					dev->callbacks_info->ch3_single_callback_info->trigger_pending = 1;
				}
				else
				{
					return -EBUSY;
				}
			}
			else
			{
				return -EINVAL;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			break;

		case MESSBUSIOC_CH3_ATTACH_NEXT_SYNC_CALLBACK:

	#if CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			; // Perform a dummy statement to satisfy the compiler
			void *ch3_sync_callback = (void *) ((uintptr_t)arg);
			if (ch3_sync_callback != 0)
			{
				if(dev->callbacks_info->ch3_single_callback_info->pendsv_pending == 0)
				{
					dev->callbacks_info->ch3_single_callback_info->request = MESSBUS_SYNC_CALLBACK_REQUEST;
					dev->callbacks_info->ch3_single_callback_info->callback_function = ch3_sync_callback;
					dev->callbacks_info->ch3_single_callback_info->pendsv_pending = 1;
					dev->callbacks_info->ch3_single_callback_info->trigger_pending = 1;
				}
				else
				{
					return -EBUSY;
				}
			}
			else
			{
				return -EINVAL;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			break;

		case MESSBUSIOC_CH3_CONFIGURE_WAKE_UP_CALLBACK:

	#if CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
			; // Perform a dummy statement to satisfy the compiler
			struct wake_up_callback_spec_s *ch3_wake_up_callback_s = (struct wake_up_callback_spec_s *) ((uintptr_t)arg);
			if (ch3_wake_up_callback_s != 0)
			{
				dev->callbacks_info->ch3_wake_up_callback_info->t_wake_up = ch3_wake_up_callback_s->t_wake_up;
				dev->callbacks_info->ch3_wake_up_callback_info->callback_function = ch3_wake_up_callback_s->callback_function;
				dev->callbacks_info->ch3_wake_up_callback_info->configured = 1;
			}
			else
			{
				return -EINVAL;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
			break;

		case MESSBUSIOC_CH3_ENABLE_WAKE_UP_CALLBACK:

	#if CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
			if (dev->callbacks_info->ch3_wake_up_callback_info->configured == 1)
			{
				dev->callbacks_info->ch3_wake_up_callback_info->enabled = 1;
			}
			else
			{
				return -EPERM;
			}
	#else
			return -EPERM;

	#endif //CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
			break;

		case MESSBUSIOC_CH1_CH3_ENABLE_BUS_POWER:

			dev->ops->enableBusPower(channel3, true);
			break;

		case MESSBUSIOC_CH1_CH3_DISABLE_BUS_POWER:

			dev->ops->enableBusPower(channel3, false);
			break;

#endif //CONFIG_MESSBUS_USE_CH3

#if CONFIG_MESSBUS_USE_CH4
	case MESSBUSIOC_CH4_GET_LAST_PROCESSED_SLOT_NUMBER:

			; // Perform a dummy statement to satisfy the compiler
			uint8_t *ch4_slot_number_ptr = (uint8_t *) ((uintptr_t)arg);
			if (ch4_slot_number_ptr != 0)
			{
				*ch4_slot_number_ptr = dev->ch4_last_processed_slot_number;
			}
			break;

	case MESSBUSIOC_CH4_ATTACH_NEXT_SLOT_CALLBACK:

	#if CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			; // Perform a dummy statement to satisfy the compiler
			void *ch4_slot_callback = (void *) ((uintptr_t)arg);
			if (ch4_slot_callback != 0)
			{
				if(dev->callbacks_info->ch4_single_callback_info->pendsv_pending == 0)
				{
					dev->callbacks_info->ch4_single_callback_info->request = MESSBUS_NEXT_SLOT_CALLBACK_REQUEST;
					dev->callbacks_info->ch4_single_callback_info->callback_function = ch4_slot_callback;
					dev->callbacks_info->ch4_single_callback_info->pendsv_pending = 1;
					dev->callbacks_info->ch4_single_callback_info->trigger_pending = 1;
				}
				else
				{
					return -EBUSY;
				}
			}
			else
			{
				return -EINVAL;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			break;

		case MESSBUSIOC_CH4_ATTACH_SLOT_NUMBER_CALLBACK:

	#if CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			; // Perform a dummy statement to satisfy the compiler
			struct single_callback_spec_s *ch4_callback_s = (struct single_callback_spec_s *) ((uintptr_t)arg);
			if (ch4_callback_s != 0)
			{
				if(dev->callbacks_info->ch4_single_callback_info->pendsv_pending == 0)
				{
					dev->callbacks_info->ch4_single_callback_info->request = ch4_callback_s->slot_number;
					dev->callbacks_info->ch4_single_callback_info->callback_function = ch4_callback_s->callback_function;
					dev->callbacks_info->ch4_single_callback_info->pendsv_pending = 1;
					dev->callbacks_info->ch4_single_callback_info->trigger_pending = 1;
				}
				else
				{
					return -EBUSY;
				}
			}
			else
			{
				return -EINVAL;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			break;

		case MESSBUSIOC_CH4_ATTACH_NEXT_SYNC_CALLBACK:

	#if CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			; // Perform a dummy statement to satisfy the compiler
			void *ch4_sync_callback = (void *) ((uintptr_t)arg);
			if (ch4_sync_callback != 0)
			{
				if(dev->callbacks_info->ch4_single_callback_info->pendsv_pending == 0)
				{
					dev->callbacks_info->ch4_single_callback_info->request = MESSBUS_SYNC_CALLBACK_REQUEST;
					dev->callbacks_info->ch4_single_callback_info->callback_function = ch4_sync_callback;
					dev->callbacks_info->ch4_single_callback_info->pendsv_pending = 1;
					dev->callbacks_info->ch4_single_callback_info->trigger_pending = 1;
				}
				else
				{
					return -EBUSY;
				}
			}
			else
			{
				return -EINVAL;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			break;

		case MESSBUSIOC_CH4_CONFIGURE_WAKE_UP_CALLBACK:

	#if CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
			; // Perform a dummy statement to satisfy the compiler
			struct wake_up_callback_spec_s *ch4_wake_up_callback_s = (struct wake_up_callback_spec_s *) ((uintptr_t)arg);
			if (ch4_wake_up_callback_s != 0)
			{
				dev->callbacks_info->ch4_wake_up_callback_info->t_wake_up = ch4_wake_up_callback_s->t_wake_up;
				dev->callbacks_info->ch4_wake_up_callback_info->callback_function = ch4_wake_up_callback_s->callback_function;
				dev->callbacks_info->ch4_wake_up_callback_info->configured = 1;
			}
			else
			{
				return -EINVAL;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
			break;

		case MESSBUSIOC_CH4_ENABLE_WAKE_UP_CALLBACK:

	#if CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
			if (dev->callbacks_info->ch4_wake_up_callback_info->configured == 1)
			{
				dev->callbacks_info->ch4_wake_up_callback_info->enabled = 1;
			}
			else
			{
				return -EPERM;
			}
	#else
			return -EPERM;
	#endif //CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
			break;

		case MESSBUSIOC_CH2_CH4_ENABLE_BUS_POWER:

			dev->ops->enableBusPower(channel4, true);
			break;

		case MESSBUSIOC_CH2_CH4_DISABLE_BUS_POWER:

			dev->ops->enableBusPower(channel4, false);
			break;

#endif //CONFIG_MESSBUS_USE_CH4

	}

	return 0;
}


/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: messBUSMaster_register
 *
 * Description:
 *   Register the upper half with its lower half in the VFS.
 *
 ************************************************************************************/

int messBUSMaster_register(FAR const char *path, FAR messBUS_dev_t *dev)
{
  /* Register the driver */
  return register_driver(path, &g_messBUSMaster_ops, 0444, dev);
}
