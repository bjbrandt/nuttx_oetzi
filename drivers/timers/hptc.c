/****************************************************************************
 * drivers/timers/hptc.c
 *
 *   Copyright (C) 2014, 2016 Gregory Nutt. All rights reserved.
 *   Authors: Gregory Nutt <gnutt@nuttx.org>
 *            Stefan Nowak
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <assert.h>
#include <errno.h>
#include <debug.h>
#include <semaphore.h>
#include <nuttx/fs/fs.h>
#include <nuttx/irq.h>
#include <nuttx/kmalloc.h>
#include <nuttx/timers/hptc.h>

#ifdef CONFIG_HPTC


/****************************************************************************
 * Private Type Definitions
 ****************************************************************************/
#if 0
/* This structure describes the state of the upper half driver */

struct hptc_upperhalf_s
{
  uint8_t   crefs;         /* The number of times the device has been opened */
  uint8_t   signo;         /* The signal number to use in the notification */
  pid_t     pid;           /* The ID of the task/thread to receive the signal */
  sem_t     sem;           /* semaphore to be posted */
  FAR void *arg;           /* An argument to pass with the signal */
  FAR char *path;          /* Registration path */

  /* The contained lower-half driver */

  FAR struct hptc_dev_s *dev;
};
#endif
/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/
#if 0 /* XXX */
static bool    timer_notifier(FAR uint32_t *next_interval_us, FAR void *arg);
#endif /* XXX */
static int     hptc_open(FAR struct file *filep);
static int     hptc_close(FAR struct file *filep);
static ssize_t hptc_read(FAR struct file *filep, FAR char *buffer,
                 size_t buflen);
static ssize_t hptc_write(FAR struct file *filep, FAR const char *buffer,
                 size_t buflen);
static int     hptc_ioctl(FAR struct file *filep, int cmd,
                 unsigned long arg);
#if 0
static void    hptc_notify(FAR struct hptc_dev_s *dev);
#endif
/****************************************************************************
 * Private Data
 ****************************************************************************/

//FIXME: workaround
static struct hptc_dev_s *g_dev;

static const struct file_operations g_hptcops =
{
  hptc_open,  /* open */
  hptc_close, /* close */
  hptc_read,  /* read */
  hptc_write, /* write */
  NULL,        /* seek */
  hptc_ioctl  /* ioctl */
#ifndef CONFIG_DISABLE_POLL
  , NULL       /* poll */
#endif
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
  , NULL       /* unlink */
#endif
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/************************************************************************************
 * Name: timer_notifier
 *
 * Description:
 *   Notify the application via a signal when the timer interrupt occurs
 *
 * REVISIT: This function prototype is insufficient to support signaling
 *
 ************************************************************************************/
#if 0
static bool timer_notifier(FAR uint32_t *next_interval_us, FAR void *arg)
{
  FAR struct hptc_upperhalf_s *upper = (FAR struct hptc_upperhalf_s *)arg;
#ifdef CONFIG_CAN_PASS_STRUCTS
  union sigval value;
#endif

  DEBUGASSERT(upper != NULL);

  /* Signal the waiter.. if there is one */

#ifdef CONFIG_CAN_PASS_STRUCTS
  value.sival_ptr = upper->arg;
  (void)sigqueue(upper->pid, upper->signo, value);
#else
  (void)sigqueue(upper->pid, upper->signo, upper->arg);
#endif

  return true;
}
#endif

/************************************************************************************
 * Name: hptc_open
 *
 * Description:
 *   This function is called whenever the timer device is opened.
 *
 ************************************************************************************/

static int hptc_open(FAR struct file *filep)
{
  FAR struct inode             *inode = filep->f_inode;
  FAR struct hptc_dev_s *dev = inode->i_private;
  uint8_t                       tmp;
  int                           ret;

//  tmrinfo("crefs: %d\n", upper->crefs);

  /* Increment the count of references to the device.  If this the first
   * time that the driver has been opened for this device, then initialize
   * the device.
   */

  tmp = dev->crefs + 1;
  if (tmp == 0)
    {
      /* More than 255 opens; uint8_t overflows to zero */

      ret = -EMFILE;
      goto errout;
    }

  /* Save the new open count */

  dev->crefs = tmp;
  ret = OK;

errout:
  return ret;
}

/************************************************************************************
 * Name: hptc_close
 *
 * Description:
 *   This function is called when the hptc device is closed.
 *
 ************************************************************************************/

static int hptc_close(FAR struct file *filep)
{
  FAR struct inode *inode = filep->f_inode;
  FAR struct hptc_dev_s *dev = inode->i_private;

//  tmrinfo("crefs: %d\n", upper->crefs);

  /* Decrement the references to the driver.  If the reference count will
   * decrement to 0, then uninitialize the driver.
   */

  if (dev->crefs > 0)
    {
      dev->crefs--;
    }

  return OK;
}

/************************************************************************************
 * Name: hptc_read
 *
 * Description:
 *   A dummy read method.  This is provided only to satisfy the VFS layer.
 *
 ************************************************************************************/

static ssize_t hptc_read(FAR struct file *filep, FAR char *buffer, size_t buflen)
{
  /* Return zero -- usually meaning end-of-file */

  return 0;
#if 0
  FAR struct inode     *inode = filep->f_inode;
  FAR struct hptc_dev_s *dev   = inode->i_private;
  size_t                nread;
  irqstate_t            flags;
  int                   ret   = 0;
  int                   msglen;

  tmrinfo("buflen: %d\n", (int)buflen);
#if 0
  /* Determine size of the messages to return.
   *
   * REVISIT:  What if buflen is 8 does that mean 4 messages of size 2?  Or
   * 2 messages of size 4?  What if buflen is 12.  Does that mean 3 at size
   * 4?  Or 4 at size 3?  The form of the return data should probably really
   * be specified via IOCTL.
   */

  if (buflen % 5 == 0)
    {
      msglen = 5;
    }
  else if (buflen % 4 == 0)
    {
      msglen = 4;
    }
  else if (buflen % 3 == 0)
    {
      msglen = 3;
    }
  else if (buflen % 2 == 0)
    {
      msglen = 2;
    }
  else if (buflen == 1)
    {
      msglen = 1;
    }
  else
#endif
    {
      msglen = 10;
    }

  if (buflen >= msglen)
    {
      /* Interrupts must be disabled while accessing the ad_recv FIFO */
//FIXME: use mutex like serial.c
      flags = enter_critical_section();
      while (dev->ic_recv.head == dev->ic_recv.tail)
        {
          /* The receive FIFO is empty -- was non-blocking mode selected? */

          if (filep->f_oflags & O_NONBLOCK)
            {
              ret = -EAGAIN;
              goto return_with_irqdisabled;
            }

          /* Wait for a message to be received */

          dev->nrxwaiters++;
          ret = nxsem_wait(&dev->ic_recv.sem);
          dev->nrxwaiters--;
          if (ret < 0)
            {
              goto return_with_irqdisabled;
            }
        }

      /* The ad_recv FIFO is not empty.  Copy all buffered data that will fit
       * in the user buffer.
       */

      nread = 0;
      do
        {
          FAR struct hptc_ic_msg_s *msg = &dev->ic_recv.buffer[dev->ic_recv.head];

          /* Will the next message in the FIFO fit into the user buffer? */

          if (nread + msglen > buflen)
            {
              /* No.. break out of the loop now with nread equal to the actual
               * number of bytes transferred.
               */

              break;
            }

          /* Copy the message to the user buffer */
#if 0
          if (msglen == 1)
            {
              /* Only one channel, return MS 8-bits of the sample*/

              buffer[nread] = msg->data >> 24;
            }
          else if (msglen == 2)
            {
              /* Only one channel, return only the MS 16-bits of the sample.*/

              int16_t data16 = msg->data >> 16;
              memcpy(&buffer[nread], &data16, 2);
            }
          else if (msglen == 3)
            {
              int16_t data16;

              /* Return the channel and the MS 16-bits of the sample. */

              buffer[nread] = msg->channel;
              data16 = msg->data >> 16;
              memcpy(&buffer[nread + 1], &data16, 2);
            }
          else if (msglen == 4)
            {
              int32_t data24;

#ifdef CONFIG_ENDIAN_BIG
              /* In the big endian case, we simply copy the MS three bytes
               * which are indices: 0-2.
               */

              data24 = msg->data;
#else
              /* In the little endian case, indices 0-2 correspond to the
               * the three LS bytes.
               */

              data24 = msg->data >> 8;
#endif

              /* Return the channel and the most significant 24-bits */

              buffer[nread] = msg->channel;
              memcpy(&buffer[nread + 1], &data24, 3);
            }
          else
            {
              /* Return the channel and all four bytes of the sample */

              buffer[nread] = msg->channel;
              memcpy(&buffer[nread + 1], &msg->data, 4);
            }
#endif

          memcpy(&buffer[nread], msg, msglen);


          nread += msglen;

          /* Increment the head of the circular message buffer */

          if (++dev->ic_recv.head >= CONFIG_HPTC_IC_FIFOSIZE)
            {
              dev->ic_recv.head = 0;
            }
        }
      while (dev->ic_recv.head != dev->ic_recv.tail);

      /* All on the messages have bee transferred.  Return the number of bytes
       * that were read.
       */

      ret = nread;

return_with_irqdisabled:
      leave_critical_section(flags);
    }

  tmrinfo("Returning: %d\n", ret);
  return ret;
#endif
}


/************************************************************************************
 * Name: timer_write
 *
 * Description:
 *   A dummy write method.  This is provided only to satisfy the VFS layer.
 *
 ************************************************************************************/

static ssize_t hptc_write(FAR struct file *filep, FAR const char *buffer,
                           size_t buflen)
{
  return 0;
}

/************************************************************************************
 * Name: hptc_ioctl
 *
 * Description:
 *   The standard ioctl method.  This is where ALL of the hptc work is
 *   done.
 *
 ************************************************************************************/

static int hptc_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
  FAR struct inode             *inode = filep->f_inode;
  FAR struct hptc_dev_s *dev = inode->i_private;
  //FAR struct hptc_dev_s *dev = upper->dev;
  int                           ret;

  tmrinfo("cmd: %d arg: %ld\n", cmd, arg);
  DEBUGASSERT(dev);

  /* Handle built-in ioctl commands */

  switch (cmd)
    {
    /* cmd:         TCIOC_START
     * Description: Start the timer
     * Argument:    Ignored
     */

    case HPTCIOC_START:
      {
        /* Start the timer, resetting the time to the current timeout */

        if (dev->ops->start)
          {
            ret = dev->ops->start(dev,&(dev->sem));
          }
        else
          {
            ret = -ENOSYS;
          }
      }
      break;

    /* cmd:         TCIOC_STOP
     * Description: Stop the timer
     * Argument:    Ignored
     */

    case HPTCIOC_STOP:
      {
        /* Stop the timer */

        if (dev->ops->stop)
          {
            ret = dev->ops->stop(dev);
          }
        else
          {
            ret = -ENOSYS;
          }
      }
      break;

    /* cmd:         TCIOC_GETSTATUS
     * Description: Get the status of the timer.
     * Argument:    A writeable pointer to struct timer_status_s.
     */

    case HPTCIOC_GETSTATUS:
      {
        FAR struct hptc_status_s *status;

        /* Get the current timer status */

        if (dev->ops->getstatus) /* Optional */
          {
            status = (FAR struct hptc_status_s *)((uintptr_t)arg);
            if (status)
              {
                ret = dev->ops->getstatus(dev, status);
              }
            else
              {
                ret = -EINVAL;
              }
          }
        else
          {
            ret = -ENOSYS;
          }
      }
      break;

    case HPTCIOC_GETSEM:
      {
    	sem_t **sem;
    	sem = (sem_t **)((uintptr_t)arg);
        *sem=(&dev->sem);

        ret = OK;

      }
      break;

    case HPTCIOC_GETSEMTIME:
      {
    	  FAR struct timespec *ts;
    	  if(dev->ops->getsemtime)
    	  {
    		  ts = (FAR struct timespec *)((uintptr_t)arg);
    		  if (ts){
    			  ret = dev->ops->getsemtime(dev, ts);
    		  }
    		  else
    		  {
    			  ret = -EINVAL;
    		  }
    	  }
    	  else
          {
            ret = -ENOSYS;
          }
      }
      break;

     /* cmd:         HPTCIOC_TUNE
      * Description: Tune the clock.
      * Argument:    A 32-bit tune value in nanoseconds per second (ppb)
      */

    case HPTCIOC_TUNE:
      {
          if (dev->ops->tune)
            {
              ret = dev->ops->tune((int32_t) arg);
            }
          else
            {
              ret = -ENOSYS;
            }

      }
      break;

      /* cmd:         HPTCIOC_JUMP
       * Description: Jump in time
       * Argument:    A struct timespec offset to jump
       */
    case HPTCIOC_JUMP:
      {
          if (dev->ops->jump)
            {
              ret = dev->ops->jump((struct timespec*) arg);
            }
          else
            {
              ret = -ENOSYS;
            }

      }
      break;



#if 0
    /* cmd:         TCIOC_SETTIMEOUT
     * Description: Reset the timeout to this value
     * Argument:    A 32-bit timeout value in microseconds.
     *
     * TODO: pass pointer to uint64 ns? Need to determine if these timers
     * are 16 or 32 bit...
     */

    case TCIOC_SETTIMEOUT:
      {
        /* Set a new timeout value (and reset the timer) */

        if (dev->ops->settimeout) /* Optional */
          {
            ret = dev->ops->settimeout(dev, (uint32_t)arg);
          }
        else
          {
            ret = -ENOSYS;
          }
      }
      break;

    /* cmd:         TCIOC_NOTIFICATION
     * Description: Notify application via a signal when the timer expires.
     * Argument:    signal number
     *
     * NOTE: This ioctl cannot be support in the kernel build mode. In that
     * case direct callbacks from kernel space into user space is forbidden.
     */

    case TCIOC_NOTIFICATION:
      {
        FAR struct timer_notify_s *notify =
          (FAR struct timer_notify_s *)((uintptr_t)arg);

        if (notify != NULL)
          {
            dev->signo = notify->signo;
            dev->pid   = notify->pid;
            dev->arg   = notify->arg;

            ret = timer_setcallback((FAR void *)dev, timer_notifier, dev);
          }
        else
          {
            ret = -EINVAL;
          }
      }
      break;
#endif
    /* Any unrecognized IOCTL commands might be platform-specific ioctl commands */

    default:
      {
        tmrinfo("Forwarding unrecognized cmd: %d arg: %ld\n", cmd, arg);

        /* An ioctl commands that are not recognized by the "upper-half"
         * driver are forwarded to the lower half driver through this
         * method.
         */

        if (dev->ops->ioctl) /* Optional */
          {
            ret = dev->ops->ioctl(dev, cmd, arg);
          }
        else
          {
            ret = -ENOSYS;
          }
      }
      break;
    }

  return ret;
}

#if 0
/****************************************************************************
 * Name: hptc_receive
 ****************************************************************************/

static int hptc_receive(FAR struct hptc_dev_s *dev, uint8_t ch, uint8_t flags, int32_t sec, int32_t nsec)
{
  FAR struct hptc_ic_fifo_s *fifo = &dev->ic_recv;
  int                    nexttail;
  int                    errcode = -ENOMEM;

  /* Check if adding this new message would over-run the drivers ability to enqueue
   * read data.
   */

  nexttail = fifo->tail + 1;
  if (nexttail >= CONFIG_HPTC_IC_FIFOSIZE)
    {
      nexttail = 0;
    }

  /* Refuse the new data if the FIFO is full */

  if (nexttail != fifo->head)
    {
      /* Add the new, decoded ADC sample at the tail of the FIFO */
	  fifo->buffer[fifo->tail].sec     = sec;
	  fifo->buffer[fifo->tail].nsec    = nsec;
      fifo->buffer[fifo->tail].flags   = flags;
      fifo->buffer[fifo->tail].channel = ch;

      /* Increment the tail of the circular buffer */

      fifo->tail = nexttail;

      hptc_notify(dev);

      errcode = OK;
    }

  return errcode;
}
#endif

#if 0
/****************************************************************************
 * Name: hptc_pollnotify
 ****************************************************************************/

#ifndef CONFIG_DISABLE_POLL
static void hptc_pollnotify(FAR struct hptc_dev_s *dev, uint32_t type)
{
  int i;

  for (i = 0; i < CONFIG_HPTC_NPOLLWAITERS; i++)
    {
      struct pollfd *fds = dev->fds[i];
      if (fds)
        {
          fds->revents |= type;
          nxsem_post(fds->sem);
        }
    }
}
#endif
#endif

#if 0
/****************************************************************************
 * Name: hptc_notify
 ****************************************************************************/

static void hptc_notify(FAR struct hptc_dev_s *dev)
{
  FAR struct hptc_ic_fifo_s *fifo = &dev->ic_recv;

  /* If there are threads waiting for read data, then signal one of them
   * that the read data is available.
   */

  if (dev->nrxwaiters > 0)
    {
      nxsem_post(&fifo->sem);
    }

  /* If there are threads waiting on poll() for data to become available,
   * then wake them up now.
   */
#if 0
#ifndef CONFIG_DISABLE_POLL
   adc_pollnotify(dev, POLLIN);
#endif
#endif
}

#endif

#if 0
/************************************************************************************
 * Name: adc_poll
 ************************************************************************************/

#ifndef CONFIG_DISABLE_POLL
static int hptc_poll(FAR struct file *filep, struct pollfd *fds, bool setup)
{
  FAR struct inode     *inode = filep->f_inode;
  FAR struct hptc_dev_s *dev   = inode->i_private;
  irqstate_t flags;
  int ret = 0;
  int i;

  /* Interrupts must be disabled while accessing the list of poll structures
   * and ad_recv FIFO.
   */

  flags = enter_critical_section();

  if (setup)
    {
      /* Ignore waits that do not include POLLIN */

      if ((fds->events & POLLIN) == 0)
        {
          ret = -EDEADLK;
          goto return_with_irqdisabled;
        }

      /* This is a request to set up the poll.  Find an available
       * slot for the poll structure reference
       */

      for (i = 0; i < CONFIG_ADC_NPOLLWAITERS; i++)
        {
          /* Find an available slot */

          if (!dev->fds[i])
            {
              /* Bind the poll structure and this slot */

              dev->fds[i] = fds;
              fds->priv   = &dev->fds[i];
              break;
            }
        }

      if (i >= CONFIG_ADC_NPOLLWAITERS)
        {
          fds->priv    = NULL;
          ret          = -EBUSY;
          goto return_with_irqdisabled;
        }

      /* Should we immediately notify on any of the requested events? */

      if (dev->ad_recv.af_head != dev->ad_recv.af_tail)
        {
          adc_pollnotify(dev, POLLIN);
        }
    }
  else if (fds->priv)
    {
      /* This is a request to tear down the poll. */

      struct pollfd **slot = (struct pollfd **)fds->priv;

      /* Remove all memory of the poll setup */

      *slot                = NULL;
      fds->priv            = NULL;
    }

return_with_irqdisabled:
  leave_critical_section(flags);
  return ret;
}
#endif
#endif

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: hptc_register
 *
 * Description:
 *   This function binds an instance of a "lower half" timer driver with the
 *   "upper half" timer device and registers that device so that can be used
 *   by application code.
 *
 *   When this function is called, the "lower half" driver should be in the
 *   disabled state (as if the stop() method had already been called).
 *
 * Input parameters:
 *   dev path - The full path to the driver to be registers in the NuttX
 *     pseudo-filesystem.  The recommended convention is to name all timer
 *     drivers as "/dev/hptc0", "/dev/hptc1", etc.  where the driver
 *     path differs only in the "minor" number at the end of the device name.
 *   lower - A pointer to an instance of lower half timer driver.  This
 *     instance is bound to the timer driver and must persists as long as
 *     the driver persists.
 *
 * Returned Value:
 *   On success, a non-NULL handle is returned to the caller.  In the event
 *   of any failure, a NULL value is returned.
 *
 ****************************************************************************/

FAR void *hptc_register(FAR const char *path,
                         FAR struct hptc_dev_s *dev)
{
  //FAR struct hptc_upperhalf_s *upper;
  int ret;

  DEBUGASSERT(path && dev);
  tmrinfo("Entry: path=%s\n", path);
#if 0
  /* Allocate the upper-half data structure */

  upper = (FAR struct hptc_upperhalf_s *)
    kmm_zalloc(sizeof(struct hptc_upperhalf_s));
  if (!upper)
    {
      tmrerr("ERROR: Upper half allocation failed\n");
      goto errout;
    }

  /* Initialize the timer device structure (it was already zeroed
   * by kmm_zalloc()).
   */

  upper->dev = dev;
#endif
  /* Copy the registration path */

  dev->path = strdup(path);
  if (!dev->path)
    {
      tmrerr("ERROR: Path allocation failed\n");
      goto errout;
    }

  /* init the semaphore */
  ret = sem_init(&dev->sem, 0, 0);
  if (ret < 0)
    {
      tmrerr("ERROR: sem_init failed: %d\n", ret);
      goto errout_with_path;
    }
  ret = sem_setprotocol(&dev->sem, SEM_PRIO_NONE);
  if (ret < 0)
    {
      tmrerr("ERROR: sem_setprotocol failed: %d\n", ret);
      goto errout_with_sem;
    }
#if 0
  ret = sem_init(&dev->ic_recv.sem, 0, 0);
  if (ret < 0)
    {
      tmrerr("ERROR: sem_init failed: %d\n", ret);
      goto errout_with_sem;
    }
  ret = sem_setprotocol(&dev->ic_recv.sem, SEM_PRIO_NONE);
  if (ret < 0)
    {
      tmrerr("ERROR: sem_setprotocol failed: %d\n", ret);
      goto errout_with_2sem;
    }


  /* bind input capture callback */
  ret = dev->ops->setinputcallback(dev, (hptc_in_cb_t) hptc_receive);
  if (ret < 0)
    {
      tmrerr("ERROR: Failed to bind callback: %d\n", ret);
      goto errout_with_2sem;
    }
#endif

  /* Register the timer device */

  ret = register_driver(path, &g_hptcops, 0666, dev);
  if (ret < 0)
    {
      tmrerr("ERROR: register_driver failed: %d\n", ret);
      goto errout_with_sem;
    }

  //FIXME: workaround
  g_dev = dev;

  return (FAR void *)dev;

#if 0
errout_with_2sem:
    sem_destroy(&dev->ic_recv.sem);
#endif

errout_with_sem:
  sem_destroy(&dev->sem);
errout_with_path:
  kmm_free(dev->path);
#if 0
errout_with_upper:
  kmm_free(upper);
#endif
errout:
  return NULL;
}

/****************************************************************************
 * Name: hptc_unregister
 *
 * Description:
 *   This function can be called to disable and unregister the timer
 *   device driver.
 *
 * Input parameters:
 *   handle - This is the handle that was returned by timer_register()
 *
 * Returned Value:
 *   None
 *
 ****************************************************************************/

void hptc_unregister(FAR void *handle)
{
  //FAR struct hptc_upperhalf_s *upper;
  FAR struct hptc_dev_s *dev;

  /* Recover the pointer to the upper-half driver state */

  dev = (FAR struct hptc_dev_s *)handle;
  DEBUGASSERT(dev != NULL);

  tmrinfo("Unregistering: %s\n", dev->path);

  /* Disable the timer */

  DEBUGASSERT(dev->ops->stop); /* Required */
  (void)dev->ops->stop(dev);

  /* Unregister the timer device */

  (void)unregister_driver(dev->path);

  /* Then free all of the driver resources */
  sem_destroy(&dev->sem);

  kmm_free(dev->path);
  //kmm_free(upper);
}

CODE int (*setasyncinputcallback)(CODE hptc_async_in_cb_t callback);

int hptc_set_async_input_callback(hptc_async_in_cb_t callback){
      int ret;
      tmrinfo("set async callback\n");
	  /* bind input capture callback */
	  ret = g_dev->ops->setasyncinputcallback((hptc_async_in_cb_t) callback);
	  if (ret < 0)
	    {
	      tmrerr("ERROR: Failed to bind callback: %d\n", ret);
	    }

      return ret;


}


#if 0 /* XXX */
/****************************************************************************
 * Name: timer_setcallback
 *
 * Description:
 *   This function can be called to add a callback into driver-related code
 *   to handle timer expirations.  This is a strictly OS internal interface
 *   and may NOT be used by appliction code.
 *
 * Input parameters:
 *   handle   - This is the handle that was returned by timer_register()
 *   callback - The new timer interrupt callback
 *   arg      - Argument to be provided with the callback
 *
 * Returned Value:
 *   None
 *
 ****************************************************************************/

int timer_setcallback(FAR void *handle, tccb_t callback, FAR void *arg)
{
  FAR struct hptc_upperhalf_s *upper;
  FAR struct hptc_dev_s *dev;

  /* Recover the pointer to the upper-half driver state */

  upper = (FAR struct hptc_upperhalf_s *)handle;
  DEBUGASSERT(upper != NULL && upper->dev != NULL);
  dev = upper->dev;
  DEBUGASSERT(dev->ops != NULL);

  /* Check if the lower half driver supports the setcallback method */

  if (dev->ops->setcallback != NULL) /* Optional */
    {
      /* Yes.. Defer the hander attachment to the lower half driver */

      dev->ops->setcallback(dev, callback, arg);
      return OK;
    }

  return -ENOSYS;
}
#endif /* XXX */
#endif /* CONFIG_HPTC */
