/************************************************************************************
 * arch/arm/src/stm32/stm32_messBUSClient.h
 *
 *   Author: Peer Ulbig <p.ulbig@tu-braunschweig.de>
 *
 ************************************************************************************/

#ifndef _ARCH_ARM_SRC_STM32F7_STM32_MESSBUSCLIENT_H_
#define _ARCH_ARM_SRC_STM32F7_STM32_MESSBUSCLIENT_H_

/****************************************************************************
 * Includes
 ****************************************************************************/

#include <sys/types.h>
#include <arch/board/board.h>
#include <arch/chip/chip.h>
#include <nuttx/messBUS/messBUSClient.h>
#include "stm32_dma.h"


/****************************************************************************
 * Debug helpers during development of driver (helpful with oscilloscope)
 ****************************************************************************/

#define mB_DEBUG_VISUALIZE_INTERRUPTS						0
#define mB_DEBUG_VISUALIZE_SYNC_TRIGGER_WITH_TIM1_OPM		0

/****************************************************************************
 * Helper #defines
 ****************************************************************************/

#ifndef US_PER_SEC
#define US_PER_SEC   (1000000)
#endif


#define mB_SEC_2_USEC		1000000ul
#define mB_USEC_2_NSEC		1000



/****************************************************************************
 * messBUS
 ****************************************************************************/

/* messBUS SYNC duration is fixed to 5us */


/* Maximum supported slots and channels.
 * Usually you would need two actions per slot except for writes where there is
 * a further Data Enable action required before. */
#define mB_MAX_TX_SLOTS		10
#define mB_MAX_RX_SLOTS		10

/* Normal operation modes of timer interrupt handler */
#define mB_CH1_RX_START		10
#define mB_CH1_RX_END		11
#define mB_CH1_TX_DE		12
#define mB_CH1_TX_START		13
#define mB_CH1_TX_END		14
#define mB_CH1_WAKE_UP		15
#define mB_CH1_DONE			16

/****************************************************************************
 * Timer (SYNC Catcher + ISR)
 ****************************************************************************/

/*
 * messBUS node need to begin offset microseconds before specified rx slot begin with
 * its preparation to ensure that it is really ready to read data at slot begin.
 */
#define mB_RX_OFFSET_US     (-1)
/*
 * messBUS node TX_OFFSET_US for transmission afer data enable is set
 */
#define mB_TX_OFFSET_US		(1)

/* General */
#define mB_SYNC_US			 (5)
#define mB_SYNC_TOLERANCE_US (1)

#define mB_CH1_TIM_SYNC_CHECK_OFFSET_US (7)
#define mB_CH1_TIM_SYNC_TRGO_OFFSET_US (10)
#define mB_CH1_TIM_SYNC_UPDATE_OFFSET_US (14)

#define mB_CH1_TIM_PRESYNC_US	(2)						// Presync us when trigger event shall occur
#define mB_CH1_TIM_PRESYNC_IDLE_US (3)					//idle time before presync trigger -> also set trigger
#define mB_TIMESLICE_US		(10000)

#define mB_TIMESLICE_FREQ (US_PER_SEC/mB_TIMESLICE_US)


/****************************************************************************
 * DMA for USART6 Rx
 ****************************************************************************/

#if (mB_CH1_RX_DMAMAP == DMAMAP_USART6_RX_1) /*DMA2,DMA_STREAM1,DMA_CHAN5*/
	#define mB_CH1_RX_DMA_SxNDTR		STM32_DMA2_S1NDTR
	#define mB_CH1_RX_DMA_SxCR		STM32_DMA2_S1CR
	#define mB_CH1_RX_DMA_xIFCR		STM32_DMA2_LIFCR
	#define mB_CH1_RX_DMA_SxMOAR		STM32_DMA2_S1M0AR
	#define mB_CH1_RX_DMA_INT_SxMASK	DMA_INT_STREAM1_MASK
#elif (mB_CH1_RX_DMAMAP == DMAMAP_USART6_RX_2) /*DMA2,DMA_STREAM2,DMA_CHAN5*/
	#define mB_CH1_RX_DMA_SxNDTR		STM32_DMA2_S2NDTR
	#define mB_CH1_RX_DMA_SxCR		STM32_DMA2_S2CR
	#define mB_CH1_RX_DMA_xIFCR		STM32_DMA2_LIFCR
	#define mB_CH1_RX_DMA_SxMOAR		STM32_DMA2_S2M0AR
	#define mB_CH1_RX_DMA_INT_SxMASK	DMA_INT_STREAM2_MASK
#endif


/****************************************************************************
 * DMA for USART1 Rx
 ****************************************************************************/

#if (mB_CH1_RX_DMAMAP == DMAMAP_USART1_RX_1)  /*DMA2,DMA_STREAM2,DMA_CHAN4*/
	#define mB_CH1_RX_DMA_SxNDTR		STM32_DMA2_S2NDTR
	#define mB_CH1_RX_DMA_SxCR			STM32_DMA2_S2CR
	#define mB_CH1_RX_DMA_xIFCR			STM32_DMA2_LIFCR
	#define mB_CH1_RX_DMA_SxMOAR		STM32_DMA2_S2M0AR
	#define mB_CH1_RX_DMA_INT_SxMASK	DMA_INT_STREAM2_MASK
#elif (mB_CH1_RX_DMAMAP == DMAMAP_USART1_RX_2) /*DMA2,DMA_STREAM5,DMA_CHAN4*/
	#define mB_CH1_RX_DMA_SxNDTR		STM32_DMA2_S5NDTR
	#define mB_CH1_RX_DMA_SxCR			STM32_DMA2_S5CR
	#define mB_CH1_RX_DMA_xIFCR			STM32_DMA2_HIFCR
	#define mB_CH1_RX_DMA_SxMOAR		STM32_DMA2_S5M0AR
	#define mB_CH1_RX_DMA_INT_SxMASK	DMA_INT_STREAM5_MASK
#endif






/****************************************************************************
 * DMA for USART1 Tx
 ****************************************************************************/

#if (mB_CH1_TX_DMAMAP == DMAMAP_USART1_TX) /*DMA2,DMA_STREAM7,DMA_CHAN4*/
	#define mB_CH1_TX_DMA_SxNDTR		STM32_DMA2_S7NDTR
	#define mB_CH1_TX_DMA_SxCR		STM32_DMA2_S7CR
	#define mB_CH1_TX_DMA_xIFCR		STM32_DMA2_HIFCR
	#define mB_CH1_TX_DMA_SxMOAR		STM32_DMA2_S7M0AR
	#define mB_CH1_TX_DMA_INT_SxMASK	DMA_INT_STREAM7_MASK
#endif

/****************************************************************************
 * DMA for USART6 Tx
 ****************************************************************************/

#if (mB_CH1_TX_DMAMAP == DMAMAP_USART6_TX_1) /*DMA2,DMA_STREAM6,DMA_CHAN5*/
	#define mB_CH1_TX_DMA_SxNDTR		STM32_DMA2_S6NDTR
	#define mB_CH1_TX_DMA_SxCR		STM32_DMA2_S6CR
	#define mB_CH1_TX_DMA_xIFCR		STM32_DMA2_HIFCR
	#define mB_CH1_TX_DMA_SxMOAR		STM32_DMA2_S6M0AR
	#define mB_CH1_TX_DMA_INT_SxMASK	DMA_INT_STREAM6_MASK
#elif (mB_CH1_TX_DMAMAP == DMAMAP_USART6_TX_2) /*DMA2,DMA_STREAM7,DMA_CHAN5*/
	#define mB_CH1_TX_DMA_SxNDTR		STM32_DMA2_S7NDTR
	#define mB_CH1_TX_DMA_SxCR		STM32_DMA2_S7CR
	#define mB_CH1_TX_DMA_xIFCR		STM32_DMA2_HIFCR
	#define mB_CH1_TX_DMA_SxMOAR		STM32_DMA2_S7M0AR
	#define mB_CH1_TX_DMA_INT_SxMASK	DMA_INT_STREAM7_MASK
#endif


/****************************************************************************
 * Resources for USART6
 ****************************************************************************/
#if (STM32_mB_CH1_USART_BASE == STM32_USART6_BASE)
#if 0 /*TODO: check correct configuration*/
#error USART6 used
#endif

#if defined(CONFIG_STM32_STM32F4XXX)
	#define mB_CH1_PCLKx_FREQUENCY              STM32_PCLK2_FREQUENCY
#endif
	#define STM32_mB_CH1_DMA_RCC_AHBxENR		STM32_RCC_AHB1ENR
    #define mB_CH1_RCC_AHBxENR_DMAyEN   		RCC_AHB1ENR_DMA2EN

	#define STM32_mB_CH1_USART_RCC_APBxENR		STM32_RCC_APB2ENR
	#define mB_CH1_RCC_APBxENR_USARTyEN			RCC_APB2ENR_USART6EN


	#define STM32_mB_CH1_TIM_RCC_APBxENR		STM32_RCC_APB1ENR
	#define mB_CH1_RCC_APBxENR_TIMyEN			RCC_APB1ENR_TIM3EN

	/*TIM3*/


	#define mB_CH1_TIM_IRQ			    STM32_IRQ_TIM3
	#define mB_CH1_TIM_CLKIN	        STM32_APB1_TIM3_CLKIN
	#define STM32_mB_CH1_TIM_BASE				STM32_TIM3_BASE

	/*PC7*/
	#define STM32_mB_CH1_TX_GPIO_AFRx	STM32_GPIOC_AFRL
	#define mB_CH1_RX_AFRx_MASK			GPIO_AFRL7_MASK
	#define mB_CH1_RX_AFRx_USART        (8 << GPIO_AFRL7_SHIFT) /*AF8*/
	#define mB_CH1_RX_AFRx_TIM			(2 << GPIO_AFRL7_SHIFT) /*AF2*/


#endif

/****************************************************************************
 * Resources for USART1
 ****************************************************************************/
#if (STM32_mB_CH1_USART_BASE == STM32_USART1_BASE)
#if 0 /*TODO: check correct configuration*/
#error USART1 used
#endif

#if defined(CONFIG_STM32_STM32F4XXX)
	#define mB_CH1_PCLKx_FREQUENCY              STM32_PCLK2_FREQUENCY
#endif
	#define STM32_mB_CH1_DMA_RCC_AHBxENR		STM32_RCC_AHB1ENR
    #define mB_CH1_RCC_AHBxENR_DMAyEN   		RCC_AHB1ENR_DMA2EN

	#define STM32_mB_CH1_USART_RCC_APBxENR		STM32_RCC_APB2ENR
	#define mB_CH1_RCC_APBxENR_USARTyEN			RCC_APB2ENR_USART1EN


	#define STM32_mB_CH1_TIM_RCC_APBxENR		STM32_RCC_APB1ENR
	#define mB_CH1_RCC_APBxENR_TIMyEN			RCC_APB1ENR_TIM4EN

	/*TIM4*/
	#define mB_CH1_TIM_IRQ			    STM32_IRQ_TIM4
	#define mB_CH1_TIM_CLKIN	        STM32_APB1_TIM4_CLKIN
	#define STM32_mB_CH1_TIM_BASE		STM32_TIM4_BASE

	/*PB7*/
	#define STM32_mB_CH1_RX_GPIO_AFRx	STM32_GPIOB_AFRL
	#define mB_CH1_RX_AFRx_MASK			GPIO_AFRL7_MASK
	#define mB_CH1_RX_AFRx_USART        (7 << GPIO_AFRL7_SHIFT) /*AF7*/
	#define mB_CH1_RX_AFRx_TIM			(2 << GPIO_AFRL7_SHIFT) /*AF2*/


#endif




/*************************************************************************************
 * USART
 *************************************************************************************/
#  define STM32_mB_CH1_USART_CR1        (STM32_mB_CH1_USART_BASE+STM32_USART_CR1_OFFSET)
#  define STM32_mB_CH1_USART_CR2        (STM32_mB_CH1_USART_BASE+STM32_USART_CR2_OFFSET)
#  define STM32_mB_CH1_USART_CR3        (STM32_mB_CH1_USART_BASE+STM32_USART_CR3_OFFSET)
#  define STM32_mB_CH1_USART_BRR        (STM32_mB_CH1_USART_BASE+STM32_USART_BRR_OFFSET)
#  define STM32_mB_CH1_USART_GTPR       (STM32_mB_CH1_USART_BASE+STM32_USART_GTPR_OFFSET)
#  define STM32_mB_CH1_USART_RTOR       (STM32_mB_CH1_USART_BASE+STM32_USART_RTOR_OFFSET)
#  define STM32_mB_CH1_USART_RQR        (STM32_mB_CH1_USART_BASE+STM32_USART_RQR_OFFSET)
#  define STM32_mB_CH1_USART_GTPR       (STM32_mB_CH1_USART_BASE+STM32_USART_GTPR_OFFSET)
#  define STM32_mB_CH1_USART_ISR        (STM32_mB_CH1_USART_BASE+STM32_USART_ISR_OFFSET)
#  define STM32_mB_CH1_USART_ICR        (STM32_mB_CH1_USART_BASE+STM32_USART_ICR_OFFSET)
#  define STM32_mB_CH1_USART_RDR        (STM32_mB_CH1_USART_BASE+STM32_USART_RDR_OFFSET)
#  define STM32_mB_CH1_USART_TDR        (STM32_mB_CH1_USART_BASE+STM32_USART_TDR_OFFSET)


/*************************************************************************************
 * Timer
 *************************************************************************************/
#  define STM32_mB_CH1_TIM_CR1          (STM32_mB_CH1_TIM_BASE+STM32_GTIM_CR1_OFFSET)
#  define STM32_mB_CH1_TIM_CR2          (STM32_mB_CH1_TIM_BASE+STM32_GTIM_CR2_OFFSET)
#  define STM32_mB_CH1_TIM_SMCR         (STM32_mB_CH1_TIM_BASE+STM32_GTIM_SMCR_OFFSET)
#  define STM32_mB_CH1_TIM_DIER         (STM32_mB_CH1_TIM_BASE+STM32_GTIM_DIER_OFFSET)
#  define STM32_mB_CH1_TIM_SR           (STM32_mB_CH1_TIM_BASE+STM32_GTIM_SR_OFFSET)
#  define STM32_mB_CH1_TIM_EGR          (STM32_mB_CH1_TIM_BASE+STM32_GTIM_EGR_OFFSET)
#  define STM32_mB_CH1_TIM_CCMR1        (STM32_mB_CH1_TIM_BASE+STM32_GTIM_CCMR1_OFFSET)
#  define STM32_mB_CH1_TIM_CCMR2        (STM32_mB_CH1_TIM_BASE+STM32_GTIM_CCMR2_OFFSET)
#  define STM32_mB_CH1_TIM_CCER         (STM32_mB_CH1_TIM_BASE+STM32_GTIM_CCER_OFFSET)
#  define STM32_mB_CH1_TIM_CNT          (STM32_mB_CH1_TIM_BASE+STM32_GTIM_CNT_OFFSET)
#  define STM32_mB_CH1_TIM_PSC          (STM32_mB_CH1_TIM_BASE+STM32_GTIM_PSC_OFFSET)
#  define STM32_mB_CH1_TIM_ARR          (STM32_mB_CH1_TIM_BASE+STM32_GTIM_ARR_OFFSET)
#  define STM32_mB_CH1_TIM_CCR1         (STM32_mB_CH1_TIM_BASE+STM32_GTIM_CCR1_OFFSET)
#  define STM32_mB_CH1_TIM_CCR2         (STM32_mB_CH1_TIM_BASE+STM32_GTIM_CCR2_OFFSET)
#  define STM32_mB_CH1_TIM_CCR3         (STM32_mB_CH1_TIM_BASE+STM32_GTIM_CCR3_OFFSET)
#  define STM32_mB_CH1_TIM_CCR4         (STM32_mB_CH1_TIM_BASE+STM32_GTIM_CCR4_OFFSET)
#  define STM32_mB_CH1_TIM_DCR          (STM32_mB_CH1_TIM_BASE+STM32_GTIM_DCR_OFFSET)
#  define STM32_mB_CH1_TIM_DMAR         (STM32_mB_CH1_TIM_BASE+STM32_GTIM_DMAR_OFFSET)


/* High resolution settings (SYNC) */
#define mB_CH1_TIM_SYNC_PSC 	   			(0)    // Set Prescaler to zero to achieve highest resolution

/* scale to timeslice period to keep values in 32bit range */
#define mB_CH1_TIM_SYNC_NOM_TIMESLICE_TICKS ((mB_CH1_TIM_CLKIN) / (mB_TIMESLICE_FREQ))

#define mB_CH1_TIM_PRESYNC_TICKS			((                                        (mB_CH1_TIM_PRESYNC_US)    *    mB_CH1_TIM_SYNC_NOM_TIMESLICE_TICKS) / mB_TIMESLICE_US)
#define mB_CH1_TIM_SYNC_TICKS	   			((                                                   (mB_SYNC_US)    *    mB_CH1_TIM_SYNC_NOM_TIMESLICE_TICKS) / mB_TIMESLICE_US) 					// Nominal SYNC ticks (no clock deviation)
#define mB_CH1_TIM_SYNC_TICKS_MIN			((                       ((mB_SYNC_US) - (mB_SYNC_TOLERANCE_US) )    *    mB_CH1_TIM_SYNC_NOM_TIMESLICE_TICKS) / mB_TIMESLICE_US)
#define mB_CH1_TIM_SYNC_TICKS_MAX			((                       ((mB_SYNC_US) + (mB_SYNC_TOLERANCE_US) )    *    mB_CH1_TIM_SYNC_NOM_TIMESLICE_TICKS) / mB_TIMESLICE_US)
#define mB_CH1_TIM_SYNC_CHECK_OFFSET_TICKS  ((                              (mB_CH1_TIM_SYNC_CHECK_OFFSET_US)    *    mB_CH1_TIM_SYNC_NOM_TIMESLICE_TICKS) / mB_TIMESLICE_US)
#define mB_CH1_TIM_SYNC_TRGO_OFFSET_TICKS   ((                               (mB_CH1_TIM_SYNC_TRGO_OFFSET_US)    *    mB_CH1_TIM_SYNC_NOM_TIMESLICE_TICKS) / mB_TIMESLICE_US)
#define mB_CH1_TIM_SYNC_UPDATE_TICKS		(( ((mB_CH1_TIM_PRESYNC_US) + (mB_CH1_TIM_SYNC_UPDATE_OFFSET_US))    *    mB_CH1_TIM_SYNC_NOM_TIMESLICE_TICKS) / mB_TIMESLICE_US)


/* Low resolution settings (XMIT) */
#define mB_CH1_TIM_XMIT_TICKS_MAX           (65535)
#define mB_CH1_TIM_XMIT_NOM_TICKS_MAX       (mB_CH1_TIM_XMIT_TICKS_MAX - mB_CH1_TIM_XMIT_TICKS_MAX/10)  //leave 10 percent for clock deviation
#define mB_CH1_TIM_XMIT_NOM_FREQ_MAX        (mB_CH1_TIM_XMIT_NOM_TICKS_MAX*(US_PER_SEC / (mB_TIMESLICE_US - mB_CH1_TIM_PRESYNC_IDLE_US )))
#define mB_CH1_TIM_XMIT_PSC 	            ((((mB_CH1_TIM_CLKIN) + (mB_CH1_TIM_XMIT_NOM_FREQ_MAX-1)) / mB_CH1_TIM_XMIT_NOM_FREQ_MAX) - 1)	// Required prescaler setting according to clocking defined in board.h (with roundup)
#define mB_CH1_TIM_XMIT_NOM_FREQ            ((mB_CH1_TIM_CLKIN)/((mB_CH1_TIM_XMIT_PSC)+1))
#define mB_CH1_TIM_XMIT_NOM_TIMESLICE_TICKS (mB_CH1_TIM_XMIT_NOM_FREQ / mB_TIMESLICE_FREQ)
#define mB_CH1_TIM_XMIT_NOM_TICKS		    (mB_CH1_TIM_XMIT_NOM_TIMESLICE_TICKS-(mB_CH1_TIM_PRESYNC_IDLE_US*mB_CH1_TIM_XMIT_NOM_TIMESLICE_TICKS)/mB_TIMESLICE_US)


#define mB_CH1_TIM_XMIT_OFFSET_TICKS    ((mB_CH1_TIM_SYNC_UPDATE_TICKS)/((mB_CH1_TIM_XMIT_PSC)+1))






/****************************************************************************
 * Ch1 Data Enable pin
 ****************************************************************************/

/* Extract the pin number */
#define mB_CH1_DE_ON			(1 << (GPIO_mB_CH1_DE & GPIO_PIN_MASK))
#define mB_CH1_DE_OFF			(1 << ((GPIO_mB_CH1_DE & GPIO_PIN_MASK)+16))

/* Check for F4 or F7 architecture */
#if defined(CONFIG_ARCH_CHIP_STM32F7)
#define mB_NGPIO_PORTS	STM32F7_NGPIO
#else
#define mB_NGPIO_PORTS	STM32_NGPIO_PORTS
#endif

/* Get the right BSRR address according to port configuration */
#if mB_NGPIO_PORTS > 0
#if ((GPIO_mB_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTA)
#define mB_CH1_DE_BSRR		STM32_GPIOA_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 1
#if ((GPIO_mB_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTB)
#define mB_CH1_DE_BSRR		STM32_GPIOB_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 2
#if ((GPIO_mB_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTC)
#define mB_CH1_DE_BSRR		STM32_GPIOC_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 3
#if ((GPIO_mB_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTD)
#define mB_CH1_DE_BSRR		STM32_GPIOD_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 4
#if ((GPIO_mB_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTE)
#define mB_CH1_DE_BSRR		STM32_GPIOE_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 5
#if ((GPIO_mB_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTF)
#define mB_CH1_DE_BSRR		STM32_GPIOF_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 6
#if ((GPIO_mB_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTG)
#define mB_CH1_DE_BSRR		STM32_GPIOG_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 7
#if ((GPIO_mB_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTH)
#define mB_CH1_DE_BSRR		STM32_GPIOH_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 8
#if ((GPIO_mB_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTI)
#define mB_CH1_DE_BSRR		STM32_GPIOI_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 9
#if ((GPIO_mB_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTJ)
#define mB_CH1_DE_BSRR		STM32_GPIOJ_BSRR
#endif
#endif
#if mB_NGPIO_PORTS > 10
#if ((GPIO_mB_CH1_DE & GPIO_PORT_MASK) == GPIO_PORTK)
#define mB_CH1_DE_BSRR		STM32_GPIOK_BSRR
#endif
#endif

/****************************************************************************
 * Debug GPIOs
 ****************************************************************************/
#if 0  //TODO: move board specific code
/* This pin has to be belong to Port D */
#define GPIO_DEBUG_ISR     (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
							GPIO_OUTPUT_CLEAR|GPIO_PORTD|GPIO_PIN7)
#define GPIO_DEBUG_BSRR     STM32_GPIOD_BSRR
#define GPIO_DEBUG_ISR_ON	(1 << 7)
#define GPIO_DEBUG_ISR_OFF	(1 << (7+16))
#endif
#endif /* _ARCH_ARM_SRC_STM32F7_STM32_MESSBUSCLIENT_H_ */
