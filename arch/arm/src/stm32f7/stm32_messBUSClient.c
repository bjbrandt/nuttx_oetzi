/****************************************************************************
 * arch/arm/src/stm32/stm32_messBUSClient.c
 *
 *   Author: Peer Ulbig <p.ulbig@tu-braunschweig.de>
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/arch.h>
#include <nuttx/irq.h>
#include <arch/board/board.h>
#include <arch/chip/chip.h>
#include <arch/irq.h>
#include <arch/chip/irq.h>
#include <debug.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <sched.h>
#include <stdio.h>						// printf

#include "up_arch.h" 					// putreg32, modifyreg16 etc.
#include "chip.h"
#include "stm32_gpio.h"
#include "chip/stm32_tim.h"
#include "chip/stm32_gpio.h"
#include "chip/stm32_uart.h"
#include "up_internal.h"
#include "ram_vectors.h"
#include "nvic.h"
#include "stm32_dma.h"
#include "stm32_rcc.h"
#include "stm32_uart.h"
#include "stm32_tim.h"
#include "stm32_hptc.h"

#ifdef CONFIG_ARCH_CHIP_STM32F7
	#include <nuttx/cache.h>
#endif
#include "stm32_messBUSClient.h"

#include <nuttx/messBUS/messBUS_led.h> //debug LEDs

/* Header file which is also used in application and board specific bringup.c */
#include <nuttx/messBUS/messBUSClient.h>


#define STM32_MB_ISR_MODE_SWITCH_CNT 100
#define STM32_MB_ACTIVE_ISR_MODE_SWITCH_CNT 10

//#define mB_DEBUG_LED 1

/****************************************************************************
 * Non-atomic hardware access helper functions
 ****************************************************************************/

/****************************************************************************
 * Name: setbit32
 *
 * Description:
 *   Set a single bit in 32-bit register *target with read-modify-write operation.
 *   Suppress compiler optimizations using volatile and avoid extra clock
 *   cycles for function call by declaring the function always inline.
 *
 ****************************************************************************/

static inline void setbit32(unsigned int target, uint32_t bit) __attribute__((always_inline));
static inline void setbit32(unsigned int target, uint32_t bit){
	*(volatile uint32_t *)(target) |= (bit);
};

/****************************************************************************
 * Name: clearbit32
 *
 * Description:
 *   Clear a single bit in 32-bit register *target with read-modify-write operation.
 *   Suppress compiler optimizations using volatile and avoid extra clock
 *   cycles for function call by declaring the function always inline.
 *
 ****************************************************************************/

static inline void clearbit32(unsigned int target, uint32_t bit) __attribute__((always_inline));
static inline void clearbit32(unsigned int target, uint32_t bit){
	*(volatile uint32_t *)(target) &= ~(bit);
};

/****************************************************************************
 * Name: modifybit32
 *
 * Description:
 *   Modify a single bit in 32-bit register *target with read-modify-write operation.
 *   Suppress compiler optimizations using volatile and avoid extra clock
 *   cycles for function call by declaring the function always inline.
 *
 ****************************************************************************/

static inline void modifybit32(unsigned int target, uint32_t clearbits, uint32_t setbits) __attribute__((always_inline));
static inline void modifybit32(unsigned int target, uint32_t clearbits, uint32_t setbits){
//	putreg32(target,(getreg32(target) & ~clearbits) | setbits);
	clearbit32(target, clearbits);
	setbit32(target,setbits);
};


/****************************************************************************
 * Name: setbit16
 *
 * Description:
 *   Set a single bit in 16-bit register *target with read-modify-write operation.
 *   Suppress compiler optimizations using volatile and avoid extra clock
 *   cycles for function call by declaring the function always inline.
 *
 ****************************************************************************/

static inline void setbit16(unsigned int target, uint16_t bit) __attribute__((always_inline));
static inline void setbit16(unsigned int target, uint16_t bit){
	*(volatile uint16_t *)(target) |= (bit);
};

/****************************************************************************
 * Name: clearbit16
 *
 * Description:
 *   Clear a single bit in 16-bit register *target with read-modify-write operation.
 *   Suppress compiler optimizations using volatile and avoid extra clock
 *   cycles for function call by declaring the function always inline.
 *
 ****************************************************************************/

static inline void clearbit16(unsigned int target, uint16_t bit) __attribute__((always_inline));
static inline void clearbit16(unsigned int target, uint16_t bit){
	*(volatile uint16_t *)(target) &= ~(bit);
};

/****************************************************************************
 * Name: modifybit16
 *
 * Description:
 *   Modify a single bit in 16-bit register *target with read-modify-write operation.
 *   Suppress compiler optimizations using volatile and avoid extra clock
 *   cycles for function call by declaring the function always inline.
 *
 ****************************************************************************/

static inline void modifybit16(unsigned int target, uint16_t clearbits, uint16_t setbits) __attribute__((always_inline));
static inline void modifybit16(unsigned int target, uint16_t clearbits, uint16_t setbits){
//	putreg16(target,(getreg16(target) & ~clearbits) | setbits);
	clearbit16(target, clearbits);
	setbit16(target,setbits);
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

static uint16_t up_baud_to_brr (uint32_t baud);
static void up_convert_slotlist(void *slotlist, void *actiontable, void *infotable);
static void pendsv_handler(void);
static void mB_ch1_tim_handler(void);
static int up_setup(void);
static int up_shutdown(void);
static int up_start(void);
static int up_stop(void);
static void up_update_buffers(void);
static int up_get_clock_deviation(void);

/****************************************************************************
 * Private Types
 ****************************************************************************/

/* This struct contains the timing and op_mode information for a single
 * action in interrupt context.
 */
struct up_isr_action_s
{
	uint16_t 	nom_ticks;		// Amount of nominal timer ticks (250ns period) without correction for real period yet
	uint8_t		op_mode;		// Next ISR operation mode / case
};

/* This struct contains the data related information of a single slot.
 * This struct can be used in more than one action and is thus seperated
 * from the action structs.
 */
struct up_isr_info_s
{
	uint16_t	brr;
	char		*buffer;
	uint16_t	buf_length;
	uint16_t	*buf_length_ptr;
};

/****************************************************************************
 * Private Data
 ****************************************************************************/

/* Arch-specific instance of messBUS_ops_s, which will be attached to
 * g_messBUSpriv and thus to the upper half driver.
 */
static const struct messBUS_ops_s g_messBUS_ops =
{
  .setup			= up_setup,
  .shutdown			= up_shutdown,
  .start			= up_start,
  .stop				= up_stop,
  .convert_slotlist	= up_convert_slotlist,
  .update_buffers	= up_update_buffers,
  .get_clock_deviation	= up_get_clock_deviation
};

/* Default operation mode for next action */
static volatile uint8_t g_op_mode = 0;

/* Consecutive Sync errors */
static volatile uint8_t g_sync_err = 0;

/* Consecutive phase errors */
static volatile uint8_t g_phase_err = 0;

/* Timer 3 CCR1 and CCR2 values (to synchronize with SYNC pulse) */
static volatile uint16_t g_mB_ch1_tim_ccr1 = 0;
static volatile uint16_t g_mB_ch1_tim_ccr2 = 0;
static volatile uint16_t g_mB_ch1_tim_ccr3 = 0;

/* Update event ticks. This will be the Timer 3's ARR value, which will
 * also contain clock correction if necessary.
 */
static volatile uint16_t g_ue_ticks = 0;

/* Action tables and respective pointer. We always need a further action for
 * SYNC preparation (case 2) at the end of each timeslice.
 */
static struct up_isr_action_s isr_actiontable0[((2 * CONFIG_MESSBUS_CH1_MAX_RX_SLOTS) + (3 * CONFIG_MESSBUS_CH1_MAX_TX_SLOTS) + 2)];
static struct up_isr_action_s isr_actiontable1[((2 * CONFIG_MESSBUS_CH1_MAX_RX_SLOTS) + (3 * CONFIG_MESSBUS_CH1_MAX_TX_SLOTS) + 2)];
static struct up_isr_action_s *isr_actiontable_ptr = isr_actiontable0;

/* Isr info tables, one per each slot */
struct up_isr_info_s isr_infotable0[(CONFIG_MESSBUS_CH1_MAX_RX_SLOTS + CONFIG_MESSBUS_CH1_MAX_TX_SLOTS)] = {0};
struct up_isr_info_s isr_infotable1[(CONFIG_MESSBUS_CH1_MAX_RX_SLOTS + CONFIG_MESSBUS_CH1_MAX_TX_SLOTS)] = {0};
static struct up_isr_info_s *isr_infotable_ptr = isr_infotable0;

/* Callback infos */
struct channel_single_callback_info_s ch1_single_callback_info = {0, 0, MESSBUS_NO_CALLBACK_REQUEST, 0};
struct channel_wake_up_callback_info_s ch1_wake_up_callback_info = {0, 0, 0, 0, 0};
static struct callbacks_info_s callbacks_info = {&ch1_single_callback_info, &ch1_wake_up_callback_info};

/* Arch-specific instance of messBUS_dev_t, which will be attached to the
 * upper half driver. This struct is used to bind the lower half to its
 * upper half and to share data between them.
 */
static messBUS_dev_t g_messBUSpriv =
{
	/* This variable indicates whether the device has already been opened
	 * or not. Default is closed (0).
	 */
	.open = 0,

	/* This variable indicates whether the driver is running or paused.
	 * A user always needs to open the device first and attach a slotlist
	 * via ioctl before he can actually start the driver. Default is
	 * paused (0).
	 */
	.running = 0,

	/* This variable is incremented by the interrupt handler after each
	 * successful SYNC. Higher logic can poll for this variable to become
	 * non-zero when it waits for the next timeslice. If the value of this
	 * variable is > 1 higher logic can recognise that it missed x time-
	 * slices.
	 */
	.sync_count = 0,

	/* This variable indicates whether the interrupt handler currently
	 * points to tables with index 0 or 1. It is modified by the interrupt
	 * handler. The interrupt handler only changes tables when there are
	 * new tables available. The unused tables can be modified by higher
	 * logic.
	 */
	.active_table = 0,

	/* This variable indicates that new tables are available. It is set by
	 * higher logic and cleared by the interrupt handler after the current
	 * time slice. If set to one by higher logic the interrupt handler will
	 * point to the modified tables after the current time slice.
	 */
	.new_table_avail = 0,

	/* This variable contains the chronological number of the last slot,
	 * that has been completed by channel 1.
	 */
	.ch1_last_processed_slot_number = 0,

	/* Let the upper half know where to find the callbacks information */
	.callbacks_info = &callbacks_info,

	/* Let the upper half know where to find the tables for the interrupt
	 * routine. This information is needed when a new slotlist is attached
	 * to the upper half driver.
	 */
	.isr_actiontable0 = isr_actiontable0,
	.isr_actiontable1 = isr_actiontable1,
	.isr_infotable0 = isr_infotable0,
	.isr_infotable1 = isr_infotable1,

	/* Attach the lower half operations to the upper half */
	.ops = &g_messBUS_ops,
};

/* This will be the expected period ticks for a ideal clock */
//XXXstatic uint16_t g_nom_period_ticks = 0;

/* This will be the sum of period ticks over n periods */
static uint32_t g_summarized_period_ticks = 0;

/* This will be be the current period ticks */
static uint16_t g_period_ticks = 0;

/* Deviation of the client relative to the master in client's ticks */
static int32_t g_dev_ticks = 0;

/* Real ticks for timing an action */
static int32_t g_real_ticks = 0;

/* Number of performed period scans during (re-)synchronization */
static uint16_t g_n_period_scans = 0;
#ifdef mB_SELF_SYNC
/* This is the last CCR2 value for comparison during period scan */
static uint16_t g_tim4_last_ccr1;
#endif
/* This flag indicates the end of a timeslice for channel 1 */
static uint8_t g_ch1_end_of_timeslice;


/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: baud_to_brr
 *
 * Description:
 *   This function converts a baudrate into a BRR register value. The way the
 *   conversion has to be done differs for the F4 and F7 chips. See their
 *   reference manuals for more information.
 *
 * Assumptions:
 *   - Note that the F7 chips have to be clocked by 216MHz SYSCLK, which has
 *     to be configured in board.h respectively.
 *
 *   - This implementation requires that oversampling by 8 is configured.
 *     Oversampling by 16 is not supported, because messBUS' relatively high
 *     baudrates can be represented way more accurately when using over-
 *     sampling by 8 in the very most cases.
 *
 ****************************************************************************/
#if defined(CONFIG_ARCH_CHIP_STM32F7)
static uint16_t up_baud_to_brr (uint32_t baud)
{
	/* As we oversample by 8, the equation is:
	 *
	 *   baud      = 2 * fCK / usartdiv8
	 *   usartdiv8 = 2 * fCK / baud
	 *
	 * fCK is the 216 Mhz SYSCLK, which allows very high baudrates. Make
	 * sure SYSCLK is selected as clock source in board.h.
	 *
	 * As we oversample by 8, magical bit shifting operation is required for
	 * bits 0 to 3 in order to obtain brr register value from usartdiv8.
	 *
	 * Baud/2 is added to fCK to support rounding.
	 */
	uint16_t usartdiv8 = (uint16_t) (((STM32_SYSCLK_FREQUENCY << 1) + (baud >> 1)) / baud);
	uint16_t brr = ((usartdiv8 & 0xfff0) | ((usartdiv8 & 0x000f) >> 1));

	return brr;
}
#elif defined(CONFIG_STM32_STM32F4XXX)
static uint16_t up_baud_to_brr (uint32_t baud)
{
	/* This second implementation is for U[S]ARTs that support fractional
	 * dividers, such as the F4's USARTS.
	 *
	 * Configure the USART Baud Rate.  The baud rate for the receiver and
	 * transmitter (Rx and Tx) are both set to the same value as programmed
	 * in the Mantissa and Fraction values of USARTDIV. In case of
	 * oversampling by 8 the equations are:
	 *
	 *   baud     = fCK / (8 * usartdiv)
	 *   usartdiv = fCK / (8 * baud)
	 *
	 * Where fCK is the input clock to the peripheral (PCLK1 for USART2, 3, 4, 5
	 * or PCLK2 for USART1 and USART6)
	 *
	 * First calculate (NOTE: all stand baud values are even so dividing by two
	 * does not lose precision):
	 *
	 *   usartdiv32 = 32 * usartdiv = fCK / (baud/2)
	 */
	uint16_t usartdiv32 = (uint16_t) (mB_CH1_PCLKx_FREQUENCY / (baud >> 1));

	/* The mantissa part is then */
	uint16_t mantissa = (usartdiv32 >> 4);

	/* The fractional remainder (with rounding) */
	uint16_t fraction = ((usartdiv32 - (mantissa << 4) + 1) >> 1);

	/* Place both values in the brr which will be returned */
	uint16_t brr = (mantissa << USART_BRR_MANT_SHIFT);
	brr |= (fraction << USART_BRR_FRAC_SHIFT);

	return brr;
}
#endif

/****************************************************************************
 * Name: up_convert_slotlist
 *
 * Description:
 *   This function converts a slotlist into both a action- and infotable,
 *   which the interrupt handler will use to perform the desired slots.
 *
 * Input arguments:
 *   slotlist - A reference to the slotlist
 *   actiontable - A reference to a preallocated actiontable
 *   infotable - A reference to a preallocated infotable
 *
 ****************************************************************************/

static void up_convert_slotlist(void *slotlist, void *actiontable, void *infotable)
{
	uint8_t remaining_slots = ((ch1_slotlist_s *) slotlist)->n_slots;
	struct slot_s *slot_ptr = ((ch1_slotlist_s *) slotlist)->slot;
	struct up_isr_action_s *action_ptr = actiontable;
	struct up_isr_info_s *info_ptr = infotable;
	uint32_t nom_ticks;

	while (remaining_slots > 0)
	{
		if (slot_ptr->read_write == MESSBUS_TX)
		{
			/* Tx operation has three actions:
			 *
			 * - CH1_TX_DE (Data Enable)
			 * - CH1_TX_START
			 * - CH1_TX_END
			 *
			 * So we create three timed actions and a single info structure for
			 * this slot. We start with the first action:
			 */
			action_ptr->op_mode = mB_CH1_TX_DE;

			nom_ticks = ((slot_ptr->t_start + mB_CH1_TIM_PRESYNC_US) * mB_CH1_TIM_XMIT_NOM_TIMESLICE_TICKS) / mB_TIMESLICE_US;
			action_ptr->nom_ticks = (uint16_t) nom_ticks;
			action_ptr++;

			/* Schedule second action mB_TX_OFFSET_US after first action. */
			action_ptr->op_mode = mB_CH1_TX_START;
			nom_ticks += (mB_TX_OFFSET_US * mB_CH1_TIM_XMIT_NOM_TIMESLICE_TICKS)/ mB_TIMESLICE_US;
			action_ptr->nom_ticks = (uint16_t) nom_ticks;
			action_ptr++;

			/* Third action: */
			action_ptr->op_mode = mB_CH1_TX_END;
			nom_ticks = ((slot_ptr->t_end + mB_CH1_TIM_PRESYNC_US) * mB_CH1_TIM_XMIT_NOM_TIMESLICE_TICKS) / mB_TIMESLICE_US;
			action_ptr->nom_ticks = (uint16_t) nom_ticks;
			action_ptr++;
		}
		else
		{
			/* Rx operation has two actions:
			 *
			 * - CH1_RX_START
			 * - CH1_RX_END
			 *
			 * So we create two timed actions and a single info structure for
			 * this slot. We start with the first action:
			 */

			/*
			 * messBUS node need to begin offset us before rx slot with its preparation
			 * to ensure that it is ready to read data an slot begin
			 */
			//XXXuint16_t t_start_corr = slot_ptr->t_start + mB_RX_OFFSET_US;
			action_ptr->op_mode = mB_CH1_RX_START;
			if ((slot_ptr->t_start + mB_RX_OFFSET_US) < (mB_CH1_TIM_SYNC_UPDATE_OFFSET_US) ){
				// allow RX to start immediately after checking sync pulse, no exact timing available
				nom_ticks = 0;
			} else {
				nom_ticks = ((slot_ptr->t_start + mB_RX_OFFSET_US + mB_CH1_TIM_PRESYNC_US) * mB_CH1_TIM_XMIT_NOM_TIMESLICE_TICKS) / mB_TIMESLICE_US;

			}
			action_ptr->nom_ticks = (uint16_t) nom_ticks;
			action_ptr++;

			/* Second action: */
			action_ptr->op_mode = mB_CH1_RX_END;
			nom_ticks = ((slot_ptr->t_end + mB_CH1_TIM_PRESYNC_US) * mB_CH1_TIM_XMIT_NOM_TIMESLICE_TICKS) / mB_TIMESLICE_US;
			action_ptr->nom_ticks = (uint16_t) nom_ticks;
			action_ptr++;
		}

		/* Finally the info for this slot, which is the same regardless of
		 * TX or RX operation:
		 */
		info_ptr->brr = up_baud_to_brr(slot_ptr->baud);
		info_ptr->buffer = slot_ptr->buffer;
		info_ptr->buf_length_ptr = &slot_ptr->buf_length;
		info_ptr->buf_length = slot_ptr->buf_length;

		/* This slot is done now. */
		remaining_slots--;

		/* If there are still remaining slots increment the pointers */
		if (remaining_slots > 0)
		{
			info_ptr++;
			slot_ptr++;
		}
	}

#if CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
	/* Insert action to trigger the wake up callback */
	if(ch1_wake_up_callback_info.configured)
	{
		action_ptr->op_mode = mB_CH1_WAKE_UP;
		nom_ticks = ((ch1_wake_up_callback_info.t_wake_up  + mB_CH1_TIM_PRESYNC_US) * mB_CH1_TIM_XMIT_NOM_TIMESLICE_TICKS) / mB_TIMESLICE_US;
		action_ptr->nom_ticks = (uint16_t) nom_ticks;
		action_ptr++;
	}

#endif //CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS

	/* Insert last action here with max ticks in order to make
	 * sure that this action will not be triggered by Capture/Compare inter-
	 * rupt before update event.
	 */
	action_ptr->op_mode = mB_CH1_DONE;
	action_ptr->nom_ticks = 0xFFFF;
}

/****************************************************************************
 * Name: pendsv_handler
 *
 * Description:
 *   This is the handler for the PendSV interrupt.
 *
 ****************************************************************************/

static void pendsv_handler(void)
{
#if mB_DEBUG_VISUALIZE_INTERRUPTS
	/* Set Debug-GPIO */
	setbit32(GPIO_DEBUG_BSRR, GPIO_DEBUG_ISR_ON);
#endif
#if mB_DEBUG_VISUALIZE_INTERRUPTS
	/* Reset Debug-GPIO */
	setbit32(GPIO_DEBUG_BSRR, GPIO_DEBUG_ISR_OFF);
#endif
#if mB_DEBUG_VISUALIZE_INTERRUPTS
	/* Set Debug-GPIO */
	setbit32(GPIO_DEBUG_BSRR, GPIO_DEBUG_ISR_ON);
#endif
#if mB_DEBUG_VISUALIZE_INTERRUPTS
	/* Reset Debug-GPIO */
	setbit32(GPIO_DEBUG_BSRR, GPIO_DEBUG_ISR_OFF);
#endif

	void (*callback_function)(void) = 0;
	if(ch1_single_callback_info.pendsv_pending)
	{
		callback_function = ch1_single_callback_info.callback_function;
		(*callback_function)();
		ch1_single_callback_info.pendsv_pending = 0;
	}
	if(ch1_wake_up_callback_info.pendsv_pending)
	{
		callback_function = ch1_wake_up_callback_info.callback_function;
		(*callback_function)();
		ch1_wake_up_callback_info.pendsv_pending = 0;
	}

#if mB_DEBUG_VISUALIZE_INTERRUPTS
	/* Set Debug-GPIO */
	setbit32(GPIO_DEBUG_BSRR, GPIO_DEBUG_ISR_ON);
#endif
#if mB_DEBUG_VISUALIZE_INTERRUPTS
	/* Reset Debug-GPIO */
	setbit32(GPIO_DEBUG_BSRR, GPIO_DEBUG_ISR_OFF);
#endif
#if mB_DEBUG_VISUALIZE_INTERRUPTS
	/* Set Debug-GPIO */
	setbit32(GPIO_DEBUG_BSRR, GPIO_DEBUG_ISR_ON);
#endif
#if mB_DEBUG_VISUALIZE_INTERRUPTS
	/* Reset Debug-GPIO */
	setbit32(GPIO_DEBUG_BSRR, GPIO_DEBUG_ISR_OFF);
#endif
}

/****************************************************************************
 * Name: mB_ch1_tim_handler
 *
 * Description:
 *   This is the handler for the high priority TIM interrupt.
 *
 ****************************************************************************/

static void mB_ch1_tim_handler(void)
{
#if mB_DEBUG_VISUALIZE_INTERRUPTS
	/* Set Debug-GPIO */
	setbit32(GPIO_DEBUG_BSRR, GPIO_DEBUG_ISR_ON);
#endif
	static int isr_mode_cnt=0;
	static uint8_t isr_mode=0;
	static bool mB_transmission_enabled = true;
	static int32_t deviation_ppm=0;
	uint16_t mB_ch1_tim_sr;


	/* State machine */

	switch (isr_mode)
	{

	/*
	 * Actively search for SYNC pulse with high resolution
	 * triggered by CCR3
	 */
	case 0:
	  {
#ifdef mB_DEBUG_LED
   switch_LED_red_on();
#endif
		/* read status flags */
		mB_ch1_tim_sr = getreg16(STM32_mB_CH1_TIM_SR);
		/* clear status flags */
		putreg16(~(GTIM_SR_CC1IF | GTIM_SR_CC2IF | GTIM_SR_CC3IF | GTIM_SR_UIF | GTIM_SR_TIF), STM32_mB_CH1_TIM_SR);
		/*force CH4 (TRGO) low */
		modifybit16(STM32_mB_CH1_TIM_CCMR2,GTIM_CCMR2_OC4M_MASK,GTIM_CCMR_MODE_OCREFLO << GTIM_CCMR2_OC4M_SHIFT);

#if defined(CONFIG_STM32_STM32F4XXX)
		/*workaround for reset-mode without combined trigger mode*/
		if(getreg16(STM32_mB_CH1_TIM_CNT) < g_mB_ch1_tim_ccr3){
			putreg16(GTIM_EGR_CC1G, STM32_mB_CH1_TIM_EGR);
			return;
		}
		if (mB_ch1_tim_sr & GTIM_SR_CC2IF){
#endif
		if (mB_ch1_tim_sr & GTIM_SR_CC1IF){
			/*check rising edge*/
			uint16_t mB_ch1_tim_ccr1 = getreg16(STM32_mB_CH1_TIM_CCR1);
			if ((mB_ch1_tim_ccr1 > mB_CH1_TIM_SYNC_TICKS_MIN) && (mB_ch1_tim_ccr1 < mB_CH1_TIM_SYNC_TICKS_MAX)){
				if(isr_mode_cnt < STM32_MB_ISR_MODE_SWITCH_CNT){
				   isr_mode_cnt++;
	//   switch_LED_green_on();
				} else {
					/*TODO: switch to passive mode via ioctl?*/
					/*prepare passive mode*/
					isr_mode_cnt=0;
					isr_mode = 1;

				#ifdef CONFIG_ARCH_CHIP_STM32F7
					modifybit32(STM32_mB_CH1_TIM_SMCR, GTIM_SMCR_SMS | GTIM_SMCR_SMS_MASK | GTIM_SMCR_TS_MASK, GTIM_SMCR_TRIGGER | GTIM_SMCR_ITR1);						// Trigger mode on ITR1
				#else

					modifybit16(STM32_mB_CH1_TIM_CR1, 0, GTIM_CR1_OPM);	// Set one pulse mode
					modifybit16(STM32_mB_CH1_TIM_SMCR, GTIM_SMCR_SMS_MASK | GTIM_SMCR_TS_MASK, GTIM_SMCR_TRIGGER | GTIM_SMCR_ITR1);										// Trigger mode on ITR1
				#endif
					/* Set new ARR value to trigger next interrupt 7us after SYNC */
					//putreg16(mB_CH1_TIM_SYNC_CHECK_OFFSET_TICKS, STM32_mB_CH1_TIM_ARR);

					/* Configure GPIO Alternate function -> USART RX */
					modifybit32(STM32_mB_CH1_RX_GPIO_AFRx,mB_CH1_RX_AFRx_MASK, mB_CH1_RX_AFRx_USART);
					/* clear Update interrupt flag*/
					putreg16(~GTIM_SR_UIF,STM32_mB_CH1_TIM_SR);
					//modifybit32(STM32_mB_CH1_TIM_DIER, GTIM_DIER_CC3IE, GTIM_DIER_UIE); //debug
					setbit32(STM32_mB_CH1_TIM_DIER, GTIM_DIER_UIE);

//setbit32(STM32_mB_CH1_TIM_DIER,GTIM_DIER_TIE); //XXX debug
#ifdef mB_DEBUG_LED
switch_LED_red_off();
#endif
					/* check that the counter was not reset since this ISR was triggered*/
					if(getreg16(STM32_mB_CH1_TIM_CNT) >= (mB_CH1_TIM_SYNC_CHECK_OFFSET_TICKS)){
						/* set CH4 (TRGO) active on match */
						modifybit16(STM32_mB_CH1_TIM_CCMR2,GTIM_CCMR2_OC4M_MASK,GTIM_CCMR_MODE_CHACT << GTIM_CCMR2_OC4M_SHIFT);
					} else {
						/* avoid wrong CCR3 IRQ:*/
						/* disable counter immediately */
						clearbit16(STM32_mB_CH1_TIM_CR1, GTIM_CR1_CEN);
						/* generate update event to trigger update interrupt*/
						putreg16(GTIM_EGR_UG,STM32_mB_CH1_TIM_EGR);
					}

					break;

				}

				/* set CH4 (TRGO) active on match */
				modifybit16(STM32_mB_CH1_TIM_CCMR2,GTIM_CCMR2_OC4M_MASK,GTIM_CCMR_MODE_CHACT << GTIM_CCMR2_OC4M_SHIFT);
			}
		}
#if defined(CONFIG_STM32_STM32F4XXX)
		}
#endif
	  }
#ifdef mB_DEBUG_LED
	  switch_LED_red_off();
#endif
	  break;
#if 0
//old case 0
		/* Get the CCR1 register value */
		g_mB_ch1_tim_ccr1 = getreg16(STM32_mB_CH1_TIM_CCR1);

		/* Acknowledge the update event interrupt.*/
		putreg16(~GTIM_SR_UIF, STM32_mB_CH1_TIM_SR);

		/* Did a SYNC pulse occur (pulse > 4us && < 6us)?
		 * Then we try to catch SYNC without trigger next time, but by timing.
		 */
		if (g_mB_ch1_tim_ccr1 > mB_CH1_TIM_SYNC_TICKS_MIN && g_mB_ch1_tim_ccr1 < mB_CH1_TIM_SYNC_TICKS_MAX)
		{
			/* Activate the trigger for timer 4 */
			clearbit16(STM32_mB_CH1_TIM_CR2, GTIM_CR2_MMS_MASK);
			setbit16(STM32_mB_CH1_TIM_CR2, GTIM_CR2_MMS_UPDATE);

			/* Do we still have to sync with the master? */
			if (g_n_period_scans < (CONFIG_N_PERIOD_SCANS + 1))
			{
				/* Disable reset of timer 3 when trigger occurs */
				clearbit16(STM32_mB_CH1_TIM_SMCR, GTIM_SMCR_SMS_MASK);

				/* Set new ARR value to trigger next interrupt 15us after SYNC */
				putreg16(mB_CH1_TIM_PERIOD_SYNC4, STM32_mB_CH1_TIM_ARR);

				/* Next case: Action 8 */
				g_op_mode = 8;
			}
			else
			{
				/* Reset synchronization variables */
				g_n_period_scans = 0;
#ifdef mB_SELF_SYNC
				g_tim4_last_ccr1 = 0;
#endif
				/* Calculate average period ticks */
				g_period_ticks = (uint16_t) (g_summarized_period_ticks / CONFIG_N_PERIOD_SCANS);
				//g_messBUSpriv.period_ticks = g_period_ticks;

				/* Calculate the deviation of measured period ticks
				 * from the expected value for an ideal clock.
				 */
				g_dev_ticks = (int32_t) (g_period_ticks - mB_CH1_TIM_XMIT_NOM_TIMESLICE_TICKS);

				/* Calculate ticks until update event triggering action 2 will happen */
				int32_t nom_ticks = (mB_CH1_TIM_PERIOD * (int32_t) mB_CH1_TIM_XMIT_NOM_TIMESLICE_TICKS) / mB_TIMESLICE_US;
				g_real_ticks = nom_ticks + ((g_dev_ticks * nom_ticks) / ((int32_t) mB_CH1_TIM_XMIT_NOM_TIMESLICE_TICKS));
				g_ue_ticks = (uint16_t) (g_real_ticks - 1);

				/* Disable reset when trigger occurs */
				clearbit16(STM32_mB_CH1_TIM_SMCR, GTIM_SMCR_SMS_MASK);

				/* Disable TRC for IC2 and enable TI2 instead */
				clearbit16(STM32_mB_CH1_TIM_CCMR1, GTIM_CCMR1_CC2S_MASK);								// Reset Ch2 to output first (this does not affect pin config, so everything is fine)
				setbit16(STM32_mB_CH1_TIM_CCMR1, (GTIM_CCMR_CCS_CCIN1 << GTIM_CCMR1_CC2S_SHIFT));
				setbit16(STM32_mB_CH1_TIM_CCER, GTIM_CCER_CC2E);										// Enable Ch2 capture

				/* Set new prescaler to cover at least 10ms with 16bit.
				 * This setting is not activated immediately, but after the next update event.
				 */
				putreg16(mB_CH1_TIM_XMIT_PSC, STM32_mB_CH1_TIM_PSC);

				/* Set new ARR value to trigger next interrupt 15us after SYNC */
				putreg16(mB_CH1_TIM_PERIOD_SYNC4, STM32_mB_CH1_TIM_ARR);

				/* Next case: Compute corrected ARR value to reset 2 us before SYNC. */
				g_op_mode = 1;
			}

		}

		break;
#endif
		/* idle period
		 * Update Interrupt with disabled timer
		 *
		 * operation is safe and tested when trigger occurs at running isr
		 */
	case 1:
	{
		/* read status flags */
		mB_ch1_tim_sr = getreg16(STM32_mB_CH1_TIM_SR);



		if(!(mB_ch1_tim_sr & GTIM_SR_UIF)){
			/* somthing went wrong*/

			/*clear all pending status flags*/
			putreg16(~(mB_ch1_tim_sr), STM32_mB_CH1_TIM_SR);
#ifdef mB_DEBUG_LED
switch_LED_red_on();
switch_LED_green_on();
switch_LED_green_off();
switch_LED_green_on();
switch_LED_green_off();
switch_LED_red_off();
#endif
			/* wait for next update interrupt */
			break;
		}



#ifdef mB_DEBUG_LED
switch_LED_red_on();
#endif
		/*force CH4 (TRGO) low */
		modifybit16(STM32_mB_CH1_TIM_CCMR2,GTIM_CCMR2_OC4M_MASK,GTIM_CCMR_MODE_OCREFLO << GTIM_CCMR2_OC4M_SHIFT);


		/* use Timer input for RX*/
		modifybit32(STM32_mB_CH1_RX_GPIO_AFRx,mB_CH1_RX_AFRx_MASK,mB_CH1_RX_AFRx_TIM);
		/* disable one pulse mode */
		/*not atomic, can accidently disable timer after trigger*/
		 //clearbit16(STM32_mB_CH1_TIM_CR1, GTIM_CR1_OPM);

		/* Set new ARR value to trigger next interrupt 14us after SYNC */
		putreg16(mB_CH1_TIM_SYNC_UPDATE_TICKS, STM32_mB_CH1_TIM_ARR);

		/* Set CCR3 IRQ to check sync pulse */
		putreg16(mB_CH1_TIM_PRESYNC_TICKS+mB_CH1_TIM_SYNC_CHECK_OFFSET_TICKS, STM32_mB_CH1_TIM_CCR3);

		/* Set new prescaler to cover at least 10ms with 16bit.
		* This setting is not activated immediately, but after the next update event.
		*/
		putreg16(mB_CH1_TIM_XMIT_PSC, STM32_mB_CH1_TIM_PSC);

		/* clear status flags for clean capture of CC1 and CC2 */
		putreg16(~(GTIM_SR_CC1OF | GTIM_SR_CC2OF  | GTIM_SR_CC1IF | GTIM_SR_CC2IF | GTIM_SR_CC3IF | GTIM_SR_UIF ), STM32_mB_CH1_TIM_SR);


		if(mB_transmission_enabled){

			/* Are there new tables available from higher logic? If yes switch tables
			 * and reset the flag.
			 */
			if(g_messBUSpriv.new_table_avail)
			{
				g_messBUSpriv.new_table_avail = 0;
				if (g_messBUSpriv.active_table)
				{
					g_messBUSpriv.active_table = 0;
				}
				else
					g_messBUSpriv.active_table = 1;
			}

			/* Choose tables for next timeslice according to updated g_isr_active_tables flag */
			if (g_messBUSpriv.active_table)
			{
				isr_actiontable_ptr = isr_actiontable1;
				isr_infotable_ptr = isr_infotable1;
			}
			else
			{
				isr_actiontable_ptr = isr_actiontable0;
				isr_infotable_ptr = isr_infotable0;
			}

			/* Indicate the end of a timeslice */
			g_ch1_end_of_timeslice = 1;
		}

		isr_mode = 2;
#ifdef mB_DEBUG_LED
		switch_LED_red_off();
#endif
	}
		break;





		/*
		 * CCR3 @ 7us
		 * Passive search for SYNC pulse with high resolution
		 */
		case 2:
#ifdef mB_DEBUG_LED
switch_LED_green_on();
switch_LED_red_on();
#endif
		  {

			/* read status flags */
			mB_ch1_tim_sr = getreg16(STM32_mB_CH1_TIM_SR);

			/* disable one pulse mode at running timer*/
			clearbit16(STM32_mB_CH1_TIM_CR1, GTIM_CR1_OPM);

			/* check IRQ flag */
			if(!(mB_ch1_tim_sr & GTIM_SR_CC3IF)){
				/*something went wrong leave interrupt pending; case 3 should handle it*/


#ifdef mB_DEBUG_LED
				switch_LED_red_off();
				switch_LED_red_on();
				switch_LED_red_off();
				switch_LED_red_on();
				switch_LED_red_off();
				switch_LED_red_on();
				switch_LED_red_off();
				//for(;;);
				switch_LED_green_off();
#endif

				isr_mode = 3;

				break;

			}

			/* clear status flags */
			putreg16(~(GTIM_SR_CC3IF ), STM32_mB_CH1_TIM_SR);
            //TODO: handle overcapture ? or just leave it, because we check the pulse width

				/* get falling edge */
				uint16_t mB_ch1_tim_ccr2 = getreg16(STM32_mB_CH1_TIM_CCR2);

				/* get rising edge */
				uint16_t mB_ch1_tim_ccr1 = getreg16(STM32_mB_CH1_TIM_CCR1);

				/* check sync pulse */
				//TODO: check that (mB_ch1_tim_ccr1 < ccr3)
				if ((mB_ch1_tim_sr & GTIM_SR_CC2IF)
						&& (mB_ch1_tim_sr & GTIM_SR_CC1IF)
						&& ((mB_ch1_tim_ccr1 - mB_ch1_tim_ccr2)  > mB_CH1_TIM_SYNC_TICKS_MIN)
						&& ((mB_ch1_tim_ccr1-mB_ch1_tim_ccr2) < mB_CH1_TIM_SYNC_TICKS_MAX))
				{
					/* valid sync pulse captured */
					isr_mode_cnt=0;


					/* set CH4 (TRGO) active on match */
					uint16_t mB_ch1_tim_ccr4= mB_ch1_tim_ccr2+mB_CH1_TIM_SYNC_TRGO_OFFSET_TICKS;
					if(mB_ch1_tim_ccr4 < (mB_CH1_TIM_SYNC_UPDATE_TICKS)){ //check for smaller value than ARR
						putreg16(mB_ch1_tim_ccr4,STM32_mB_CH1_TIM_CCR4 );
						modifybit16(STM32_mB_CH1_TIM_CCMR2,GTIM_CCMR2_OC4M_MASK,GTIM_CCMR_MODE_CHACT << GTIM_CCMR2_OC4M_SHIFT);
						/*if we are too late, pin will be already forced low on next update*/
					}

					/* Signal that there was another successful SYNC */
					g_messBUSpriv.sync_count++;//TODO: from peer; required?

				} else {
					/* no valid pulse captured */

					if(isr_mode_cnt < STM32_MB_ACTIVE_ISR_MODE_SWITCH_CNT){
					   isr_mode_cnt++;
					} else {
						/*TODO: switch to passive mode via ioctl?*/
						/*prepare active mode*/

						isr_mode_cnt=0;
						isr_mode = 0;

						/* disable timer */
						clearbit16(STM32_mB_CH1_TIM_CR1, GTIM_CR1_CEN);
						clearbit32(STM32_mB_CH1_TIM_DIER, GTIM_DIER_UIE);

						/* clear status flags */
						putreg16(~(GTIM_SR_CC1OF | GTIM_SR_CC2OF |GTIM_SR_CC1IF | GTIM_SR_CC2IF | GTIM_SR_CC3IF ), STM32_mB_CH1_TIM_SR);


						/* Configure Timer as active SYNC catcher */
						putreg16(mB_CH1_TIM_SYNC_PSC, STM32_mB_CH1_TIM_PSC);										// Configure prescaler											// Configure 7us period
						putreg16(0xFFFF, STM32_mB_CH1_TIM_ARR);														//max timeout

						/*TIM CH3: interrupt @ 7us*/
						putreg16(mB_CH1_TIM_SYNC_CHECK_OFFSET_TICKS, STM32_mB_CH1_TIM_CCR3);										// set interrupt to 7us

						/*TIM CH4: TRGO @ 10us after valid sync pulse*/
						putreg16(mB_CH1_TIM_SYNC_TRGO_OFFSET_TICKS, STM32_mB_CH1_TIM_CCR4);										// set TRGO trigger to 10us

					#ifdef CONFIG_ARCH_CHIP_STM32F7
						modifybit16(STM32_mB_CH1_TIM_CR1, 0, GTIM_CR1_OPM);											// Set one pulse mode
						modifybit32(STM32_mB_CH1_TIM_SMCR, GTIM_SMCR_SMS_MASK | GTIM_SMCR_TS_MASK, GTIM_SMCR_SMS | GTIM_SMCR_TI2FP2);			// Combined Reset and Trigger mode
					#else
						modifybit16(STM32_mB_CH1_TIM_SMCR, GTIM_SMCR_SMS_MASK | GTIM_SMCR_TS_MASK , GTIM_SMCR_RESET | GTIM_SMCR_TI2FP2);		// Reset mode
						modifybit16(STM32_mB_CH1_TIM_CR1, 0, GTIM_CR1_CEN);											// Enable the counter
					#endif
#ifdef mB_DEBUG_LED
  switch_LED_red_off();
  switch_LED_green_off();
#endif
						break;


					}

				}

					if(mB_transmission_enabled && (isr_actiontable_ptr->nom_ticks == 0)){
#ifdef mB_DEBUG_LED
  switch_LED_green_off();
#endif
			        /* prepare to receive master message*/
//TODO: verify configuration of first slot
					/* use Uart input for RX*/
					modifybit32(STM32_mB_CH1_RX_GPIO_AFRx,mB_CH1_RX_AFRx_MASK,mB_CH1_RX_AFRx_USART);


					/* Set baudrate */
					putreg32((uint32_t) isr_infotable_ptr->brr, STM32_mB_CH1_USART_BRR);

					/* Prepare and enable the DMA stream */
					putreg32((uint32_t) isr_infotable_ptr->buffer, mB_CH1_RX_DMA_SxMOAR);		// Set the memory address
					putreg32((uint32_t) 0xffff, mB_CH1_RX_DMA_SxNDTR);						// Set the NDTR to max 0xffff
					putreg32(mB_CH1_RX_DMA_INT_SxMASK,mB_CH1_RX_DMA_xIFCR);				// Clear pending flags for safety reasons
					setbit32(mB_CH1_RX_DMA_SxCR, DMA_SCR_EN);									// Enable the stream

					/* Enable USART RE again */
					setbit32(STM32_mB_CH1_USART_CR1, USART_CR1_RE);

		#if CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
					/* Check for pending trigger of callbacks. */
					if (ch1_single_callback_info.trigger_pending == 1 )
					{
						/* Is there a new sync related callback request available? */
						if (ch1_single_callback_info.request == MESSBUS_SYNC_CALLBACK_REQUEST)
						{
							/* The callback shall take place as soon as possible. Reset hiprio pending flag. */
							ch1_single_callback_info.trigger_pending = 0;

							/* Trigger the PendSV interrupt */
							putreg32( NVIC_INTCTRL_PENDSVSET, NVIC_INTCTRL);
						}
					}
		#endif //CONFIG_MESSBUS_USE_CALLBACKS

					/* Finally increment relevant table pointers */
					isr_actiontable_ptr++;
#ifdef mB_DEBUG_LED
  switch_LED_green_on();
#endif
					}



		  }
#ifdef mB_DEBUG_LED
  switch_LED_red_off();
  switch_LED_green_off();
#endif
			isr_mode = 3;
		  break;



	case 3: /*update at 14 us ?! */
	{
		/* read status flags */
		mB_ch1_tim_sr = getreg16(STM32_mB_CH1_TIM_SR);

		/*clear all pending status flags*/
		putreg16(~(mB_ch1_tim_sr), STM32_mB_CH1_TIM_SR);

		if(!(mB_ch1_tim_sr & GTIM_SR_UIF)){
			/* somthing went wrong, abort*/

#ifdef mB_DEBUG_LED
switch_LED_red_on();
switch_LED_green_on();
switch_LED_green_off();
switch_LED_green_on();
switch_LED_green_off();
switch_LED_red_off();
#endif
			break;
		}


	}

#ifdef mB_DEBUG_LED
  switch_LED_red_on();
#endif
		/*update at 14 us*/


/*force CH4 (TRGO) low */
modifybit16(STM32_mB_CH1_TIM_CCMR2,GTIM_CCMR2_OC4M_MASK,GTIM_CCMR_MODE_OCREFLO << GTIM_CCMR2_OC4M_SHIFT);



		/* enable one pulse mode */
		setbit16(STM32_mB_CH1_TIM_CR1, GTIM_CR1_OPM);


		/*get clock deviation*/
		deviation_ppm=stm32_clock_get_deviation()/1000; /* deviation in ppm instead of ppb */


		/* Set new ARR value to trigger next interrupt 5us before SYNC */
		putreg16(mB_CH1_TIM_XMIT_NOM_TICKS + (deviation_ppm * mB_CH1_TIM_XMIT_NOM_TICKS)/US_PER_SEC - mB_CH1_TIM_XMIT_OFFSET_TICKS, STM32_mB_CH1_TIM_ARR);

		//putreg16(((mB_TIMESLICE_US-50) * (int32_t) mB_CH1_TIM_XMIT_NOM_TIMESLICE_TICKS) / mB_TIMESLICE_US, STM32_mB_CH1_TIM_ARR);
		//putreg16(mB_CH1_TIM_PERIOD /2, STM32_mB_CH1_TIM_ARR); //debug only


		/* Reset prescaler for highest resolution.
		 * This setting is not activated immediately, but after the next update event.
		 */
		putreg16(mB_CH1_TIM_SYNC_PSC, STM32_mB_CH1_TIM_PSC);



		if(mB_transmission_enabled){

			/*setup first (second?!) action*/

			/* Calculate and set new CCR3 value for next capture interrupt. */
			g_real_ticks = isr_actiontable_ptr->nom_ticks + ((deviation_ppm * isr_actiontable_ptr->nom_ticks) / US_PER_SEC) - mB_CH1_TIM_XMIT_OFFSET_TICKS;
			if (g_real_ticks<0) g_real_ticks=0; //FIXME: workaround for stability, should not happen
			putreg16((uint16_t) g_real_ticks, STM32_mB_CH1_TIM_CCR3);

			/* Clear the compare flag for safety reasons. */
			putreg16(~GTIM_SR_CC3IF, STM32_mB_CH1_TIM_SR);


			/* Enable Compare interrupts */
//			setbit16(STM32_mB_CH1_TIM_DIER, GTIM_DIER_CC3IE);

			/* Next case: First action */
			g_op_mode = isr_actiontable_ptr->op_mode;

			/* Finally increment relevant table pointers */
			isr_actiontable_ptr++;

			isr_mode = 4;


			if (g_real_ticks <= getreg16(STM32_mB_CH1_TIM_CNT))
			{
#ifdef mB_DEBUG_LED
				switch_LED_green_on();
				switch_LED_red_off();
				switch_LED_green_off();

				switch_LED_green_on();
				switch_LED_red_on();
				switch_LED_red_off();
#endif

				goto NEXT_ACTION;
			}


		} else {
			/* Set CCR3 IRQ to max to avoid other ccr3 irqs */
			putreg16(0xFFFF, STM32_mB_CH1_TIM_CCR3);
			isr_mode = 1;
		}
#ifdef mB_DEBUG_LED
  switch_LED_red_off();
#endif
		break;



#if 0 //XXXold
	/* Compute corrected ARR value to reset 2 us before SYNC.
	 * Prepare high resolution and input capture settings for passive SYNC search.
	 */
	case 1:


		/* Acknowledge the update event interrupt.*/
		putreg16(~GTIM_SR_UIF, STM32_mB_CH1_TIM_SR);

		/* Deactivate the trigger for timer 4, by setting OC1REF als trigger source */
		clearbit16(STM32_mB_CH1_TIM_CR2, GTIM_CR2_MMS_MASK);
		setbit16(STM32_mB_CH1_TIM_CR2, GTIM_CR2_MMS_OC1REF);

		/* Set new ARR value to reset approx. 3us before SYNC */
		putreg16(g_ue_ticks, STM32_mB_CH1_TIM_ARR);

		/* Reset prescaler for highest resolution.
		 * This setting is not activated immediately, but after the next update event.
		 */
		putreg16(mB_CH1_TIM_SYNC_PSC, STM32_mB_CH1_TIM_PSC);

		/* Next case: Set new ARR and switch to case 3 */
		g_op_mode = 2;

		break;
#endif
#if 0
	/* Set new ARR, configure GPIO to TIM CH2IN and switch to case 3 */
	case 2:








		/* Acknowledge the update event interrupt.*/
		putreg16(~GTIM_SR_UIF, STM32_mB_CH1_TIM_SR);

		/* Disable USART RE.
		 * Otherwise switching the AF may be interpreted as incoming byte. */
		clearbit32(STM32_mB_CH1_USART_CR1, USART_CR1_RE);

		/* Disable DMA2 Stream 1 */
		clearbit32(mB_CH1_RX_DMA_SxCR, DMA_SCR_EN);

		/* Configure GPIO to Alternate function TIM CH2IN */
		//clearbit32(STM32_mB_CH1_RX_GPIO_AFRx, mB_CH1_RX_AFRx_MASK);
		//setbit32(STM32_mB_CH1_RX_GPIO_AFRx, mB_CH1_RX_AFRx_TIM);
		modifybit32(STM32_mB_CH1_RX_GPIO_AFRx,mB_CH1_RX_AFRx_MASK,mB_CH1_RX_AFRx_TIM);

		/* Clear capture flags*/
		putreg16(~(GTIM_SR_CC1OF | GTIM_SR_CC2OF | GTIM_SR_CC2IF | GTIM_SR_CC1IF), STM32_mB_CH1_TIM_SR);

		/* Disable Compare interrupts for safety reasons */
		clearbit16(STM32_mB_CH1_TIM_DIER, GTIM_DIER_CC3IE);

		/* Mark end of operations that must happen before falling edge
		 * of incoming sync signal. From this, maximal tolerable
		 * CONFIG_MESSBUS_PHASE_TOLERANCE can be estimated (~750ns).
		 */
		#if mB_DEBUG_VISUALIZE_INTERRUPTS
			/* Reset Debug-GPIO */
			setbit32(GPIO_DEBUG_BSRR, GPIO_DEBUG_ISR_OFF);
		#endif
		#if mB_DEBUG_VISUALIZE_INTERRUPTS
			/* Set Debug-GPIO */
			setbit32(GPIO_DEBUG_BSRR, GPIO_DEBUG_ISR_ON);
		#endif

		/* Set new ARR for passive SYNC without clock correction */
		putreg16(mB_CH1_TIM_PERIOD_SYNC2, STM32_mB_CH1_TIM_ARR);

		/* Are there new tables available from higher logic? If yes switch tables
		 * and reset the flag.
		 */
		if(g_messBUSpriv.new_table_avail)
		{
			g_messBUSpriv.new_table_avail = 0;
			if (g_messBUSpriv.active_table)
			{
				g_messBUSpriv.active_table = 0;
			}
			else
				g_messBUSpriv.active_table = 1;
		}

		/* Choose tables for next timeslice according to updated g_isr_active_tables flag */
		if (g_messBUSpriv.active_table)
		{
			isr_actiontable_ptr = isr_actiontable1;
			isr_infotable_ptr = isr_infotable1;
		}
		else
		{
			isr_actiontable_ptr = isr_actiontable0;
			isr_infotable_ptr = isr_infotable0;
		}

		/* Indicate the end of a timeslice */
		g_ch1_end_of_timeslice = 1;

		/* Next case: Passively measure SYNC */
		g_op_mode = 3;

		break;
#endif

#if 0
	/* Passively measure SYNC (which normally should have occurred just before).
	 * Also prepare offset correcting update event, as CCR2 may not represent 2us exactly.
	 * Configure GPIO to USART RX if we are still synchronized.
	 */
	case 3:

		/* Acknowledge the update event interrupt.*/
		putreg16(~GTIM_SR_UIF, STM32_mB_CH1_TIM_SR);

		/* Check capture flags for safety reasons */
		uint16_t volatile mB_ch1_tim_sr = getreg16(STM32_mB_CH1_TIM_SR);
		uint8_t capture_err = 0;

		/* Check for overcapture */
		if((mB_ch1_tim_sr & GTIM_SR_CC1OF) && (mB_ch1_tim_sr & GTIM_SR_CC2OF))
		{
			capture_err = 1;
		}

		/* Make sure new captures occured */
		if(!(mB_ch1_tim_sr & GTIM_SR_CC1IF) && !(mB_ch1_tim_sr & GTIM_SR_CC2IF))
		{
			capture_err = 1;
		}

		/* Get the CCR1 and CCR2 register values */
		g_mB_ch1_tim_ccr1 = getreg16(STM32_mB_CH1_TIM_CCR1);
		g_mB_ch1_tim_ccr2 = getreg16(STM32_mB_CH1_TIM_CCR2);

		/* Calculate the ticks during SYNC */
		uint16_t sync_ticks = g_mB_ch1_tim_ccr1 - g_mB_ch1_tim_ccr2;

		/* Check if SYNC was in acceptable range and hence successful */
		if (sync_ticks >= mB_CH1_TIM_SYNC_TICKS_MIN && sync_ticks <= mB_CH1_TIM_SYNC_TICKS_MAX && capture_err == 0)
		{
			g_sync_err = 0;
		}
		else
		{
			g_sync_err++;
		}

		/* Check if phase of SYNC was in acceptable range and hence successful.
		 * Also set new ARR for passive SYNC with clock offset correction.
		 */
		if (g_mB_ch1_tim_ccr2 >= mB_LOWER_PHASE_THR && g_mB_ch1_tim_ccr2 <= mB_UPPER_PHASE_THR && capture_err == 0)
		{
			/* CCR2 register value is the phase */
			int16_t phase_err_ticks = ((int16_t) (mB_CH1_TIM_PRESYNC_TICKS) - (int16_t) g_mB_ch1_tim_ccr2);
			phase_err_ticks /= (mB_CH1_TIM_XMIT_PSC + 1);		// +1 because frequency ratio = Prescaler + 1!
			g_period_ticks -= phase_err_ticks;

			/* Calculate the deviation of measured period ticks
			 * from the expected value for an ideal clock.
			 */
			g_dev_ticks = g_period_ticks - mB_CH1_TIM_XMIT_NOM_TIMESLICE_TICKS;

			/* Calculate ticks until update event triggering action 2 will happen */
			int32_t nom_ticks = (mB_CH1_TIM_PERIOD * (int32_t) mB_CH1_TIM_XMIT_NOM_TIMESLICE_TICKS) / mB_TIMESLICE_US;
			g_real_ticks = nom_ticks + ((g_dev_ticks * nom_ticks) / ((int32_t) mB_CH1_TIM_XMIT_NOM_TIMESLICE_TICKS));
			g_ue_ticks = (uint16_t) (g_real_ticks - 1);

			uint16_t new_arr = mB_CH1_TIM_PERIOD_SYNC3 + g_mB_ch1_tim_ccr2;
			putreg16(new_arr, STM32_mB_CH1_TIM_ARR);
			g_phase_err = 0;
		}
		else
		{
			putreg16(mB_CH1_TIM_PERIOD_SYNC4, STM32_mB_CH1_TIM_ARR);
			g_phase_err++;
		}

		/* Check error counters to determine how to proceed in the next ISR */
		if (g_sync_err <= CONFIG_MESSBUS_MAX_SYNC_ERR && g_phase_err <= CONFIG_MESSBUS_MAX_PHASE_ERR)
		{
			/* Set new prescaler to cover at least 10ms with 16bit.
			* This setting is not activated immediately, but after the next update event.
			*/
			putreg16(mB_CH1_TIM_XMIT_PSC, STM32_mB_CH1_TIM_PSC);

			/* Activate the trigger for timer 4 */
			clearbit16(STM32_mB_CH1_TIM_CR2, GTIM_CR2_MMS_MASK);
			setbit16(STM32_mB_CH1_TIM_CR2, GTIM_CR2_MMS_UPDATE);

			/* Configure GPIO Alternate function -> USART RX */
			//clearbit32(STM32_mB_CH1_RX_GPIO_AFRx, mB_CH1_RX_AFRx_MASK);
			//setbit32(STM32_mB_CH1_RX_GPIO_AFRx, mB_CH1_RX_AFRx_USART);
			modifybit32(STM32_mB_CH1_RX_GPIO_AFRx,mB_CH1_RX_AFRx_MASK, mB_CH1_RX_AFRx_USART);

			/* Set baudrate */
			putreg32((uint32_t) isr_infotable_ptr->brr, STM32_mB_CH1_USART_BRR);

			/* Prepare and enable the DMA stream */
			putreg32((uint32_t) isr_infotable_ptr->buffer, mB_CH1_RX_DMA_SxMOAR);		// Set the memory address
			putreg32((uint32_t) 0xffff, mB_CH1_RX_DMA_SxNDTR);						// Set the NDTR to max 0xffff
			putreg32(mB_CH1_RX_DMA_INT_SxMASK,mB_CH1_RX_DMA_xIFCR);				// Clear pending flags for safety reasons
			setbit32(mB_CH1_RX_DMA_SxCR, DMA_SCR_EN);									// Enable the stream

			/* Enable USART RE again */
			setbit32(STM32_mB_CH1_USART_CR1, USART_CR1_RE);

			/* Signal that there was another successful SYNC */
			g_messBUSpriv.sync_count++;

#if CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			/* Check for pending trigger of callbacks. */
			if (ch1_single_callback_info.trigger_pending == 1 )
			{
				/* Is there a new sync related callback request available? */
				if (ch1_single_callback_info.request == MESSBUS_SYNC_CALLBACK_REQUEST)
				{
					/* The callback shall take place as soon as possible. Reset hiprio pending flag. */
					ch1_single_callback_info.trigger_pending = 0;

					/* Trigger the PendSV interrupt */
					putreg32( NVIC_INTCTRL_PENDSVSET, NVIC_INTCTRL);
				}
			}
#endif //CONFIG_MESSBUS_USE_CALLBACKS

			/* Finally increment relevant table pointers */
			isr_actiontable_ptr++;

			/* Next case: Passively measure SYNC */
			g_op_mode = 4;
		}
		else
		{
			/* Next case: Actively measure SYNC */
			g_op_mode = 9;
		}

		break;
#endif


#if 0
		/* Compute corrected ARR value to reset approx. 3 us before SYNC.
	 * Prepare high resolution and input capture settings for passive SYNC search.
	 */
	case 4:

		/* Acknowledge the update event interrupt.*/
		putreg16(~GTIM_SR_UIF, STM32_mB_CH1_TIM_SR);

		/* Deactivate the trigger for timer 4, by setting OC1REF als trigger source */
		clearbit16(STM32_mB_CH1_TIM_CR2, GTIM_CR2_MMS_MASK);
		setbit16(STM32_mB_CH1_TIM_CR2, GTIM_CR2_MMS_OC1REF);

		/* Set new ARR value to reset approx. 3us before SYNC */
		putreg16(g_ue_ticks, STM32_mB_CH1_TIM_ARR);

		/* Reset prescaler for highest resolution.
		 * This setting is not activated immediately, but after the next update event.
		 */
		putreg16(mB_CH1_TIM_SYNC_PSC, STM32_mB_CH1_TIM_PSC);

		/* Calculate and set new CCR3 value for next capture interrupt. */
//		g_real_ticks = isr_actiontable_ptr->nom_ticks + ((g_dev_ticks * isr_actiontable_ptr->nom_ticks) / mB_CH1_TIM_XMIT_NOM_TIMESLICE_TICKS);
//		putreg16((uint16_t) g_real_ticks, STM32_mB_CH1_TIM_CCR3);


//XXX
		/* Schedule next ISR or wait for it if it's already too late. Make sure that there are
		 * at least two timer counts remaining until the scheduled next interrupt. One timer
		 * count would not be enough as it could happen that the transition occurs right after
		 * the query of the timer counter register so that we would miss the scheduled interrupt.
		 * If there are less then two timer counts remaining busy wait for the next scheduled
		 * interrupt time and "goto" the switch case routine again. This routine also works when
		 * the next interrupt should already have occurred.
		 */
		g_real_ticks = isr_actiontable_ptr->nom_ticks + ((g_dev_ticks * isr_actiontable_ptr->nom_ticks) / mB_CH1_TIM_XMIT_NOM_TIMESLICE_TICKS);
		if (g_real_ticks > (getreg16(STM32_mB_CH1_TIM_CNT) + 1))
		{
			putreg16(g_real_ticks, STM32_mB_CH1_TIM_CCR3);

			/* Are we already too late? Then go to next case manually! */
			if (g_real_ticks <= getreg16(STM32_mB_CH1_TIM_CNT))
				{
				isr_actiontable_ptr++;
				goto NEXT_ACTION;
				}

			isr_actiontable_ptr++;
		}
		else
		{
			while (g_real_ticks > getreg16(STM32_mB_CH1_TIM_CNT))
				{
				// Do nothing, simply busy wait
				}
			isr_actiontable_ptr++;
			goto NEXT_ACTION;
		}


//XXX
		/* Next case: First action */


		/* Clear the compare flag for safety reasons. */
		putreg16(~GTIM_SR_CC3IF, STM32_mB_CH1_TIM_SR);


		/* Enable Compare interrupts */
		setbit16(STM32_mB_CH1_TIM_DIER, GTIM_DIER_CC3IE);

		/* Next case: First action */
		g_op_mode = isr_actiontable_ptr->op_mode;

		/* Finally increment relevant table pointers */
		isr_actiontable_ptr++;

		break;

	/* This case has been added to allow the client to measure its own timebase
	 * relative to the master's period (10ms) for proper and automated
	 * synchronization.
	 */
	case 8:

		/* Acknowledge the update event interrupt.*/
		putreg16(~GTIM_SR_UIF, STM32_mB_CH1_TIM_SR);
#ifdef mB_SELF_SYNC
		/* Get the period from sync to sync if we already have
		 * a last_ccr1.
		 */
		uint16_t tim4_ccr1 = getreg16(STM32_TIM4_CCR1);
		if (g_n_period_scans > 0)
		{
			uint16_t tim4_period = tim4_ccr1 - g_tim4_last_ccr1;
			g_summarized_period_ticks += (uint32_t) tim4_period;
		}

		/* In any case make the current ccr1 the future last one */
		g_tim4_last_ccr1 = tim4_ccr1;
		g_n_period_scans++;
#endif
		/* Configure 7us period */
		putreg16(mB_CH1_TIM_PERIOD_SYNC1, STM32_mB_CH1_TIM_ARR);

		/* Deactivate the trigger for timer 4, by setting OC1REF als trigger source */
		clearbit16(STM32_mB_CH1_TIM_CR2, GTIM_CR2_MMS_MASK);
		setbit16(STM32_mB_CH1_TIM_CR2, GTIM_CR2_MMS_OC1REF);

		/* Reenable reset of timer 3 when trigger occurs */
		setbit16(STM32_mB_CH1_TIM_SMCR, GTIM_SMCR_RESET);

		/* Next case: Actively measure SYNC again */
		g_op_mode = 0;

		break;

	/* Reconfigure Timer 3 to actively search for SYNC again */
	case 9:

		/* Acknowledge the update event interrupt.*/
		putreg16(~GTIM_SR_UIF, STM32_mB_CH1_TIM_SR);



		/* Set new ARR value to 7us */
		putreg16(mB_CH1_TIM_PERIOD_SYNC1, STM32_mB_CH1_TIM_ARR);

		/* Disable TI2 for IC2 and enable TRC instead */
		clearbit16(STM32_mB_CH1_TIM_CCER, GTIM_CCER_CC2E);									// Disable Ch2 capture
		clearbit16(STM32_mB_CH1_TIM_CCMR1, GTIM_CCMR1_CC2S_MASK);								// Reset Ch2 to output first (this does not affect pin config, so everything is fine)
		setbit16(STM32_mB_CH1_TIM_CCMR1, (GTIM_CCMR_CCS_CCINTRC << GTIM_CCMR1_CC2S_SHIFT));

		/* Reset the period ticks */
		g_summarized_period_ticks = 0;

		/* Enable reset when trigger occurs */
		setbit16(STM32_mB_CH1_TIM_SMCR, GTIM_SMCR_RESET);

		/* Next case: Actively measure SYNC again */
		g_op_mode = 0;

		break;

#endif


	case 4:
		// CCR3 interrupt for transmission
#ifdef mB_DEBUG_LED
		switch_LED_green_on();
#endif


		/* read status flags */
		mB_ch1_tim_sr = getreg16(STM32_mB_CH1_TIM_SR);

		/* check IRQ flag */
		if(!(mB_ch1_tim_sr & GTIM_SR_CC3IF)){
			/*something went wrong leave interrupt pending; case 3 should handle it*/

			/* cleanup Tx specific operations...
			 * Reset the Data Enable pin first:
			 */
			putreg32(mB_CH1_DE_OFF,mB_CH1_DE_BSRR);

			/* Force the DMA stream to disable itself, which should already have happened after
			 * transmission of all bytes according to previous NDTR setting.
			 * Hence, this is only a safety feature to make sure that the next DMA transfer can
			 * start immediately and there is no pending data from the previous transfer. The
			 * disabling procedure may takes some time, as the DMA waits for the current transfer
			 * to complete before actually writing the DMA_SCR_EN bit to zero. But this is ok for
			 * our purposes, as the next transfer will not happen immediately.
			 */
			clearbit32(mB_CH1_TX_DMA_SxCR, DMA_SCR_EN);


			/* cleanup Rx specific operations...
			 * Disable the Receive Enable bit at the USART first. Incomplete transfers will be aborted,
			 * so after enabling the receiver again it will start searching for a start bit again.
			 */
			clearbit32(STM32_mB_CH1_USART_CR1, USART_CR1_RE);

			/* Disable the DMA stream */
			clearbit32(mB_CH1_RX_DMA_SxCR, DMA_SCR_EN);

			isr_mode = 1;

			break;

		}


	NEXT_ACTION:
		/* Acknowledge the compare interrupt.*/
		putreg16(~(GTIM_SR_CC3IF), STM32_mB_CH1_TIM_SR);



		switch(g_op_mode){
		/* Prepare Rx operation */
		case mB_CH1_RX_START:

			/* Rx start specific operations now...
			 * Set baudrate first:
			 */
			putreg32((uint32_t) isr_infotable_ptr->brr, STM32_mB_CH1_USART_BRR);

			/* Prepare and enable the DMA stream */
			putreg32((uint32_t) isr_infotable_ptr->buffer, mB_CH1_RX_DMA_SxMOAR);		// Set the memory address
			putreg32((uint32_t) 0xffff, mB_CH1_RX_DMA_SxNDTR);						// Set the NDTR to max 0xffff
			putreg32(mB_CH1_RX_DMA_INT_SxMASK, mB_CH1_RX_DMA_xIFCR);				// Clear pending flags for safety reasons
			setbit32(mB_CH1_RX_DMA_SxCR, DMA_SCR_EN);									// Enable the stream

			/* Enable the Receive Enable bit at the USART */
			setbit32(STM32_mB_CH1_USART_CR1, USART_CR1_RE);
	break;

		/* Stop Rx operation */
		case mB_CH1_RX_END:

			/* Rx end specific operations now...
			 * Disable the Receive Enable bit at the USART first. Incomplete transfers will be aborted,
			 * so after enabling the receiver again it will start searching for a start bit again.
			 */
			clearbit32(STM32_mB_CH1_USART_CR1, USART_CR1_RE);

			/* Disable the DMA stream */
			clearbit32(mB_CH1_RX_DMA_SxCR, DMA_SCR_EN);

			/* Write the number of received bytes directly into the slotlist (as feedback) */
			*isr_infotable_ptr->buf_length_ptr = 0xffff - ((uint16_t) getreg32(mB_CH1_RX_DMA_SxNDTR));

			/* Indicate the completion of the slot */
			if(g_ch1_end_of_timeslice == 1)
			{
				/* Then this is slot 0... */
				g_messBUSpriv.ch1_last_processed_slot_number = 0;
				g_ch1_end_of_timeslice = 0;
			}
			else
			{
				/* This is not the first slot in a timeslice, so just increment */
				g_messBUSpriv.ch1_last_processed_slot_number++;
			}

	#if CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			/* Check for pending trigger of callbacks. */
			if (ch1_single_callback_info.trigger_pending == 1 )
			{
				/* Are there new slot related callbacks available? */
				if (ch1_single_callback_info.request == MESSBUS_NEXT_SLOT_CALLBACK_REQUEST)
				{
					/* The callback shall take place as soon as possible. Reset hiprio pending flag. */
					ch1_single_callback_info.trigger_pending = 0;

					/* Trigger the PendSV interrupt */
					putreg32( NVIC_INTCTRL_PENDSVSET, NVIC_INTCTRL);
				}
				if (ch1_single_callback_info.request == g_messBUSpriv.ch1_last_processed_slot_number)
				{
					/* The callback shall take place as soon as possible. Reset hiprio pending flag. */
					ch1_single_callback_info.trigger_pending = 0;

					/* Trigger the PendSV interrupt */
					putreg32( NVIC_INTCTRL_PENDSVSET, NVIC_INTCTRL);
				}
			}
	#endif //CONFIG_MESSBUS_USE_CALLBACKS

			/* Increment infotable pointer as we have completed this RX slot now. */
			isr_infotable_ptr++;
			break;

		/* Tx data enable operation */
		case mB_CH1_TX_DE:

			/* Tx data enable specific operations now...
			 * Set the Data Enable pin:
			 */
			putreg32(mB_CH1_DE_ON, mB_CH1_DE_BSRR);

			/* Set baudrate */
			putreg32((uint32_t) isr_infotable_ptr->brr, STM32_mB_CH1_USART_BRR);

			/* Already prepare the DMA stream */
			putreg32((uint32_t) isr_infotable_ptr->buffer, mB_CH1_TX_DMA_SxMOAR);				// Set the memory address
			putreg32((uint32_t) isr_infotable_ptr->buf_length, mB_CH1_TX_DMA_SxNDTR);			// Set the NDTR
			putreg32(mB_CH1_TX_DMA_INT_SxMASK,mB_CH1_TX_DMA_xIFCR);						// Clear pending flags for safety reasons

			break;

		/* Tx start operation */
		case mB_CH1_TX_START:

			/* Tx start specific operations now...
			 * Enable the previously prepared DMA stream now:
			 */
			setbit32(mB_CH1_TX_DMA_SxCR, DMA_SCR_EN);

			break;

		/* Tx end operation */
		case mB_CH1_TX_END:

			/* Tx end specific operations now...
			 * Reset the Data Enable pin first:
			 */
			putreg32(mB_CH1_DE_OFF,mB_CH1_DE_BSRR);

			/* Force the DMA stream to disable itself, which should already have happened after
			 * transmission of all bytes according to previous NDTR setting.
			 * Hence, this is only a safety feature to make sure that the next DMA transfer can
			 * start immediately and there is no pending data from the previous transfer. The
			 * disabling procedure may takes some time, as the DMA waits for the current transfer
			 * to complete before actually writing the DMA_SCR_EN bit to zero. But this is ok for
			 * our purposes, as the next transfer will not happen immediately.
			 */
			clearbit32(mB_CH1_TX_DMA_SxCR, DMA_SCR_EN);

			/* Indicate the completion of the slot. This time it cannot be slot 0, because for
			 * the client the first slot must be the reception of the master message and thus
			 * must not be TX. Hence, we just increment. */
			g_messBUSpriv.ch1_last_processed_slot_number++;

	#if CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
			/* Check for pending trigger of callbacks. */
			if (ch1_single_callback_info.trigger_pending == 1 )
			{
				/* Are there new slot related callbacks available? */
				if (ch1_single_callback_info.request == MESSBUS_NEXT_SLOT_CALLBACK_REQUEST)
				{
					/* The callback shall take place as soon as possible. Reset hiprio pending flag. */
					ch1_single_callback_info.trigger_pending = 0;

					/* Trigger the PendSV interrupt */
					putreg32( NVIC_INTCTRL_PENDSVSET, NVIC_INTCTRL);
				}
				if (ch1_single_callback_info.request == g_messBUSpriv.ch1_last_processed_slot_number)
				{
					/* The callback shall take place as soon as possible. Reset hiprio pending flag. */
					ch1_single_callback_info.trigger_pending = 0;

					/* Trigger the PendSV interrupt */
					putreg32( NVIC_INTCTRL_PENDSVSET, NVIC_INTCTRL);
				}
			}
	#endif //CONFIG_MESSBUS_USE_SINGLE_CALLBACKS

			/* Increment infotable pointer */
			isr_infotable_ptr++;

			break;

	#if CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
		/* Wake up operation */
		case mB_CH1_WAKE_UP:

			/* Are wake up callbacks enabled? */
			if (ch1_wake_up_callback_info.enabled == 1)
			{
				/* Check if the last wake up callback is done */
				if (ch1_wake_up_callback_info.pendsv_pending == 0)
				{
					/* Mark pendSV pending */
					ch1_wake_up_callback_info.pendsv_pending = 1;

					/* Trigger the PendSV interrupt */
					putreg32( NVIC_INTCTRL_PENDSVSET, NVIC_INTCTRL);
				}
			}


			break;
	#endif //CONFIG_MESSBUS_USE_WAKE_UP_CALLBACKS
		}


			/* Next case: */
			g_op_mode = isr_actiontable_ptr->op_mode;


			if (g_op_mode != mB_CH1_DONE){

			/* Schedule next ISR or wait for it if it's already too late. Make sure that there are
			 * at least two timer counts remaining until the scheduled next interrupt. One timer
			 * count would not be enough as it could happen that the transition occurs right after
			 * the query of the timer counter register so that we would miss the scheduled interrupt.
			 * If there are less then two timer counts remaining busy wait for the next scheduled
			 * interrupt time and "goto" the switch case routine again. This routine also works when
			 * the next interrupt should already have occurred.
			 */

			g_real_ticks = isr_actiontable_ptr->nom_ticks + ((deviation_ppm * isr_actiontable_ptr->nom_ticks) / US_PER_SEC) - mB_CH1_TIM_XMIT_OFFSET_TICKS;


			isr_actiontable_ptr++;

			putreg16(g_real_ticks, STM32_mB_CH1_TIM_CCR3);

			if (g_real_ticks <= getreg16(STM32_mB_CH1_TIM_CNT))
			{
#ifdef mB_DEBUG_LED
				switch_LED_green_off();
				switch_LED_green_on();
				switch_LED_red_on();
				switch_LED_red_off();
#endif

				goto NEXT_ACTION;
			}

#if 0

			if (g_real_ticks > (getreg16(STM32_mB_CH1_TIM_CNT) + 1))
			{
				putreg16(g_real_ticks, STM32_mB_CH1_TIM_CCR3);

				/* Are we already too late? Then go to next case manually! */
				if (g_real_ticks <= getreg16(STM32_mB_CH1_TIM_CNT))
					{
					isr_actiontable_ptr++;
					goto NEXT_ACTION;
					}

				isr_actiontable_ptr++;
			}
			else
			{
				while (g_real_ticks > getreg16(STM32_mB_CH1_TIM_CNT))
					{
					// Do nothing, simply busy wait
					}
				isr_actiontable_ptr++;
				goto NEXT_ACTION;
			}
#endif
			} else {
				isr_mode = 1;
			}
#ifdef mB_DEBUG_LED
			switch_LED_green_off();
#endif
	  break;
	}

#if mB_DEBUG_VISUALIZE_INTERRUPTS
	/* Reset Debug-GPIO */
	setbit32(GPIO_DEBUG_BSRR, GPIO_DEBUG_ISR_OFF);
#endif
}

/****************************************************************************
 * Name: up_setup
 *
 * Description:
 *   Setup the hardware but don't start it already.
 *   This method is called by the upper half after a successful call to the
 *   upper half's open method from application logic.
 *
 ****************************************************************************/

static int up_setup(void)
{
	int ret;

#if mB_DEBUG_VISUALIZE_INTERRUPTS
	/* Configure dummy GPIO */
	ret = stm32_configgpio(GPIO_DEBUG_ISR);
	setbit32(GPIO_DEBUG_BSRR, GPIO_DEBUG_ISR_ON);
#endif

	/* Attach mB_CH1_TIM ram vector */
	ret = up_ramvec_attach(mB_CH1_TIM_IRQ, mB_ch1_tim_handler);
	if (ret < 0)
		{
			// Error handling ???
		}

	/* Set the priority of the TIM interrupt vector */
	ret = up_prioritize_irq(mB_CH1_TIM_IRQ, NVIC_SYSH_HIGH_PRIORITY);
	if (ret < 0)
		{
			// Error handling?
		}

	/* Enable the timer interrupt at the NVIC */
	up_enable_irq(mB_CH1_TIM_IRQ);

#if CONFIG_MESSBUS_USE_SINGLE_CALLBACKS
	/* Attach PendSV ram vector */
	ret = irq_attach(STM32_IRQ_PENDSV, pendsv_handler, NULL);
	if (ret < 0)
		{
			// Error handling ???
		}

	/* Enable the timer interrupt at the NVIC */
	up_enable_irq(STM32_IRQ_PENDSV);
#endif //CONFIG_MESSBUS_USE_CALLBACKS

	/* Make sure peripheral clocks are enabled */
	modifyreg32(STM32_mB_CH1_TIM_RCC_APBxENR, 0, mB_CH1_RCC_APBxENR_TIMyEN);
#ifdef mB_SELF_SYNC
	modifyreg32(STM32_RCC_APB1ENR, 0, RCC_APB1ENR_TIM4EN);
#endif
	modifyreg32(STM32_mB_CH1_DMA_RCC_AHBxENR, 0, mB_CH1_RCC_AHBxENR_DMAyEN);
	modifyreg32(STM32_mB_CH1_USART_RCC_APBxENR, 0, mB_CH1_RCC_APBxENR_USARTyEN);

	/* Configure USART related GPIOs as alternate functions.
	 * The Timer3 Ch2In pin is the same as the USART RX pin and hence
	 * must not be configured here!
	 * The USART RX configuration provides higher GPIO speed and is therefore
	 * chosen here instead of the Timer3 Ch2In configuration.
	 */
	ret = stm32_configgpio(GPIO_mB_CH1_RX);				// RX GPIO
	ret = stm32_configgpio(GPIO_mB_CH1_TX);				// TX GPIO

	/* Reconfigure GPIO AF as Timer input for startup
	 */
	modifyreg32(STM32_mB_CH1_RX_GPIO_AFRx,mB_CH1_RX_AFRx_MASK,mB_CH1_RX_AFRx_TIM);


	/* Try to take USART's Rx DMA stream in the OS. It will be locked with
	 * a semaphore so that no other driver can use the stream. If another
	 * driver already uses the stream stm32_dmachannel waits until the
	 * specified stream is released. This is a safety feature to make sure
	 * the driver has exclusive access to the DMA stream.
	 *
	 * Also initially configure the stream with the method stm32_dmasetup.
	 */
	uint32_t scr;
	DMA_HANDLE mB_ch1_rx_dma;
	scr = (DMA_SCR_DIR_P2M | DMA_SCR_MINC | DMA_SCR_PRIVERYHI);
	mB_ch1_rx_dma = stm32_dmachannel(mB_CH1_RX_DMAMAP);
#if defined(CONFIG_ARCH_CHIP_STM32F7)
	stm32_dmasetup(mB_ch1_rx_dma, STM32_mB_CH1_USART_RDR, 0, 0, scr);
#elif defined(CONFIG_STM32_STM32F4XXX)
	stm32_dmasetup(mB_ch1_rx_dma, STM32_mB_CH1_USART_DR, 0, 0, scr);
#endif

	/* Try to take USART's Tx DMA stream in the OS. It will be locked with
	 * a semaphore so that no other driver can use the stream. If another
	 * driver already uses the stream stm32_dmachannel waits until the
	 * specified stream is released. This is a safety feature to make sure
	 * the driver has exclusive access to the DMA stream.
	 *
	 * Also initially configure the stream with the method stm32_dmasetup.
	 */
	DMA_HANDLE mB_ch1_tx_dma;
	scr = (DMA_SCR_DIR_M2P | DMA_SCR_MINC | DMA_SCR_PRIHI);
	mB_ch1_tx_dma = stm32_dmachannel(mB_CH1_TX_DMAMAP);
#if defined(CONFIG_ARCH_CHIP_STM32F7)
	stm32_dmasetup(mB_ch1_tx_dma, STM32_mB_CH1_USART_TDR, 0, 0, scr);
#elif defined(CONFIG_STM32_STM32F4XXX)
	stm32_dmasetup(mB_ch1_tx_dma, STM32_mB_CH1_USART_DR, 0, 0, scr);
#endif

	/* Setup USART.
	 * Don't configure baudrate already. This will be done via
	 * convert_slotlist and the ISR */
	modifyreg32(STM32_mB_CH1_USART_CR3, 0, USART_CR3_DMAR);										// Enable DMA receiver for USART
	modifyreg32(STM32_mB_CH1_USART_CR3, 0, USART_CR3_DMAT);										// Enable DMA transmitter for USART
	modifyreg32(STM32_mB_CH1_USART_CR1, 0, USART_CR1_OVER8);										// Enable oversampling by 8

	/* Calculate the nominal ticks per period, which will be
	 * needed for conversion of slotlists.
	 */
	uint16_t psc = mB_CH1_TIM_XMIT_PSC;
//XXX	uint32_t nom_period_ticks = mB_CH1_TIM_CLKIN / ((psc + 1) * 100);
//XXX	g_nom_period_ticks = (uint16_t) nom_period_ticks;

#if mB_DEBUG_VISUALIZE_SYNC_TRIGGER_WITH_TIM1_OPM
	/* Visualize trigger via timer 1 in one pulse mode */
	modifyreg32(STM32_RCC_APB2ENR, 0, RCC_APB2ENR_TIM1EN);
	ret = stm32_configgpio(GPIO_TIM1_CH1OUT_1);

	/* Configure Timer 1 as slave of Timer 2 in triggered one pulse mode */
	putreg16(250, STM32_TIM1_ARR);												// Configure period
	modifyreg16(STM32_TIM1_CR1, 0, ATIM_CR1_OPM);											// One pulse mode
	modifyreg16(STM32_TIM1_SMCR, 0, ATIM_SMCR_ITR2);										// Select ITR2 as input trigger
	modifyreg16(STM32_TIM1_SMCR, 0, ATIM_SMCR_TRIGGER);										// Select slave trigger mode
	modifyreg16(STM32_TIM1_EGR, 0, ATIM_EGR_UG);											// Enable update event
	modifyreg16(STM32_TIM1_BDTR, 0, ATIM_BDTR_MOE);											// Main output enable

	putreg16(1, STM32_TIM1_CCR1);															// Configure delay time
	modifyreg16(STM32_TIM1_CCMR1, 0, (ATIM_CCMR_MODE_PWM2 << ATIM_CCMR1_OC1M_SHIFT));		// PWM2-Mode
	modifyreg16(STM32_TIM1_CCMR1, 0, ATIM_CCMR1_OC1PE);										// Output compare 1 preload enable
	modifyreg16(STM32_TIM1_CCER, 0, ATIM_CCER_CC1P);										// Output polarity active low
	modifyreg16(STM32_TIM1_CCER, 0, ATIM_CCER_CC1E);										// Output enable
#endif


	return 0;
}

/****************************************************************************
 * Name: up_start
 *
 * Description:
 *   Enable the USART and configure and start timer 3.
 *
 ****************************************************************************/

static int up_start(void)
{
	/* Enable USART 6. The RE bit will be set via Timer 3's ISR. */
	modifyreg32(STM32_mB_CH1_USART_CR1, 0, USART_CR1_UE);											// Enable USART
	modifyreg32(STM32_mB_CH1_USART_CR1, 0, USART_CR1_TE);											// Enable the transmitter

	/* Configure Timer as active SYNC catcher */
	putreg16(mB_CH1_TIM_SYNC_PSC, STM32_mB_CH1_TIM_PSC);										// Configure prescaler
	putreg16(0xFFFF, STM32_mB_CH1_TIM_ARR);														//max timeout

	/*TIM CH1: captures rising edge of sync pulse on ch2*/
	modifyreg32(STM32_mB_CH1_TIM_CCMR1, 0, (GTIM_CCMR_CCS_CCIN2 << GTIM_CCMR1_CC1S_SHIFT));		// Remap TI2 to catch input on CH1
	/*TIM CH2: captures falling edge of sync pulse and triggers/resets timer*/
	modifyreg32(STM32_mB_CH1_TIM_CCMR1, 0, (GTIM_CCMR_CCS_CCIN1 << GTIM_CCMR1_CC2S_SHIFT));	// Ch2 is input for trigger TRC

	modifyreg16(STM32_mB_CH1_TIM_CCER, GTIM_CCER_CC1NP | GTIM_CCER_CC1P, 0);					// TI2FP1 sensitive to rising edge
	modifyreg16(STM32_mB_CH1_TIM_CCER, GTIM_CCER_CC2NP, GTIM_CCER_CC2P);					// TI2FP2 sensitive to falling edge
	modifyreg16(STM32_mB_CH1_TIM_SMCR, 0, GTIM_SMCR_TI2FP2);								// TI2 is trigger input
	modifyreg16(STM32_mB_CH1_TIM_CR1, 0, GTIM_CR1_URS);											// Update event interrupt only for overflow (not for reset)
//	modifyreg16(STM32_mB_CH1_TIM_CCER, 0, GTIM_CCER_CC1E);										// Enable Ch1 capture
//	modifyreg16(STM32_mB_CH1_TIM_DIER, 0, GTIM_DIER_UIE);										// Enable update interrupt

	/*TIM CH3: interrupt @ 7us*/
	modifyreg32(STM32_mB_CH1_TIM_CCMR2,GTIM_CCMR2_CC3S_MASK,GTIM_CCMR_CCS_CCOUT << GTIM_CCMR2_CC3S_SHIFT);//configure as output compare

	putreg16(mB_CH1_TIM_SYNC_CHECK_OFFSET_TICKS, STM32_mB_CH1_TIM_CCR3);										// set interrupt to 7us
	modifyreg16(STM32_mB_CH1_TIM_DIER, 0, GTIM_DIER_CC3IE);										// Enable CH3 interrupt

	/*TIM CH4: TRGO @ 10us after valid sync pulse*/
	modifyreg32(STM32_mB_CH1_TIM_CCMR2,GTIM_CCMR2_CC4S_MASK,GTIM_CCMR_CCS_CCOUT << GTIM_CCMR2_CC4S_SHIFT);//configure as output compare
	putreg16(mB_CH1_TIM_SYNC_TRGO_OFFSET_TICKS, STM32_mB_CH1_TIM_CCR4);										// set TRGO trigger to 10us
	modifyreg32(STM32_mB_CH1_TIM_CCMR2,GTIM_CCMR2_OC4M_MASK,GTIM_CCMR_MODE_OCREFLO << GTIM_CCMR2_OC4M_SHIFT);//force OC4ref low
	modifyreg16(STM32_mB_CH1_TIM_CR2, 0, GTIM_CR2_MMS_OC4REF);									// OC4ref as TRGO

	modifyreg16(STM32_mB_CH1_TIM_CCER, 0,GTIM_CCER_CC1E | GTIM_CCER_CC2E | GTIM_CCER_CC3E | GTIM_CCER_CC4E); // enable ch1,2,3,4

#ifdef CONFIG_ARCH_CHIP_STM32F7
	modifyreg16(STM32_mB_CH1_TIM_CR1, 0, GTIM_CR1_OPM);											// Set one pulse mode
	modifyreg32(STM32_mB_CH1_TIM_SMCR, GTIM_SMCR_SMS_MASK, GTIM_SMCR_SMS);						// Combined Reset and Trigger mode
#else
	modifyreg16(STM32_mB_CH1_TIM_SMCR, 0, GTIM_SMCR_RESET);										// Reset mode
	modifyreg16(STM32_mB_CH1_TIM_CR1, 0, GTIM_CR1_CEN);											// Enable the counter
#endif

	/*enable RS485 receiver*/
	stm32_gpiowrite(GPIO_mB_CH1_RE,false);
//	switch_LED_red_on();


//	stm32_configgpio( (GPIO_INPUT|GPIO_PULLUP|GPIO_SPEED_100MHz|GPIO_PORTB|GPIO_PIN7));				// RX GPIO
//	stm32_configgpio( (GPIO_ALT|GPIO_AF2|GPIO_SPEED_50MHz|GPIO_FLOAT|GPIO_PORTB|GPIO_PIN7));				// RX GPIO
//	bool b7last=stm32_gpioread(GPIO_PORTB | GPIO_PIN7);

#if 0
for(;;){
#if 0
	bool b7=stm32_gpioread(GPIO_PORTB | GPIO_PIN7);
	if(b7!=b7last){
		if(b7){
			printf("b7: true\n");
		} else {
			printf("b7: false\n");
		}
		b7last=b7;
	}
#endif

#if 1
	//uint32_t reg32;
	printf("initialized debug loop\n");

	/* read status flags */
	uint16_t mB_ch1_tim_cnt = getreg16(STM32_mB_CH1_TIM_CNT);
	printf("CNT: 0x%04hX\n",mB_ch1_tim_cnt);

	/* read status flags */
	uint16_t mB_ch1_tim_sr = getreg16(STM32_mB_CH1_TIM_SR);
	printf("SR: 0x%04hX\n",mB_ch1_tim_sr);

	uint16_t mB_ch1_tim_ccr1 = getreg16(STM32_mB_CH1_TIM_CCR1);
	printf("CCR1: 0x%04hX\n",mB_ch1_tim_ccr1);

	uint16_t mB_ch1_tim_ccr2 = getreg16(STM32_mB_CH1_TIM_CCR2);
	printf("CCR2: 0x%04hX\n",mB_ch1_tim_ccr2);
#endif

	sleep(1);
}

#endif

	return 0;
}

/****************************************************************************
 * Name: up_stop
 *
 * Description:
 *   Not provided at this stage of development. Should principally be the
 *   counterpart to up_start.
 *
 ****************************************************************************/

static int up_stop(void)
{
	/* Maybe needed later, so the function is already provided and can be
	 * called from the upper half as it is bound to it via messBUS_ops_s.
	 */

	return 0;
}

/****************************************************************************
 * Name: up_shutdown
 *
 * Description:
 *   Not provided at this stage of development. Should principally be the
 *   counterpart to up_setup.
 *
 ****************************************************************************/

static int up_shutdown(void)
{
	/* Maybe needed later, so the function is already provided and can be
	 * called from the upper half as it is bound to it via messBUS_ops_s.
	 */

	return 0;
}

/****************************************************************************
 * Name: up_update_buffers
 *
 * Description:
 *   Invalidates the F7 chip's data cache to make sure the CPU uses the
 *   newest rx buffers. As only the F7 architecture has a cache, this
 *   function is only provided for F7 chips.
 *
 *   This is a rather bad workaround, because the call consumes about 10us.
 *   A better approach would be to use the MPU to declare the buffers
 *   uncacheable within the application.
 *
 *   Another problem is that the hardware abstraction of the upper half is
 *   partly violated, because it has to know whether a F7 chip is used or not.
 *   This can only be overcome by having a buffer inside the driver itself
 *   parsing the data to the destination buffers via a memcpy and not via DMA.
 *   This approach would on the one hand certainly decrease efficiency and
 *   increase latencies on the other hand.
 *
 ****************************************************************************/


static void up_update_buffers(void)
{
#ifdef CONFIG_ARCH_CHIP_STM32F7
	/* Invalidate the whole cache. This method takes approx. 10 us which
	 * is quite a long time. This could be improved by using single cache
	 * lines or the MPU.
	 */
	up_invalidate_dcache_all();
#endif
}

/****************************************************************************
 * Name: up_get_clock_deviation
 *
 * Description:
 *   This function returns the current deviation of timer 3's clock input
 *   compared to expected TIM_CLKIN from board.h relative to the messbus
 *   master in ppm. The calculation scheme is as follows:
 *
 *   ppm = (nom_val - act_val) * 1e6 / nom_val
 *
 *   nom_val = TIM_CLKIN / ((PSC + 1) * 100 Hz)
 *
 *   act_val = g_period_ticks
 *
 *   g_period_ticks is automatically updated each timeslice.
 *
 ****************************************************************************/


static int up_get_clock_deviation(void)
{
	uint16_t psc = mB_CH1_TIM_XMIT_PSC;
	int32_t nom_val = mB_CH1_TIM_CLKIN / ((psc + 1) * 100);
	int32_t act_val = (int32_t) g_period_ticks;
	/* With rounding */
	int32_t ppm = (((nom_val - act_val) * 1000000) + (nom_val/2)) / nom_val;
	return ppm;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

void messBUSClient_initialize(void)
{
	/* Perform registration of the driver.
	 * This method should be called from board-specific stm32_bringup.c
	 */
	int ret;
printf("messBUSClient_initialize\n");
//switch_LED_green_off();
//switch_LED_red_off();
	/* Call the upper half to register the whole driver in the VFS. */
	ret = messBUSClient_register("/dev/messBusClient", &g_messBUSpriv);
	if (ret < 0)
	{
printf("error registering messBUSClient\n")	;
		// Error handling??
	}
printf("messBUSClient register done\n");
}

