/************************************************************************************
 * configs/mB-master-stm32f756ig/src/stm32_ad5662.c
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <errno.h>
#include <debug.h>

#include <nuttx/spi/spi.h>
#include <nuttx/analog/ad5662.h>

#include "stm32_spi.h"
#include "mB-master-stm32f756ig.h"

#if defined(CONFIG_STM32F7_SPI1) && defined(CONFIG_DAC_AD5662)

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/
#ifndef OK
#  define OK 0
#endif


/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_ad5562initialize
 *
 * Description:
 *   Initialize and register the AD5562 driver.
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ************************************************************************************/

int stm32_ad5662_initialize()
{
	FAR struct spi_dev_s *spi;

	spi = stm32_spibus_initialize(AD5662_SPI_BUS);
	if (!spi)
	{
		return -ENODEV;
	}

	/*register the ad5662 character driver */

	ad5662_register(AD5662_DEV_PATH, spi, AD5662_SPI_DEV);
	return OK;
}

#endif /* CONFIG_SPI && CONFIG_AD5662 */

