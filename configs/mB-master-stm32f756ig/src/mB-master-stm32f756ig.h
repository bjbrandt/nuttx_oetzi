/****************************************************************************************************
 * configs/mB-master-stm32f756ig/src/mB-master-st32f756ig.h
 *
 ****************************************************************************************************/

#ifndef __CONFIGS_MB_MASTER_STM32F756IG_H
#define __CONFIGS_MB_MASTER_STM32F756IG_H

/****************************************************************************************************
 * Included Files
 ****************************************************************************************************/

#include <nuttx/config.h>
#include <nuttx/compiler.h>
#include <stdint.h>
#if defined(CONFIG_STM32F7_SPI4 )
# include <nuttx/spi/spi.h>
#endif

/****************************************************************************************************
 * Pre-processor Definitions
 ****************************************************************************************************/
/* LED ***************************************************************************/
#define GPIO_LED1       (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_100MHz|\
                         GPIO_OUTPUT_CLEAR|GPIO_PORTE|GPIO_PIN6) /* green */

#define GPIO_LED2       (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_100MHz|\
                         GPIO_OUTPUT_CLEAR|GPIO_PORTE|GPIO_PIN5) /* red */

/* SPI LED ***********************************************************************/
#if defined(CONFIG_STM32F7_SPI4 )
#define GPIO_LED_SPI_LD    (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
                            GPIO_OUTPUT_CLEAR|GPIO_PORTH|GPIO_PIN6)   /* Serial Data Load */

#define GPIO_LED_SPI_OEN   (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
                            GPIO_OUTPUT_SET|GPIO_PORTH|GPIO_PIN9)   /* Output Enable */

#define SPI_LED_BUS         4
#define SPI_LED_BUS_FREQ    1000000
#define SPI_LED_BUS_MODE    SPIDEV_MODE0
#define SPI_LED_BUS_NBITS   8
#endif

#if 0
/* SPI DAC AD5662 ****************************************************************/
#if defined(CONFIG_STM32F7_SPI1 )
# define AD5662_SPI_BUS       1
# define AD5662_SPI_DEV       0
# define AD5662_DEV_PATH      "/dev/ad5662"

# define GPIO_SPI1_CS         (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
                               GPIO_OUTPUT_SET|GPIO_PORTA|GPIO_PIN4)   /* ~SYNC */
#endif
#endif


/* USB HS with external ULPI *****************************************************/

#define GPIO_OTGFSHS_ULPI_RESETB  (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
                               GPIO_OUTPUT_CLEAR|GPIO_PORTC|GPIO_PIN1)


#ifdef CONFIG_USBHOST
#  define GPIO_OTGFSHS_OVER (GPIO_INPUT|GPIO_EXTI|GPIO_PULLUP|\
                           GPIO_SPEED_100MHz|GPIO_PUSHPULL|\
                           GPIO_PORTE|GPIO_PIN2)

#else
#  define GPIO_OTGFSHS_OVER (GPIO_INPUT|GPIO_PULLUP|GPIO_SPEED_100MHz|\
                           GPIO_PUSHPULL|GPIO_PORTE|GPIO_PIN2)
#endif







/****************************************************************************************************
 * Public data
 ****************************************************************************************************/



#ifndef __ASSEMBLY__

/****************************************************************************************************
 * Public Functions
 ****************************************************************************************************/

/****************************************************************************************************
 * Name: stm32_bringup
 *
 * Description:
 *   Perform architecture-specific initialization
 *
 *   CONFIG_BOARD_INITIALIZE=y :
 *     Called from board_initialize().
 *
 *   CONFIG_BOARD_INITIALIZE=n && CONFIG_LIB_BOARDCTL=y :
 *     Called from the NSH library
 *
 ****************************************************************************************************/

int stm32_bringup(void);

/****************************************************************************
 * Name: stm32_spidev_initialize
 *
 * Description:
 *   Called to configure SPI chip select GPIO pins for the mB-Master F7
 *   board.
 *
 ****************************************************************************/
void weak_function stm32_spidev_initialize(void);


/************************************************************************************
 * Name: stm32_spiled_initialize
 *
 * Description:
 *   Called to create the defined SPI buses and to initialize SPI LED functionality.
 *
 ************************************************************************************/
#if defined(CONFIG_STM32F7_SPI4) && defined(CONFIG_ARCH_HAVE_SPI_LEDS)
int stm32_spiled_initialize(void);
#endif


/************************************************************************************
 * Name: stm32_ad5662_initialize
 *
 * Description:
 *   Called to create the defined SPI buses and to initialize SPI DAC functionality.
 *
 ************************************************************************************/
#if defined(CONFIG_STM32F7_SPI1) && defined(CONFIG_DAC_AD5662)
int stm32_ad5662_initialize(void);
#endif

/****************************************************************************
 * Name: stm32_hptc_setup
 *
 * Description:
 *   Initialize HPTC and register the HPTC device.
 *
 ****************************************************************************/

#ifdef CONFIG_HPTC
int stm32_hptc_setup(void);
#endif

/************************************************************************************
 * Name: stm32_usbinitialize
 *
 * Description:
 *   Called from stm32_usbinitialize very early in inialization to setup USB-related
 *   GPIO pins.
 *
 ************************************************************************************/

#ifdef CONFIG_STM32F7_OTGFSHS
void stm32_usbinitialize(void);
#endif

#endif /* __ASSEMBLY__ */
#endif /* __CONFIGS_MB_MASTER_STM32F756IG_H */
