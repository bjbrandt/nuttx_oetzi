/************************************************************************************
 * configs/mb_imu/src/stm32_adis16xxx.c
 *
 *   Copyright (C) 2017 Stefan Nowak
 *   Author: Stefan Nowak <stefan.nowak@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <errno.h>
#include <debug.h>

#include <nuttx/spi/spi.h>
#include <nuttx/sensors/adis16xxx.h>

#include "stm32_spi.h"

#include "mb_imu.h"
#include <stm32_gpio.h>

#if defined(CONFIG_MB_IMU_ADIS16XXX) && defined(CONFIG_SENSORS_ADIS16XXX)

//FIXME: add HPTC_ASYNC_IRQ configuration
#define USE_HPTC_ASYNC_IRQ

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/



/************************************************************************************
 * Private Functions
 ************************************************************************************/

/************************************************************************************
 * Name: attach_di_adis16xxx
 *
 * Description:
 *   Attach the adis16xxx interrupt handler
 *
 * Input parameters:
 *   *config - The adis16xxx instance configuration data containing the IRQ number,
 *     device ID and interrupt handler
 *   interrupt_handler - The interrupt handler to attach
 *   arg -
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ************************************************************************************/
int attach_di_adis16xxx(FAR struct adis16xxx_config_s *config, xcpt_t interrupt_handler)
{
    return stm32_gpiosetevent(GPIO_MB_IMU_ADIS16XXX_DATA_READY,
                              true,
                              false,
                              false,
                              interrupt_handler,
                              NULL );
}

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_adis16xxxinitialize
 *
 * Description:
 *   Initialize and register the ADIS16XXX 3-axis accelerometer.
 *
 * Input parameters:
 *   devpath - The full path to the driver to register. E.g., "/dev/imu0"
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ************************************************************************************/
 
int stm32_adis16xxxinitialize(FAR const char *devpath)
{
  static struct adis16xxx_config_s imu0_config;
  struct spi_dev_s *spi;
  int ret;
  static int i=0;

  sninfo("Initializing ADIS16XXX: %d\n",i);

  i++;
  

  imu0_config.spi_devid=0;

#if 0
  imu0_config.irq=56;  /*STM32_IRQ_EXTI1510=STM32_IRQ_FIRST + 40 */
  imu0_config.attach = &attach_di_adis16xxx;
#else
  imu0_config.attach = NULL; //use hptc input instead of GPIN interrupt
#endif

  sninfo("Initializing spibus(4)\n");
  spi = stm32_spibus_initialize(4);
  if (!spi)
    {
      spiinfo("Failed to initialize SPI port\n");
      ret = -ENODEV;
    }
  else
    {
	  sninfo("adis16xxx_register\n");
      ret = adis16xxx_register(devpath, spi, &imu0_config);
    }
  sninfo("done.\n");
  return ret;
}

#endif /* CONFIG_MB_IMU_ADIS16XXX && CONFIG_ADIS16XXX */
