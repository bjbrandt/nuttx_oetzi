/************************************************************************************
 * 
 *
 ************************************************************************************/

#ifndef __CONFIGS_mB_IAcc_SRC_mB_IAcc_H
#define __CONFIGS_mB_IAcc_SRC_mB_IAcc_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#include <nuttx/compiler.h>
#include <stdint.h>

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* Configuration ********************************************************************/

/* procfs File System */

#ifdef CONFIG_FS_PROCFS
#  ifdef CONFIG_NSH_PROC_MOUNTPOINT
#    define STM32_PROCFS_MOUNTPOINT CONFIG_NSH_PROC_MOUNTPOINT
#  else
#    define STM32_PROCFS_MOUNTPOINT "/proc"
#  endif
#endif

/* mB_IAcc GPIO Pin Definitions **************************************************/
/* LED
 *
 * The Nucleo-144 board has numerous LEDs but only three, LD1 a Green LED, LD2 a
 * Blue LED and LD3 a Red LED, that can be controlled by software. The following definitions assume
 * the default Solder Bridges are installed.
 */

#define GPIO_LD1       (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz | GPIO_OUTPUT_CLEAR | \
                        GPIO_PORTA | GPIO_PIN9)
#define GPIO_LD2       (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz | GPIO_OUTPUT_CLEAR | \
                        GPIO_PORTA | GPIO_PIN8)


#define GPIO_LED_GREEN GPIO_LD1
#define GPIO_LED_RED   GPIO_LD2

#define LED_DRIVER_PATH "/dev/userleds"


/* SPI ***************************************************************************/

#define GPIO_SPI_CS    (GPIO_OUTPUT | GPIO_PUSHPULL | GPIO_SPEED_50MHz | \
                        GPIO_OUTPUT_SET)

#define GPIO_SPI1_CS0   (GPIO_SPI_CS | GPIO_PORTA | GPIO_PIN4)
#define GPIO_SPI4_CS0   (GPIO_SPI_CS | GPIO_PORTE | GPIO_PIN0)


/* Logical SPI Chip Selects used to index */

#define mB_IAcc_SPI_BUS1_CS0  0
#define mB_IAcc_SPI_BUS4_CS0  1

/************************************************************************************
 * Public data
 ************************************************************************************/

#ifndef __ASSEMBLY__

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_spidev_initialize
 *
 * Description:
 *   Called to configure SPI chip select GPIO pins for the Nucleo-144 board.
 *
 ************************************************************************************/

#if defined(CONFIG_SPI)
void stm32_spidev_initialize(void);
#endif

/************************************************************************************
 * Name: stm32_bma456_initialize
 *
 * Description:
 *
 ************************************************************************************/

#if defined(CONFIG_SENSORS_BMA456)
 int stm32_bma456_initialize(int num);
#endif

/************************************************************************************
 * Name: stm32_mpu9250_initialize
 *
 * Description:
 *
 ************************************************************************************/

#if defined(CONFIG_SENSORS_MPU9250)
 int stm32_mpu9250_initialize(int num);
#endif

/************************************************************************************
 * Name: stm32_spidev_bus_test
 *
 * Description:
 *   Called to create the defined SPI buses and test them by initializing them
 *   and sending the NUCLEO_SPI_TEST (no chip select).
 *
 ************************************************************************************/

#if defined(CONFIG_mB_IAcc_SPI_TEST)
int stm32_spidev_bus_test(void);
#endif

/************************************************************************************
 * Name: stm32_dma_alloc_init
 *
 * Description:
 *   Called to create a FAT DMA allocator
 *
 * Returned Value:
 *   0 on success or -ENOMEM
 *
 ************************************************************************************/

void stm32_dma_alloc_init(void);

#if defined (CONFIG_FAT_DMAMEMORY)
int stm32_dma_alloc_init(void);
#endif

/************************************************************************************
 * Name: stm32_bbsram_int
 ************************************************************************************/

#ifdef CONFIG_STM32F7_BBSRAM
int stm32_bbsram_int(void);
#endif

#endif /* __ASSEMBLY__ */
#endif /* __CONFIGS_mB_IAcc_SRC_mB_IAcc_H */
