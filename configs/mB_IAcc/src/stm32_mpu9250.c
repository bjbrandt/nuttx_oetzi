/************************************************************************************
 * configs/mB_IAcc/src/stm32_mpu9250.c
 *
 
 *
 ************************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/


#include <nuttx/config.h>

#include <errno.h>
#include <debug.h>

#include <nuttx/spi/spi.h>
#include <nuttx/sensors/mpu9250.h>

#include <stdio.h>
#include "stm32_spi.h"
#include "mB_IAcc.h"


#ifdef CONFIG_SENSORS_MPU9250

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32_mpu9250_initialize
 *
 * Description:
 *   Initialize SPI and start the register function from the BMA456 driver.
 *   register path /dev/BMA456_"num"
 *
 * Input parameters:
 *   num - the number of the driver
 *
 * Returned Value:
 *   Zero (OK) on success; a -1 value on failure.
 *
 ************************************************************************************/

int stm32_mpu9250_initialize(int num)
{
  FAR struct spi_dev_s *spi;
  int ret = 0;
  FAR const char *devpath;
  char devname[14];

  /*Initialize the SPI driver*/
  spi = stm32_spibus_initialize(4);

  if (!spi)
  {
	  return -1;
  }
  /*register the mpu9250 character driver */
  sprintf(devname,"/dev/MPU9250_%x",num);

  devpath = devname;

  mpu9250_register(devpath,spi,num);
  return ret;
}

#endif /* CONFIG_SPI && CONFIG_BMA456 */

