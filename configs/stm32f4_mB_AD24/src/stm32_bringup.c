/****************************************************************************
 * config/stm32f4_vamex_imu/src/stm32_bringup.c
 *
 *   Copyright (C) 2012, 2014-2016 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/mount.h>
#include <stdbool.h>
#include <stdio.h>
#include <debug.h>
#include <errno.h>

#ifdef CONFIG_USBMONITOR
#  include <nuttx/usb/usbmonitor.h>
#endif

#include <nuttx/binfmt/elf.h>

#include "stm32.h"

#ifdef CONFIG_STM32_OTGFS
#  include "stm32_usbhost.h"
#endif



#include <stm32f4_mB_AD24.h>


/* Conditional logic in stm32f4_vamex_imu.h will determine if certain features
 * are supported.  Tests for these features need to be made after including
 * stm32f4_vamex_imu.h.
 */

#ifdef HAVE_RTC_DRIVER
#  include <nuttx/timers/rtc.h>
#  include "stm32_rtc.h"
#endif

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: stm32_bringup
 *
 * Description:
 *   Perform architecture-specific initialization
 *
 *   CONFIG_BOARD_INITIALIZE=y :
 *     Called from board_initialize().
 *
 *   CONFIG_BOARD_INITIALIZE=n && CONFIG_LIB_BOARDCTL=y :
 *     Called from the NSH library
 *
 ****************************************************************************/

int stm32_bringup(void)
{
  int ret = OK;


#if !defined(CONFIG_ARCH_LEDS) && defined(CONFIG_USERLED_LOWER)
  /* Register the LED driver */
	userled_lower_initialize(LED_DRIVER_PATH);
/* seen in nucleo-144
  ret = userled_lower_initialize(LED_DRIVER_PATH);
  if (ret < 0)
    {
      syslog(LOG_ERR, "ERROR: userled_lower_initialize() failed: %d\n", ret);
    }
*/
#endif


#ifdef CONFIG_HPTC
  /* Initialize and register the HPTC device. */
  ret = stm32_hptc_setup();
  if (ret < 0)
    {
	  syslog(LOG_ERR, "ERROR: stm32_hptc_setup() failed: %d\n", ret);
    }
#endif




#ifdef HAVE_ELF
  /* Initialize the ELF binary loader */

  ret = elf_initialize();
  if (ret < 0)
    {
      serr("ERROR: Initialization of the ELF loader failed: %d\n", ret);
    }
#endif

/* AD5662 initalization */
#ifdef CONFIG_DAC_AD5662
  ret = stm32_ad5662_initialize("/dev/temp0");
#endif

#if 0
/* AD7190 initalization */
#ifdef CONFIG_SPI & CONFIG_ADC_AD7190

  if((CONFIG_AD7190_NUMBEROFDEVICES >= 1)){
	  stm32_ad7190_initialize(1);
  }
  if((CONFIG_AD7190_NUMBEROFDEVICES >= 2)){
	 stm32_ad7190_initialize(2);
  }
  if((CONFIG_AD7190_NUMBEROFDEVICES >= 3)){
 	  stm32_ad7190_initialize(3);
   }
  if((CONFIG_AD7190_NUMBEROFDEVICES >= 4)){
  	  stm32_ad7190_initialize(4);
    }
  if((CONFIG_AD7190_NUMBEROFDEVICES >= 5)){
  	  stm32_ad7190_initialize(5);
    }
  if((CONFIG_AD7190_NUMBEROFDEVICES >= 6)){
	  stm32_ad7190_initialize(6);
    }
  if((CONFIG_AD7190_NUMBEROFDEVICES >= 7)){
  	  stm32_ad7190_initialize(7);
    }
  if((CONFIG_AD7190_NUMBEROFDEVICES >= 8)){
   	  stm32_ad7190_initialize(8);
     }
  if((CONFIG_AD7190_NUMBEROFDEVICES >= 9)){
      stm32_ad7190_initialize(9);
  	  }
  if((CONFIG_AD7190_NUMBEROFDEVICES >= 10)){
       stm32_ad7190_initialize(10);
         }
  if((CONFIG_AD7190_NUMBEROFDEVICES >= 11)){
       stm32_ad7190_initialize(11);
         }
  if((CONFIG_AD7190_NUMBEROFDEVICES >= 12)){
       stm32_ad7190_initialize(12);
  }
#endif

#endif

#ifdef CONFIG_FS_PROCFS
  /* Mount the procfs file system */

  ret = mount(NULL, STM32_PROCFS_MOUNTPOINT, "procfs", 0, NULL);
  if (ret < 0)
    {
      serr("ERROR: Failed to mount procfs at %s: %d\n",
           STM32_PROCFS_MOUNTPOINT, ret);
    }
#endif

  return ret;
}
