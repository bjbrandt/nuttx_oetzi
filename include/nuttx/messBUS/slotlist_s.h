/*
 * slotlist_s.h
 *
 *  Created on: 05.03.2018
 *      Author: bbrandt
 */

#ifndef NUTTX_INCLUDE_NUTTX_MESSBUS_SLOTLIST_S_H_
#define NUTTX_INCLUDE_NUTTX_MESSBUS_SLOTLIST_S_H_

#include <nuttx/config.h>
#include "messBUS_config.h"
#include "slot_s.h"

#if defined(__cplusplus)
namespace Physikschicht
{
#endif

#ifdef CONFIG_MESSBUS_MASTER
	#if CONFIG_MESSBUS_USE_CH1
	#   define CH1_NUMSLOTS (CONFIG_MESSBUS_CH1_MAX_RX_SLOTS + CONFIG_MESSBUS_CH1_MAX_TX_SLOTS)
	typedef struct ch1_slotlist_s
	{
		uint8_t n_slots;
		struct slot_s slot[CH1_NUMSLOTS];
	} ch1_slotlist_s;
	#endif
	#if CONFIG_MESSBUS_USE_CH2
	#  define CH2_NUMSLOTS (CONFIG_MESSBUS_CH2_MAX_RX_SLOTS + CONFIG_MESSBUS_CH2_MAX_TX_SLOTS)
	typedef struct ch2_slotlist_s
	{
		uint8_t n_slots;
		struct slot_s slot[CH2_NUMSLOTS];
	} ch2_slotlist_s;
	#endif
	#if CONFIG_MESSBUS_USE_CH3
	#  define CH3_NUMSLOTS (CONFIG_MESSBUS_CH3_MAX_RX_SLOTS + CONFIG_MESSBUS_CH3_MAX_TX_SLOTS)
	typedef struct ch3_slotlist_s
	{
		uint8_t n_slots;
		struct slot_s slot[CH3_NUMSLOTS];
	} ch3_slotlist_s;
	#endif
	#if CONFIG_MESSBUS_USE_CH4
	#  define CH4_NUMSLOTS (CONFIG_MESSBUS_CH4_MAX_RX_SLOTS + CONFIG_MESSBUS_CH4_MAX_TX_SLOTS)
	typedef struct ch4_slotlist_s
	{
		uint8_t n_slots;
		struct slot_s slot[CH4_NUMSLOTS];
	} ch4_slotlist_s;
	#endif

typedef ch4_slotlist_s slotlist_s;
#endif

#ifdef CONFIG_MESSBUS_CLIENT
	#  define CH1_NUMSLOTS (CONFIG_MESSBUS_CH1_MAX_RX_SLOTS + CONFIG_MESSBUS_CH1_MAX_TX_SLOTS)
	typedef struct ch1_slotlist_s
	{
		uint8_t n_slots;
		struct slot_s slot[CH1_NUMSLOTS];
	} ch1_slotlist_s;
#endif


#if defined(__cplusplus)
}
#endif

#endif /* NUTTX_INCLUDE_NUTTX_MESSBUS_SLOTLIST_S_H_ */
