/*
 * messBUS_led.h
 *
 *  Created on: 28.05.2018
 *      Author: bbrandt
 */

#ifndef NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_DEBUG_H_
#define NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_DEBUG_H_

#include <nuttx/config.h>

#ifdef CONFIG_MESSBUS_DEBUG


/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/
#define GPIO_MESSBUS_DEBUG_PIN    (GPIO_OUTPUT|GPIO_PUSHPULL|GPIO_SPEED_50MHz|\
							GPIO_OUTPUT_CLEAR|GPIO_PORTD|GPIO_PIN5)
#define GPIO_MB_DEBUG_ON	(1 << 5)
#define GPIO_MB_DEBUG_OFF	(1 << (5+16))

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/
void switchDebugPinOn(void);
void switchDebugPinOff(void);
void toggleDebugPin(void);

#endif

#endif /* NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_DEBUG_H_ */
