/****************************************************************************
 * include/nuttx/messBUS/messBUSMaster.h
 *
 *   Author: Peer Ulbig <p.ulbig@tu-braunschweig.de>
 *
 ****************************************************************************/

#ifndef NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUSMASTER_H_
#define NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUSMASTER_H_

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/fs/ioctl.h>
#include <nuttx/compiler.h>


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/
#include "messBUS_config.h"
#include "messBUS_ioctl.h"


/****************************************************************************
 * Public Types
 ****************************************************************************/
#include "slot_s.h"
#include "slotlist_s.h"
#include "slotlists_container_s.h"
#include "messBUS_callbacks.h"

typedef enum messBUS_channels_e
{
	channel1, channel2, channel3, channel4
} messBUS_channels;

/* This structure defines all of the operations provided by the architecture
 * specific logic.  All fields must be provided with non-NULL function pointers
 * by the caller of messBUS_register().
 */
struct messBUS_ops_s
{
	/* Setup the messBUS. */
	CODE int (*setup)(void);

	/* Disable the messBUS. */
	CODE int (*shutdown)(void);

	/* Start the messBUS. */
	CODE int (*start)(void);

	/* Stop the messBUS. */
	CODE int (*stop)(void);

	/* Convert the slotlist or the whole slotlist container. */
	CODE void (*convert_slotlist)(void *slotlist, void *actiontable, void *infocontainer);

    /* Update buffers */
    CODE void (*update_buffers)(void);

    /* Enable/Disable bus power */
    CODE void (*enableBusPower)(messBUS_channels channel, bool enable);
};

/* This is the device structure used by the driver.  The caller of
 * messBUSClient_register() must allocate and initialize this structure.
 */
struct messBUS_dev_s
{
	/* State data */
	uint8_t open;
	uint8_t running;
	uint8_t sync_count;
	uint8_t active_table;
	uint8_t new_table_avail;
#if CONFIG_MESSBUS_USE_CH1
	uint8_t		ch1_last_processed_slot_number;
#endif
#if CONFIG_MESSBUS_USE_CH2
	uint8_t		ch2_last_processed_slot_number;
#endif
#if CONFIG_MESSBUS_USE_CH3
	uint8_t		ch3_last_processed_slot_number;
#endif
#if CONFIG_MESSBUS_USE_CH4
	uint8_t		ch4_last_processed_slot_number;
#endif

	/* Pointer to the callback information */
	struct callbacks_info_s	*callbacks_info;

	/* Pointers to tables for the interrupt routine */
	void *isr_actiontable0;
	void *isr_actiontable1;
	void *isr_infocontainer0;
	void *isr_infocontainer1;

	/* Arch-specific operations as driver interface */
	FAR const struct messBUS_ops_s *ops;
};
typedef struct messBUS_dev_s messBUS_dev_t;

/****************************************************************************
 * Public Data
 ****************************************************************************/

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

/************************************************************************************
 * Name: messBUSMaster_register
 *
 * Description:
 *   Register the messBUSClient in the VFS.
 *
 ************************************************************************************/

int messBUSMaster_register(FAR const char *path, FAR messBUS_dev_t *dev);

/****************************************************************************
 * Name: messBUSMaster_initialize
 *
 * Description:
 *   Perform initialization and registration of the driver.
 *	 This method should be called from board-specific stm32_bringup.c
 *
 ****************************************************************************/

void messBUSMaster_initialize(void);

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUSMASTER_H_ */
