/*
 * messBUS_ioctl.h
 *
 *  Created on: 20.03.2018
 *      Author: bbrandt
 */

#ifndef NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_IOCTL_H_
#define NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_IOCTL_H_

#include <nuttx/config.h>

/* ioctl commands */

/* Command:     MESSBUSIOC_START
 * Description: Start the messBUS. This assumes that the ioctl command
 * 				MESSBUSIOC_ATTACH_SLOTLISTS has already been called
 * 				successfully, otherwise the messBUS does not know what to do.
 * Argument:    NULL
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_START  _MESSBUSIOC(0x0001)

/* Command:     MESSBUSIOC_ATTACH_SLOTLISTS
 * Description: Attach a slotlist container to the messBUS. For the messBUS
 * 				client, the slotlist container will consist only of one
 * 				pointer to a single slotlist, as we only support one channel
 * 				for the client.
 * Argument:    A pointer to an instance of struct slotlists_container_s.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_ATTACH_SLOTLISTS     _MESSBUSIOC(0x0002)

/* Command:     MESSBUSIOC_STOP
 * Description: Stop the messBUS.
 * Argument:    NULL
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_STOP     _MESSBUSIOC(0x0003)

/* Command:     MESSBUSIOC_SYNC_BUSYWAIT
 * Description: This call busily blocks until a SYNC occurs.
 * Argument:    NULL or a pointer to a uint8_t where to write the number of
 * 				already missed SYNCS.
 * Return:      Zero (OK) on success.  Minus one will be returned in case of
 * 				already missed SYNCS.
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_SYNC_BUSYWAIT     _MESSBUSIOC(0x0004)

/* Command:     MESSBUSIOC_UPDATE_RXBUFFERS
 * Description: Invalidate cache before reading from rxbuffers. This call is
 * 				only needed for STM32F7 series MCUs.
 * Argument:    NULL
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_UPDATE_RXBUFFERS     _MESSBUSIOC(0x0005)

/* Command:     MESSBUSIOC_GET_CLOCK_DEVIATION
 * Description: Get the client's clock deviation relative to the master
 * 				in ppm.
 * Argument:    Pointer to int32_t where to write the clock deviation in ppm.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_GET_CLOCK_DEVIATION     _MESSBUSIOC(0x0006)

/* Command:     MESSBUSIOC_CH1_GET_LAST_PROCESSED_SLOT_NUMBER
 * Description: Get the chronological number of the last processed slot of
 * 				channel 1 since the start of a new timesclice (first slot
 * 				in a timeslice has number 0). The slot number does not equal
 * 				the slot ID!
 * Argument:    Pointer to uint8_t where to write the slot number.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH1_GET_LAST_PROCESSED_SLOT_NUMBER     _MESSBUSIOC(0x0007)

/* Command:     MESSBUSIOC_CH2_GET_LAST_PROCESSED_SLOT_NUMBER
 * Description: Get the chronological number of the last processed slot of
 * 				channel 2 since the start of a new timesclice (first slot
 * 				in a timeslice has number 0). The slot number does not equal
 * 				the slot ID!
 * Argument:    Pointer to uint8_t where to write the slot number.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH2_GET_LAST_PROCESSED_SLOT_NUMBER     _MESSBUSIOC(0x0008)

/* Command:     MESSBUSIOC_CH3_GET_LAST_PROCESSED_SLOT_NUMBER
 * Description: Get the chronological number of the last processed slot of
 * 				channel 3 since the start of a new timesclice (first slot
 * 				in a timeslice has number 0). The slot number does not equal
 * 				the slot ID!
 * Argument:    Pointer to uint8_t where to write the slot number.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH3_GET_LAST_PROCESSED_SLOT_NUMBER     _MESSBUSIOC(0x0009)

/* Command:     MESSBUSIOC_CH4_GET_LAST_PROCESSED_SLOT_NUMBER
 * Description: Get the chronological number of the last processed slot of
 * 				channel 4 since the start of a new timesclice (first slot
 * 				in a timeslice has number 0). The slot number does not equal
 * 				the slot ID!
 * Argument:    Pointer to uint8_t where to write the slot number.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH4_GET_LAST_PROCESSED_SLOT_NUMBER     _MESSBUSIOC(0x000a)

/* Command:     MESSBUSIOC_CH1_ATTACH_NEXT_SLOT_CALLBACK
 * Description: Attach a callback function to the channel, which will be
 * 				automatically executed after the next completion of a slot.
 * 				For example, such callback may be used for waking up a
 * 				sleeping thread.
 * Argument:    Pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH1_ATTACH_NEXT_SLOT_CALLBACK     _MESSBUSIOC(0x000b)

/* Command:     MESSBUSIOC_CH2_ATTACH_NEXT_SLOT_CALLBACK
 * Description: Attach a callback function to the channel, which will be
 * 				automatically executed after the next completion of a slot.
 * 				For example, such callback may be used for waking up a
 * 				sleeping thread.
 * Argument:    Pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH2_ATTACH_NEXT_SLOT_CALLBACK     _MESSBUSIOC(0x000c)

/* Command:     MESSBUSIOC_CH3_ATTACH_NEXT_SLOT_CALLBACK
 * Description: Attach a callback function to the channel, which will be
 * 				automatically executed after the next completion of a slot.
 * 				For example, such callback may be used for waking up a
 * 				sleeping thread.
 * Argument:    Pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH3_ATTACH_NEXT_SLOT_CALLBACK     _MESSBUSIOC(0x000d)

/* Command:     MESSBUSIOC_CH4_ATTACH_NEXT_SLOT_CALLBACK
 * Description: Attach a callback function to the channel, which will be
 * 				automatically executed after the next completion of a slot.
 * 				For example, such callback may be used for waking up a
 * 				sleeping thread.
 * Argument:    Pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH4_ATTACH_NEXT_SLOT_CALLBACK     _MESSBUSIOC(0x000e)

/* Command:     MESSBUSIOC_CH1_ATTACH_SLOT_NUMBER_CALLBACK
 * Description: Attach a callback function to the channel, which will be
 * 				automatically executed after the completion of the slot
 * 				with the specified chronological number (first slot
 * 				in a timeslice has number 0). The slot number does not
 * 				equal the slot ID!
 * 				For example, such callback may be used for waking up a
 * 				sleeping thread.
 * Argument:    Pointer to callback_spec_s containing the slot number and
 * 				a pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH1_ATTACH_SLOT_NUMBER_CALLBACK     _MESSBUSIOC(0x000f)

/* Command:     MESSBUSIOC_CH2_ATTACH_SLOT_NUMBER_CALLBACK
 * Description: Attach a callback function to the channel, which will be
 * 				automatically executed after the completion of the slot
 * 				with the specified chronological number (first slot
 * 				in a timeslice has number 0). The slot number does not
 * 				equal the slot ID!
 * 				For example, such callback may be used for waking up a
 * 				sleeping thread.
 * Argument:    Pointer to callback_spec_s containing the slot number and
 * 				a pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH2_ATTACH_SLOT_NUMBER_CALLBACK     _MESSBUSIOC(0x0010)

/* Command:     MESSBUSIOC_CH3_ATTACH_SLOT_NUMBER_CALLBACK
 * Description: Attach a callback function to the channel, which will be
 * 				automatically executed after the completion of the slot
 * 				with the specified chronological number (first slot
 * 				in a timeslice has number 0). The slot number does not
 * 				equal the slot ID!
 * 				For example, such callback may be used for waking up a
 * 				sleeping thread.
 * Argument:    Pointer to callback_spec_s containing the slot number and
 * 				a pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH3_ATTACH_SLOT_NUMBER_CALLBACK     _MESSBUSIOC(0x0011)

/* Command:     MESSBUSIOC_CH4_ATTACH_SLOT_NUMBER_CALLBACK
 * Description: Attach a callback function to the channel, which will be
 * 				automatically executed after the completion of the slot
 * 				with the specified chronological number (first slot
 * 				in a timeslice has number 0). The slot number does not
 * 				equal the slot ID!
 * 				For example, such callback may be used for waking up a
 * 				sleeping thread.
 * Argument:    Pointer to callback_spec_s containing the slot number and
 * 				a pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH4_ATTACH_SLOT_NUMBER_CALLBACK     _MESSBUSIOC(0x0012)

/* Command:     MESSBUSIOC_CH1_ATTACH_NEXT_SYNC_CALLBACK
 * Description: Attach a callback function to the channel, which will be
 * 				automatically executed after the start of the next
 * 				timeslice.
 * 				For example, such callback may be used for waking up a
 * 				sleeping thread.
 * Argument:    Pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH1_ATTACH_NEXT_SYNC_CALLBACK     _MESSBUSIOC(0x0013)

/* Command:     MESSBUSIOC_CH2_ATTACH_NEXT_SYNC_CALLBACK
 * Description: Attach a callback function to the channel, which will be
 * 				automatically executed after the start of the next
 * 				timeslice.
 * 				For example, such callback may be used for waking up a
 * 				sleeping thread.
 * Argument:    Pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH2_ATTACH_NEXT_SYNC_CALLBACK     _MESSBUSIOC(0x0014)

/* Command:     MESSBUSIOC_CH3_ATTACH_NEXT_SYNC_CALLBACK
 * Description: Attach a callback function to the channel, which will be
 * 				automatically executed after the start of the next
 * 				timeslice.
 * 				For example, such callback may be used for waking up a
 * 				sleeping thread.
 * Argument:    Pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH3_ATTACH_NEXT_SYNC_CALLBACK     _MESSBUSIOC(0x0015)

/* Command:     MESSBUSIOC_CH4_ATTACH_NEXT_SYNC_CALLBACK
 * Description: Attach a callback function to the channel, which will be
 * 				automatically executed after the start of the next
 * 				timeslice.
 * 				For example, such callback may be used for waking up a
 * 				sleeping thread.
 * Argument:    Pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH4_ATTACH_NEXT_SYNC_CALLBACK     _MESSBUSIOC(0x0016)

/* Command:     MESSBUSIOC_CH1_CONFIGURE_WAKE_UP_CALLBACK
 * Description: Attach a callback function to the channel, which will be
 * 				automatically executed after wake up events.
 * 				For example, such callback may be used for waking up a
 * 				sleeping thread.
 * 				This callback event will earliest trigger after the last
 * 				slot of a timeslice has been processed and if wake up
 * 				callbacks are enabled via
 * 				MESSBUSIOC_CH1_ENABLE_WAKE_UP_CALLBACK.
 * 				Reconfiguration is also possible, however changes apply
 * 				only after a call to MESSBUSIOC_ATTACH_SLOTLISTS.
 * Argument:    Pointer to wake_up_callback_spec_s containing the wake up
 * 				time in us relative to the beginning of a timeslice and a
 * 				pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH1_CONFIGURE_WAKE_UP_CALLBACK     _MESSBUSIOC(0x0017)

/* Command:     MESSBUSIOC_CH2_CONFIGURE_WAKE_UP_CALLBACK
 * Description: Attach a callback function to the channel, which will be
 * 				automatically executed after wake up events.
 * 				For example, such callback may be used for waking up a
 * 				sleeping thread.
 * 				This callback event will earliest trigger after the last
 * 				slot of a timeslice has been processed and if wake up
 * 				callbacks are enabled via
 * 				MESSBUSIOC_CH1_ENABLE_WAKE_UP_CALLBACK.
 * 				Reconfiguration is also possible, however changes apply
 * 				only after a call to MESSBUSIOC_ATTACH_SLOTLISTS.
 * Argument:    Pointer to wake_up_callback_spec_s containing the wake up
 * 				time in us relative to the beginning of a timeslice and a
 * 				pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH2_CONFIGURE_WAKE_UP_CALLBACK     _MESSBUSIOC(0x0018)

/* Command:     MESSBUSIOC_CH3_CONFIGURE_WAKE_UP_CALLBACK
 * Description: Attach a callback function to the channel, which will be
 * 				automatically executed after wake up events.
 * 				For example, such callback may be used for waking up a
 * 				sleeping thread.
 * 				This callback event will earliest trigger after the last
 * 				slot of a timeslice has been processed and if wake up
 * 				callbacks are enabled via
 * 				MESSBUSIOC_CH1_ENABLE_WAKE_UP_CALLBACK.
 * 				Reconfiguration is also possible, however changes apply
 * 				only after a call to MESSBUSIOC_ATTACH_SLOTLISTS.
 * Argument:    Pointer to wake_up_callback_spec_s containing the wake up
 * 				time in us relative to the beginning of a timeslice and a
 * 				pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH3_CONFIGURE_WAKE_UP_CALLBACK     _MESSBUSIOC(0x0019)

/* Command:     MESSBUSIOC_CH4_CONFIGURE_WAKE_UP_CALLBACK
 * Description: Attach a callback function to the channel, which will be
 * 				automatically executed after wake up events.
 * 				For example, such callback may be used for waking up a
 * 				sleeping thread.
 * 				This callback event will earliest trigger after the last
 * 				slot of a timeslice has been processed and if wake up
 * 				callbacks are enabled via
 * 				MESSBUSIOC_CH1_ENABLE_WAKE_UP_CALLBACK.
 * 				Reconfiguration is also possible, however changes apply
 * 				only after a call to MESSBUSIOC_ATTACH_SLOTLISTS.
 * Argument:    Pointer to wake_up_callback_spec_s containing the wake up
 * 				time in us relative to the beginning of a timeslice and a
 * 				pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH4_CONFIGURE_WAKE_UP_CALLBACK     _MESSBUSIOC(0x001a)

/* Command:     MESSBUSIOC_CH1_ENABLE_WAKE_UP_CALLBACK
 * Description: Enable wake up callbacks. This command requires previous
 * 				configuration of the wake up callback via
 * 				MESSBUSIOC_CH1_CONFIGURE_WAKE_UP_CALLBACK.
 * Argument:    Pointer to wake_up_callback_spec_s containing the wake up
 * 				time in us relative to the beginning of a timeslice and a
 * 				pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH1_ENABLE_WAKE_UP_CALLBACK     _MESSBUSIOC(0x001b)

/* Command:     MESSBUSIOC_CH2_ENABLE_WAKE_UP_CALLBACK
 * Description: Enable wake up callbacks. This command requires previous
 * 				configuration of the wake up callback via
 * 				MESSBUSIOC_CH1_CONFIGURE_WAKE_UP_CALLBACK.
 * Argument:    Pointer to wake_up_callback_spec_s containing the wake up
 * 				time in us relative to the beginning of a timeslice and a
 * 				pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH2_ENABLE_WAKE_UP_CALLBACK     _MESSBUSIOC(0x001c)

/* Command:     MESSBUSIOC_CH3_ENABLE_WAKE_UP_CALLBACK
 * Description: Enable wake up callbacks. This command requires previous
 * 				configuration of the wake up callback via
 * 				MESSBUSIOC_CH1_CONFIGURE_WAKE_UP_CALLBACK.
 * Argument:    Pointer to wake_up_callback_spec_s containing the wake up
 * 				time in us relative to the beginning of a timeslice and a
 * 				pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH3_ENABLE_WAKE_UP_CALLBACK     _MESSBUSIOC(0x001d)

/* Command:     MESSBUSIOC_CH4_ENABLE_WAKE_UP_CALLBACK
 * Description: Enable wake up callbacks. This command requires previous
 * 				configuration of the wake up callback via
 * 				MESSBUSIOC_CH1_CONFIGURE_WAKE_UP_CALLBACK.
 * Argument:    Pointer to wake_up_callback_spec_s containing the wake up
 * 				time in us relative to the beginning of a timeslice and a
 * 				pointer to the callback function.
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH4_ENABLE_WAKE_UP_CALLBACK     _MESSBUSIOC(0x001e)

/* Command:     MESSBUSIOC_CH1_DISABLE_WAKE_UP_CALLBACK
 * Description: Disable the previously enabled wake up callback.
 * Argument:    NULL
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH1_DISABLE_WAKE_UP_CALLBACK     _MESSBUSIOC(0x001f)

/* Command:     MESSBUSIOC_CH2_DISABLE_WAKE_UP_CALLBACK
 * Description: Disable the previously enabled wake up callback.
 * Argument:    NULL
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH2_DISABLE_WAKE_UP_CALLBACK     _MESSBUSIOC(0x0020)

/* Command:     MESSBUSIOC_CH3_DISABLE_WAKE_UP_CALLBACK
 * Description: Disable the previously enabled wake up callback.
 * Argument:    NULL
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH3_DISABLE_WAKE_UP_CALLBACK     _MESSBUSIOC(0x0021)

/* Command:     MESSBUSIOC_CH4_DISABLE_WAKE_UP_CALLBACK
 * Description: Disable the previously enabled wake up callback.
 * Argument:    NULL
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH4_DISABLE_WAKE_UP_CALLBACK     _MESSBUSIOC(0x0022)

#ifdef CONFIG_MESSBUS_MASTER

/* Command:     MESSBUSIOC_CH1_CH3_ENABLE_BUS_POWER
 * Description: Enables the power output for bus 1 and 3 and starts the fan.
 * Requirements:Channel 1 or 3 (or both) has to be activated via messBUS_config.h
 *              (CONFIG_MESSBUS_USE_CH[X]) before.
 * Argument:    NULL
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH1_CH3_ENABLE_BUS_POWER         _MESSBUSIOC(0x0023)

/* Command:     MESSBUSIOC_CH2_CH4_ENABLE_BUS_POWER
 * Description: Enables the power output for bus 2 and 4.
 * Requirements:Channel 2 or 4 (or both) has to be activated via messBUS_config.h
 *              (CONFIG_MESSBUS_USE_CH[X]) before.
 * Argument:    NULL
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH2_CH4_ENABLE_BUS_POWER         _MESSBUSIOC(0x0024)

/* Command:     MESSBUSIOC_CH1_CH3_DISABLE_BUS_POWER
 * Description: Disables the previously enabled power output for bus 1 and 3 and
 *              stops the fan.
 * Argument:    NULL
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH1_CH3_DISABLE_BUS_POWER        _MESSBUSIOC(0x0025)

/* Command:     MESSBUSIOC_CH2_CH4_DISABLE_BUS_POWER
 * Description: Disables the previously enabled power output for bus 2 and 4.
 * Argument:    NULL
 * Return:      Zero (OK) on success.  Minus one will be returned on failure
 *              with the errno value set appropriately.
 */

#define MESSBUSIOC_CH2_CH4_DISABLE_BUS_POWER        _MESSBUSIOC(0x0026)

#endif // CONFIG_MESSBUS_MASTER

#endif /* NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_IOCTL_H_ */
