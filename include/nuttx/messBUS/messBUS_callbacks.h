/*
 * wait_s.h
 *
 *  Created on: 15.08.2018
 *      Author: bbrandt
 */

#ifndef NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_CALLBACKS_H_
#define NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_CALLBACKS_H_

#include <nuttx/config.h>
#include "messBUS_config.h"

#if defined(__cplusplus)
namespace Physikschicht
{
#endif

#define MESSBUS_NO_CALLBACK_REQUEST			0xff
#define MESSBUS_SYNC_CALLBACK_REQUEST		0xfe
#define MESSBUS_NEXT_SLOT_CALLBACK_REQUEST	0xfd
/* others: slot number to wait for */

/* This struct is used to attach a callback function to a specific slot*/
struct single_callback_spec_s
{
	uint8_t 	slot_number;
	void		*callback_function;
};

/* This struct is used to attach a callback function to a wake up event */
struct wake_up_callback_spec_s
{
	uint16_t 	t_wake_up; //[us]
	void		*callback_function;
};

/* These structs define callback information for a single channel */
struct channel_single_callback_info_s
{
	uint8_t		trigger_pending;
	uint8_t		pendsv_pending;
	uint8_t 	request;
	void		*callback_function;
};

struct channel_wake_up_callback_info_s
{
	uint8_t		configured;
	uint8_t		enabled;
	uint8_t		pendsv_pending;
	uint16_t	t_wake_up; //[us]
	void		*callback_function;
};


#ifdef CONFIG_MESSBUS_MASTER
struct callbacks_info_s
{
#if CONFIG_MESSBUS_USE_CH1
	struct channel_single_callback_info_s *ch1_single_callback_info;
	struct channel_wake_up_callback_info_s *ch1_wake_up_callback_info;
#endif
#if CONFIG_MESSBUS_USE_CH2
	struct channel_single_callback_info_s *ch2_single_callback_info;
	struct channel_wake_up_callback_info_s *ch2_wake_up_callback_info;
#endif
#if CONFIG_MESSBUS_USE_CH3
	struct channel_single_callback_info_s *ch3_single_callback_info;
	struct channel_wake_up_callback_info_s *ch3_wake_up_callback_info;
#endif
#if CONFIG_MESSBUS_USE_CH4
	struct channel_single_callback_info_s *ch4_single_callback_info;
	struct channel_wake_up_callback_info_s *ch4_wake_up_callback_info;
#endif
};

#elif CONFIG_MESSBUS_CLIENT
struct callbacks_info_s
{
	struct channel_single_callback_info_s *ch1_single_callback_info;
	struct channel_wake_up_callback_info_s *ch1_wake_up_callback_info;
};
#endif

#if defined(__cplusplus)
}
#endif

#endif /* NUTTX_INCLUDE_NUTTX_MESSBUS_MESSBUS_CALLBACKS_H_ */
