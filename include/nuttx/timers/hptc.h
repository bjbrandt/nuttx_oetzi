/****************************************************************************
 * include/nuttx/timers/hptc.h
 *
 *   Copyright (C) 2014 Gregory Nutt. All rights reserved.
 *   Authors: Gregory Nutt <gnutt@nuttx.org>
 *            Stefan Nowak
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef __INCLUDE_NUTTX_TIMERS_HPTC_H
#define __INCLUDE_NUTTX_TIMERS_HPTC_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/compiler.h>
#include <nuttx/irq.h>
#include <nuttx/fs/ioctl.h>
#include <stdbool.h>
#include <sys/types.h>
#include <semaphore.h>

//FIXME:
//#define CONFIG_HPTC_IC_FIFOSIZE 256

#ifdef CONFIG_HPTC

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#ifndef NS_PER_SEC
#define NS_PER_SEC 1000000000
#endif

#define HPTC_NS_PER_SYSTICK (1000*CONFIG_USEC_PER_TICK)
#define HPTC_NS_PER_TIMTICK (HPTC_NS_PER_SYSTICK/CONFIG_HPTC_HP_OVERSAMPLE)
#define HPTC_SYSTICK_FREQUENCY (NS_PER_SEC / HPTC_NS_PER_SYSTICK)





/* IOCTL Commands ***********************************************************/
/* The timer driver uses a standard character driver framework.  However,
 * since the timer driver is a device control interface and not a data
 * transfer interface, the majority of the functionality is implemented in
 * driver ioctl calls.  The timer ioctl commands are listed below:
 *
 * These are detected and handled by the "upper half" timer driver.
 *
 * HPTCIOC_START        - Start the timer
 *                      Argument: Ignored
 * HPTCIOC_STOP         - Stop the timer
 *                      Argument: Ignored
 * HPTCIOC_GETSTATUS    - Get the status of the timer.
 *                      Argument:  A writeable pointer to struct timer_status_s.
 * HPTCIOC_SETTIMEOUT   - Reset the timer timeout to this value
 *                      Argument: A 32-bit timeout value in microseconds.
 * HPTCIOC_NOTIFICATION - Set up to notify an application via a signal when
 *                      the timer expires.
 *                      Argument: A read-only pointer to an instance of
 *                      stuct timer_notify_s.
 *
 * WARNING: May change HPTCIOC_SETTIMEOUT to pass pointer to 64bit nanoseconds
 * or timespec structure.
 *
 * NOTE: The HPTCIOC_SETHANDLER ioctl cannot be supported in the kernel build
 * mode. In that case direct callbacks from kernel space into user space is
 * forbidden.
 *
 * NOTE: _TCIOC(0x0001) througn _TCIOC(0x001f) are reserved for use by the
 * timer driver to assure that the values are unique.  Other timer drivers,
 * such as the oneshot timer, must not use IOCTL commands in this numeric
 * range.
 * FIXME: HPTC uses the TCIOC value space
 */

#define HPTCIOC_START        _TCIOC(0x0081)
#define HPTCIOC_STOP         _TCIOC(0x0082)
#define HPTCIOC_GETSTATUS    _TCIOC(0x0083)
#define HPTCIOC_SETTIMEOUT   _TCIOC(0x0084)
#define HPTCIOC_NOTIFICATION _TCIOC(0x0085)
#define HPTCIOC_GETSEM		 _TCIOC(0x0086)
#define HPTCIOC_GETSEMTIME	 _TCIOC(0x0087)
#define HPTCIOC_TUNE		 _TCIOC(0x0088)
#define HPTCIOC_JUMP		 _TCIOC(0x0089)


/* Bit Settings *************************************************************/
/* Bit settings for the struct timer_status_s flags field */

#define HPTCFLAGS_ACTIVE     (1 << 0) /* 1=The timer is running */
#define HPTCFLAGS_HANDLER    (1 << 1) /* 1=Call the user function when the
                                     *   timer expires */

/****************************************************************************
 * Public Types
 ****************************************************************************/

/* Upper half callback prototype. Returns true to reload the timer, and the
 * function can modify the next interval if desired.
 */

typedef CODE bool (*hptccb_t)(FAR uint32_t *next_interval_us, FAR void *arg);

#if 0
 /* This method is called from the lower half, platform-specific hptc logic when
  * new input capture data is available.
  *
  * Input Parameters:
  *   dev  - The hptc device structure that was previously registered by adc_register()
  *   ch   - And ID for the hptc channel number that generated the data
  *   data - The actual converted data from the channel.
  *
  * Returned Value:
  *   Zero on success; a negated errno value on failure.
  */

typedef CODE int (*hptc_in_cb_t)(FAR struct hptc_dev_s *dev, uint8_t ch, uint8_t flags, int32_t capture_s, int32_t capture_ns);
#endif

/* This method is called from the lower half, platform-specific hptc logic when
 * an asynchronous input capture on aux channel was detected .
 *
 * Input Parameters:
 *   ch         - And ID for the hptc channel number that generated the data
 *   flags      - flags
 *   capture_s  - seconds of capture
 *   capture_ns - nanoseconds of capture
 *
 * Returned Value:
 *   Zero on success; a negated errno value on failure.
 */

typedef CODE int (*hptc_async_in_cb_t)( uint8_t ch, uint8_t flags, int32_t capture_s, int32_t capture_ns);

/* This is the type of the argument passed to the TCIOC_GETSTATUS ioctl and
 * and returned by the "lower half" getstatus() method.
 */

struct hptc_status_s
{
  uint32_t  flags;          /* See TCFLAGS_* definitions above */
  uint32_t  cnt1;//timeout;        /* The current timeout setting (in microseconds) */
  uint32_t  cnt2;//timeleft;       /* Time left until the timer expiration (in microseconds) */
};

/* This is the type of the argument passed to the TCIOC_NOTIFICATION ioctl */

struct hptc_notify_s
{
  FAR void *arg;            /* An argument to pass with the signal */
  pid_t     pid;            /* The ID of the task/thread to receive the signal */
  uint8_t   signo;          /* The signal number to use in the notification */
};

/* This structure provides the "lower-half" driver operations available to
 * the "upper-half" driver.
 */

struct hptc_dev_s;
struct hptc_ops_s
{
  /* Required methods ********************************************************/
  /* Start the timer, resetting the time to the current timeout */

  CODE int (*start)(FAR struct hptc_dev_s *dev, sem_t *sem);

  /* Stop the timer */

  CODE int (*stop)(FAR struct hptc_dev_s *dev);

  /* Get the current timer status */

  CODE int (*getstatus)(FAR struct hptc_dev_s *dev,
                        FAR struct hptc_status_s *status);

  /* Get the current semaphore trigger time */

  CODE int (*getsemtime)(FAR struct hptc_dev_s *dev,
                        FAR struct timespec *ts);

  /* Tune the clock */
  CODE int (*tune)(int32_t tune);

  /* Jump in time */
   CODE int (*jump)(struct timespec *jump);


  /* Set a new timeout value (and reset the timer) */

  CODE int (*settimeout)(FAR struct hptc_dev_s *dev,
                         uint32_t timeout);

  /* Call the NuttX INTERNAL asynchronous input-capture callback.
   * NOTE:  Providing callback==NULL disable.
   * NOT to call back into applications.
   */

  CODE int (*setasyncinputcallback)(CODE hptc_async_in_cb_t callback);

  /* Call the NuttX INTERNAL timeout callback on timeout.
   * NOTE:  Providing callback==NULL disable.
   * NOT to call back into applications.
   */

  CODE void (*setcallback)(FAR struct hptc_dev_s *dev,
                           CODE hptccb_t callback, FAR void *arg);

  /* Any ioctl commands that are not recognized by the "upper-half" driver
   * are forwarded to the lower half driver through this method.
   */

  CODE int (*ioctl)(FAR struct hptc_dev_s *dev, int cmd,
                    unsigned long arg);
};

/* This structure provides the publicly visible representation of the
 * "lower-half" driver state structure.  "lower half" drivers will have an
 * internal structure definition that will be cast-compatible with this
 * structure definitions.
 */

struct hptc_dev_s
{
#ifdef CONFIG_HPTC
  /* Fields managed by common upper half HPTC logic */
  uint8_t   crefs;         /* The number of times the device has been opened */
  uint8_t   signo;         /* The signal number to use in the notification */
  pid_t     pid;           /* The ID of the task/thread to receive the signal */
  sem_t     sem;           /* semaphore to be posted */
  FAR void *arg;           /* An argument to pass with the signal */
  FAR char *path;          /* Registration path */

  uint8_t nrxwaiters; /* Number of threads waiting to enqueue a message */
//  struct hptc_ic_fifo_s ic_recv;       /* Describes receive FIFO */

#endif /* CONFIG_HPTC */
  /* Fields provided by lower half HPTC logic */
  FAR const struct hptc_ops_s  *ops;  /* Lower half operations */
  FAR void  *priv;  /* Lower half operations */

};


typedef struct hptc_dev_s hptc_dev_t;

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif


/****************************************************************************
 * let other drivers set an asynchronous interrupt callback on hptc input
 ****************************************************************************/
int hptc_set_async_input_callback(hptc_async_in_cb_t callback);


/****************************************************************************
 * "Upper-Half" Timer Driver Interfaces
 ****************************************************************************/
/****************************************************************************
 * Name: timer_register
 *
 * Description:
 *   This function binds an instance of a "lower half" timer driver with the
 *   "upper half" timer device and registers that device so that can be used
 *   by application code.
 *
 *   When this function is called, the "lower half" driver should be in the
 *   disabled state (as if the stop() method had already been called).
 *
 *   NOTE:  Normally, this function would not be called by application code.
 *   Rather it is called indirectly through the architecture-specific
 *   initialization.
 *
 * Input parameters:
 *   dev path - The full path to the driver to be registers in the NuttX
 *     pseudo-filesystem.  The recommended convention is to name all timer
 *     drivers as "/dev/timer0", "/dev/timer1", etc.  where the driver
 *     path differs only in the "minor" number at the end of the device name.
 *   lower - A pointer to an instance of lower half timer driver.  This
 *     instance is bound to the timer driver and must persists as long as
 *     the driver persists.
 *
 * Returned Value:
 *   On success, a non-NULL handle is returned to the caller.  In the event
 *   of any failure, a NULL value is returned.
 *
 *************setinputcallback***************************************************************/

FAR void *hptc_register(FAR const char *path,
                         FAR struct hptc_dev_s *dev);

/****************************************************************************
 * Name: timer_unregister
 *
 * Description:
 *   This function can be called to disable and unregister the timer
 *   device driver.
 *
 * Input parameters:
 *   handle - This is the handle that was returned by timer_register()
 *
 * Returned Value:
 *   None
 *
 ****************************************************************************/

void hptc_unregister(FAR void *handle);

/****************************************************************************
 * Kernel internal interfaces.  These may not be used by application logic
 ****************************************************************************/

/****************************************************************************
 * Name: timer_setcallback
 *
 * Description:
 *   This function can be called to add a callback into driver-related code
 *   to handle timer expirations.  This is a strictly OS internal interface
 *   and may NOT be used by application code.
 *
 * Input parameters:
 *   handle   - This is the handle that was returned by timer_register()
 *   callback - The new timer interrupt callback
 *   arg      - Argument provided when the callback is called.
 *
 * Returned Value:
 *   None
 *
 ****************************************************************************/

#ifdef __KERNEL__
int hptc_setcallback(FAR void *handle, hptccb_t callback, FAR void *arg);
#endif

/****************************************************************************
 * Platform-Independent "Lower-Half" Timer Driver Interfaces
 ****************************************************************************/

/****************************************************************************
 * Architecture-specific Application Interfaces
 ****************************************************************************/

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif /* CONFIG_TIMER */
#endif  /* __INCLUDE_NUTTX_TIMERS_HPTC_H */
